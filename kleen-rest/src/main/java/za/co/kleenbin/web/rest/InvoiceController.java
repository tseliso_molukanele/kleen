package za.co.kleenbin.web.rest;

import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_ADMIN;

import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.Invoice;
import za.co.kleenbin.dto.InvoiceData;
import za.co.kleenbin.service.InvoiceService;

@RestController
@RequestMapping("/invoice")
public class InvoiceController extends AbstractController {

    @Autowired
    private InvoiceService invoiceService;

    @Secured({ROLE_ADMIN})
    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<InvoiceData> data(@RequestParam(required = true) String yearmonth) {

        InvoiceData data = invoiceService.getData(yearmonth);

        return new ResponseEntity<InvoiceData>(data, HttpStatus.OK);
    }

    @Secured({ROLE_ADMIN})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Invoice> processPayment(@PathVariable Long id, @RequestBody Invoice invoice) {

        invoice = invoiceService.updateInvoice(id, invoice);

        return new ResponseEntity<Invoice>(invoice, HttpStatus.CREATED);
    }
    
    @Secured({ROLE_ADMIN})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Invoice> getPayment(@PathVariable Long id) {

        Invoice invoice = invoiceService.getInvoice(id);

        return new ResponseEntity<Invoice>(invoice, HttpStatus.OK);
    }

    @Secured({ROLE_ADMIN})
    @RequestMapping(value = "/action/run", method = RequestMethod.PUT)
    public ResponseEntity invoice(
            @RequestParam(value = "date", required = true)
            @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
            @RequestParam(value = "send", required = false) boolean send) {

    	if(send) {
    			
   			invoiceService.invoiceRun(send);
   		} else {
    	
   			invoiceService.invoiceRun(new DateTime(date));
   		}

        return new ResponseEntity(HttpStatus.CREATED);
    }
    
    @Secured({ROLE_ADMIN})
    @RequestMapping(value = "/action/send", method = RequestMethod.PUT)
    public ResponseEntity invoiceSend() {

        invoiceService.invoiceSend();

        return new ResponseEntity(HttpStatus.CREATED);
    }

}
