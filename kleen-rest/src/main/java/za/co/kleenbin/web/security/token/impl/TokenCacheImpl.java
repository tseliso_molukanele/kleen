package za.co.kleenbin.web.security.token.impl;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import za.co.kleenbin.web.security.token.TokenCache;

@Component
public class TokenCacheImpl implements TokenCache {

	private Map<String, Authentication> map = null;
	
	@Autowired
	public TokenCacheImpl(
			@Value("${sessionExpirySeconds}") long sessionExpirySeconds) {

		map = new PassiveExpiringMap<String, Authentication>(
				sessionExpirySeconds, TimeUnit.SECONDS);
	}
	
	@Override
	public void put(String token, Authentication authentication) {

		if (authentication == null) {

			return;
		}
		map.put(token, authentication);
	}

	@Override
	public Authentication get(String token) {

		Authentication value = map.get(token);

		/*
		 * Reset expiry time.
		 */
		if (value != null) {

			map.put(token, value);
		}

		return value;
	}
}
