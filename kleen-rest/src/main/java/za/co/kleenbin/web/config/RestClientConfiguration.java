package za.co.kleenbin.web.config;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import javax.security.auth.login.LoginException;
import javax.sql.DataSource;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleen.conf.DAOConfiguration;
import za.co.kleenbin.conf.Config;
import za.co.kleenbin.service.AdvancedAuditorAware;

@Configuration
@EnableScheduling
@Import({DAOConfiguration.class, Config.class})
@EnableWebMvc
@PropertySource("file:${CONF_HOME}/kleen.properties")
@ComponentScan({ "za.co.kleenbin.service", "za.co.kleenbin.schedule", "za.co.kleenbin.web" })
public class RestClientConfiguration {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Bean
    MappingJackson2HttpMessageConverter jacksonConverter() {
        return new MappingJackson2HttpMessageConverter();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(destroyMethod = "")
    public DataSource dataSource() {

        return dataSourceLookUp().getDataSource("jdbc/kleenbinds");
    }

    @Bean
    public JndiDataSourceLookup dataSourceLookUp() {
        final JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
        Properties jndiEnvironment = new Properties();
        dataSourceLookup.setJndiEnvironment(jndiEnvironment);
        dataSourceLookup.setResourceRef(true);
        return dataSourceLookup;
    }
    
    @Bean
    public AdvancedAuditorAware auditorAware() {
        
        return new AdvancedAuditorAware() {

            @Override
            public String getCurrentAuditorRole() {

                if(SecurityContextHolder.getContext().getAuthentication() == null) {
                    return "SYSTEM";
                }

                if(SecurityContextHolder.getContext().getAuthentication().getName() == null) {
                    return "SYSTEM";
                }

                Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
                if(authorities.size() == 0) {
                    return "SYSTEM";
                }

                return authorities.iterator().next().getAuthority();
            }

            @Override
            public String getCurrentAuditor() {
                
                if(SecurityContextHolder.getContext().getAuthentication() == null) {
                    return "SYSTEM";
                }
                
                if(SecurityContextHolder.getContext().getAuthentication().getName() == null) {
                    return "SYSTEM";
                }
                
                return (String)SecurityContextHolder.getContext().getAuthentication().getName();
            }
        };
    }
    
    @Value("${messaging.server}")
    private String messagingServer;
    
    @Value("${messaging.port}")
    private Integer messagingPort;
    
    @Value("${messaging.user}")
    private String messagingUser;
    
    @Value("${messaging.password}")
    private String messagingPassword;

    @Value("${messaging.mock:false}")
    private Boolean mockMessaging;
    
    @Bean
    public Client getStompClient() throws LoginException, IOException {

        if(mockMessaging) {

            Client mockClient = Mockito.mock(Client.class);
            
			doAnswer(new Answer<Object>() {

				@Override
				public Object answer(InvocationOnMock invocation) throws Throwable {
					
					logger.info(" ====== ====== ====== ====== ");
					logger.info(" ====== ====== ====== ====== ");
					logger.info("sending to destination: " + invocation.getArguments()[0]);
					logger.info(" ====== ====== ====== ====== ");
					logger.info("sending message: " + invocation.getArguments()[1]);
					logger.info(" ====== ====== ====== ====== ");
					
					return null;
				}
				
			}).when(mockClient).send(any(String.class), any(String.class));
            
			return mockClient;
        } else {

            return new Client(messagingServer, messagingPort, messagingUser, messagingPassword);
        }
    }
}
