package za.co.kleenbin.web.rest;

import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_FIELD;
import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_OFFICE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.Payment;
import za.co.kleenbin.service.PaymentService;

@RestController
@RequestMapping("/payment")
public class PaymentController extends AbstractController {

    @Autowired
    private PaymentService paymentService;

    @Secured({ROLE_OFFICE, ROLE_FIELD})
    @RequestMapping(value = "", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Payment> processPayment(@RequestBody Payment payment) {

        payment = paymentService.savePayment(payment);

        return new ResponseEntity<Payment>(payment, HttpStatus.CREATED);
    }

    @Secured({ROLE_OFFICE, ROLE_FIELD})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Payment> processPayment(@PathVariable Long id, @RequestBody Payment payment) {

        payment = paymentService.updatePayment(id, payment);

        return new ResponseEntity<Payment>(payment, HttpStatus.CREATED);
    }
    
    @Secured({ROLE_OFFICE, ROLE_FIELD})
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Payment>> getPayments() {
        
        List<Payment> payments = paymentService.getPayments();
        
        return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
    }

    @Secured({ROLE_OFFICE, ROLE_FIELD})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Payment> getPayment(@PathVariable Long id) {

        Payment payment = paymentService.getPayment(id);

        return new ResponseEntity<Payment>(payment, HttpStatus.OK);
    }

}
