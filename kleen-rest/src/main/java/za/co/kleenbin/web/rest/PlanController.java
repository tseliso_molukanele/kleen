package za.co.kleenbin.web.rest;

import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_OFFICE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.Plan;
import za.co.kleenbin.service.PlanService;

@RestController
@RequestMapping("/plan")
public class PlanController extends AbstractController {

    @Autowired
    private PlanService planService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Plan>> getPlans() {

        List<Plan> plans = planService.getPlans();

        return new ResponseEntity<List<Plan>>(plans, HttpStatus.OK);
    }
    
    @Secured({ROLE_OFFICE})
    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Plan> createPlan(@RequestBody Plan inputPlan) {
    	
    	Plan plan = planService.newPlan(inputPlan);
    	
    	return new ResponseEntity<Plan>(plan, HttpStatus.OK);
    }
    
    @Secured({ROLE_OFFICE})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Plan> updatePlan(@RequestBody Plan inputPlan, @PathVariable Long id) {
    	
    	Plan plan = planService.updatePlan(id, inputPlan);
    	
    	if(plan == null) {
    	
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	}
    	
    	return new ResponseEntity<Plan>(plan, HttpStatus.OK);
    }

}
