package za.co.kleenbin.web.rest;

import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_ADMIN;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.ExpenseItem;
import za.co.kleenbin.dto.IncomeInvoiceItem;
import za.co.kleenbin.dto.IncomeItem;
import za.co.kleenbin.service.ReportService;

@RestController
@RequestMapping("/report")
public class ReportController extends AbstractController {

	@Autowired
	private ReportService reportService;

	@Secured(ROLE_ADMIN)
	@RequestMapping(value = "/incomeInvoice", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<IncomeInvoiceItem>> getReportData() {

		return new ResponseEntity<List<IncomeInvoiceItem>>(reportService.getIncomeInvoiceReport(), HttpStatus.OK);
	}

	@Secured(ROLE_ADMIN)
	@RequestMapping(value = "/income", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<IncomeItem>> getIncomeReportData() {

		return new ResponseEntity<List<IncomeItem>>(reportService.getIncomeReport(), HttpStatus.OK);
	}

	@Secured(ROLE_ADMIN)
	@RequestMapping(value = "/expense", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<ExpenseItem>> getExpenseReportData() {

		return new ResponseEntity<List<ExpenseItem>>(reportService.getExpenseReport(), HttpStatus.OK);
	}
}
