package za.co.kleenbin.web.security.token;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import za.co.kleenbin.web.security.AuthSuccessHandler;

@Component
public class TokenAuthenticationFilter implements Filter {

    @Autowired
    private AuthenticationProvider authenticationProvider;
    
    @Autowired
    private TokenCache tokenCache;
	
    public TokenAuthenticationFilter() {
        
    }
    
	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest)req;
		
		String token = httpServletRequest.getHeader(AuthSuccessHandler.AUTH_HEADER_NAME);
				
		if (token != null) {
				
				Authentication authentication = tokenCache.get(token);				
				SecurityContextHolder.getContext().setAuthentication(authentication);
		} else {

		    String username = httpServletRequest.getHeader(AuthSuccessHandler.AUTH_HEADER_USERNAME);
		    String password = httpServletRequest.getHeader(AuthSuccessHandler.AUTH_HEADER_PASSWORD);
		    if(null != username && null != password) {
		        
		        Authentication authentication = authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		        SecurityContextHolder.getContext().setAuthentication(authentication);
		    }
		}
		
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
