package za.co.kleenbin.web.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import za.co.kleenbin.web.security.token.TokenCache;
import za.co.kleenbin.web.security.token.TokenHandler;

@Component
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthSuccessHandler.class);
    public static final String AUTH_HEADER_NAME = "x-auth-token";
    public static final String AUTH_HEADER_USERNAME = "auth-username";
    public static final String AUTH_HEADER_PASSWORD = "auth-password";

    private final ObjectMapper mapper;

    @Autowired
    AuthSuccessHandler(MappingJackson2HttpMessageConverter messageConverter) {
        this.mapper = messageConverter.getObjectMapper();
    }
    
    @Autowired
    private TokenHandler tokenHandler;
    
    @Autowired
    private TokenCache tokenCache;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_OK);

        User user = (User) authentication.getPrincipal();
        
        String token = tokenHandler.createTokenForUser((UsernamePasswordAuthenticationToken)authentication);
		response.addHeader(AUTH_HEADER_NAME, token);
        tokenCache.put(token, authentication);

        LOGGER.info(user.getUsername() + " got is connected ");

        PrintWriter writer = response.getWriter();
        mapper.writeValue(writer, user);
        writer.flush();
    }
}
