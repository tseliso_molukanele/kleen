package za.co.kleenbin.web.rest;

import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_FIELD;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.Schedule;
import za.co.kleenbin.dto.Service;
import za.co.kleenbin.service.ScheduleService;

@RestController
@RequestMapping("/schedule")
public class ScheduleController extends AbstractController {

    @Autowired
    private ScheduleService scheduleService;
    
    @RequestMapping(value = "/{day}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Schedule> processPayment(@PathVariable int day) {

        Schedule schedule = scheduleService.getSchedule(day + 1);

        return new ResponseEntity<Schedule>(schedule, HttpStatus.OK);
    }
    
    @Secured(ROLE_FIELD)
    @RequestMapping(value = "", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Service> processPayment(@RequestBody Service service) {

        service = scheduleService.processService(service);

        return new ResponseEntity<Service>(service, HttpStatus.CREATED);
    }

}
