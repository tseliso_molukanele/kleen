package za.co.kleenbin.web.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import za.co.kleenbin.service.AdvancedAuditorAware;
import za.co.kleenbin.service.RolesService;

@Service
public class CustomUserDetailsService implements UserDetailsService, RolesService {
        private final static String USER_SYSTEM = "system";
        private final static String USER_1 = "tseliso";
	private final static String USER_2 = "kagiso";
	private final static String USER_3 = "lirontsho";

    public static final String ROLE_OFFICE = "ROLE_OFFICE";
    public static final String ROLE_FIELD = "ROLE_FIELD";
    public static final String ROLE_SALES = "ROLE_SALES";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";

	@Autowired
	private AdvancedAuditorAware advancedAuditorAware;

	CustomUserDetailsService() {
	}

	private List<String> getAllowedRoles(String role) {

		List<String> roles = new ArrayList<String>();

		if(ROLE_OFFICE.equalsIgnoreCase(role)) {

			roles.add(ROLE_OFFICE);
		} if(ROLE_SALES.equalsIgnoreCase(role)) {

			roles.add(ROLE_SALES);
		} else if(ROLE_FIELD.equalsIgnoreCase(role)) {

			roles.add(ROLE_FIELD);
		} else if(ROLE_ADMIN.equalsIgnoreCase(role)) {

			roles.add(ROLE_ADMIN);
			roles.add(ROLE_FIELD);
			roles.add(ROLE_OFFICE);
			roles.add(ROLE_SALES);
		}

		return roles;
	}

	public boolean isAllowedForRole(String role) {

		List<String> allowedRoles = getAllowedRoles(advancedAuditorAware.getCurrentAuditorRole());

		return allowedRoles.contains(role);
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

	        if (username.equals(USER_SYSTEM)) {
                    ArrayList<GrantedAuthority> adminAuthorities = new ArrayList<GrantedAuthority>();
                    adminAuthorities.add(new SimpleGrantedAuthority(ROLE_OFFICE));
                    adminAuthorities.add(new SimpleGrantedAuthority(ROLE_FIELD));
                    adminAuthorities.add(new SimpleGrantedAuthority(ROLE_ADMIN));
                    adminAuthorities.add(new SimpleGrantedAuthority(ROLE_SALES));
                    User user = new User(USER_SYSTEM,
                                new ShaPasswordEncoder().encodePassword("sEcr3t@1", null),
                                adminAuthorities);

                    return user;
                } else if (username.equals(USER_1)) {
			ArrayList<GrantedAuthority> adminAuthorities = new ArrayList<GrantedAuthority>();
			adminAuthorities.add(new SimpleGrantedAuthority(ROLE_OFFICE));
			adminAuthorities.add(new SimpleGrantedAuthority(ROLE_FIELD));
			adminAuthorities.add(new SimpleGrantedAuthority(ROLE_ADMIN));
			adminAuthorities.add(new SimpleGrantedAuthority(ROLE_SALES));
			User user = new User(USER_1,
					new ShaPasswordEncoder().encodePassword("kL3enbin", null),
					adminAuthorities);

			return user;
		} else if (username.equals(USER_2)) {
			ArrayList<GrantedAuthority> fieldAuthorities = new ArrayList<GrantedAuthority>();
			fieldAuthorities.add(new SimpleGrantedAuthority(ROLE_FIELD));
			fieldAuthorities.add(new SimpleGrantedAuthority(ROLE_SALES));
			User user = new User(USER_2,
					new ShaPasswordEncoder().encodePassword("field39", null),
					fieldAuthorities);

			return user;
		}  else if (username.equals(USER_3)) {
			ArrayList<GrantedAuthority> fieldAuthorities = new ArrayList<GrantedAuthority>();
			fieldAuthorities.add(new SimpleGrantedAuthority(ROLE_OFFICE));
			User user = new User(USER_3,
					new ShaPasswordEncoder().encodePassword("pa55w0rd", null),
					fieldAuthorities);

			return user;
		} else {
			return null;
		}
	}
}
