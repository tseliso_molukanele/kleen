package za.co.kleenbin.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.AccountAction;
import za.co.kleenbin.service.CallService;

@RestController
@RequestMapping("/call")
public class CallController extends AbstractController {

    @Autowired
    private CallService callService;
    
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<AccountAction>> list() {

            List<AccountAction> callActions = callService.list();

            return new ResponseEntity<List<AccountAction>>(callActions, HttpStatus.OK);
    }
    
}
