package za.co.kleenbin.web.rest;

import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_FIELD;
import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_OFFICE;
import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_SALES;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.Invoice;
import za.co.kleenbin.dto.Payment;
import za.co.kleenbin.dto.Service;
import za.co.kleenbin.service.AccountService;
import za.co.kleenbin.service.StatementSender;
import za.co.kleenbin.web.exception.BadRequestException;

@RestController
@RequestMapping("/account")
public class AccountController extends AbstractController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private StatementSender statementSender;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Account>> processPayment(
			@RequestParam(required = false) String searchKey,
			@RequestParam(required = false) String mode) {

		List<Account> accounts = null;
		
		if ("ALL".equalsIgnoreCase(mode)) {
			accounts = accountService.list();
		} else {
			accounts = accountService.search(searchKey);
		}

		return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
	}

	@RequestMapping(value = "/{accountNumber}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Account> getAccount(
			@PathVariable String accountNumber) {

		Account account = accountService.getAccount(accountNumber);

		if(account == null) {
		
			return new ResponseEntity<Account>((Account)null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{accountNumber}/payment/", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Payment>> paymentsByAccount(
			@PathVariable String accountNumber) {

		Account account = new Account();
		account.setAccountNumber(accountNumber);
		List<Payment> payments = accountService
				.retrievePaymentsByAccount(account);

		return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
	}

	@RequestMapping(value = "/{accountNumber}/invoice/", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Invoice>> invoicesByAccount(
			@PathVariable String accountNumber) {

		Account account = new Account();
		account.setAccountNumber(accountNumber);
		List<Invoice> invoice = accountService
				.retrieveInvoicesByAccount(account);

		return new ResponseEntity<List<Invoice>>(invoice, HttpStatus.OK);
	}

	@RequestMapping(value = "/{accountNumber}/service/", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Service>> servicesByAccount(
			@PathVariable String accountNumber) {

		Account account = new Account();
		account.setAccountNumber(accountNumber);
		List<Service> invoice = accountService
				.retrieveServicesByAccount(account);

		return new ResponseEntity<List<Service>>(invoice, HttpStatus.OK);

	}

    @Secured(ROLE_OFFICE)
    @RequestMapping(value = "/{accountNumber}/activity", method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Account> editAccount(@PathVariable String accountNumber, @RequestParam(required = true) boolean up) {

        Account account = accountService.update(accountNumber, up);
        
        return new ResponseEntity<Account>(account, HttpStatus.ACCEPTED);
    }
        
    @RequestMapping(value = "/{accountNumber}/statement", method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> sendStatement(@PathVariable String accountNumber, @RequestParam(required = true) String type) {
                       
        if("email".equalsIgnoreCase(type)) {
            
            statementSender.emailStatement(accountNumber);
        } else if("sms".equalsIgnoreCase(type)) {

			statementSender.smsStatement(accountNumber);
		} else {

			return new ResponseEntity<>("Type " + type + " is not acceptible.", HttpStatus.NOT_ACCEPTABLE);
		}
        
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
	
	@Secured({ROLE_OFFICE, ROLE_FIELD})
	@RequestMapping(value = "/{accountNumber}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Account> editAccount(@PathVariable String accountNumber, @RequestBody Account account) {

	        if(!accountNumber.equalsIgnoreCase(account.getAccountNumber())) {
	        
	            throw new BadRequestException("Account number of data does not match the path!");
	        }
	    
		account = accountService.existingAccount(account);

		return new ResponseEntity<Account>(account, HttpStatus.ACCEPTED);
	}
	
	@Secured({ROLE_OFFICE, ROLE_FIELD, ROLE_SALES})
	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Account> newAccount(@RequestBody Account account) {

		account = accountService.newAccount(account);

		return new ResponseEntity<Account>(account, HttpStatus.CREATED);
	}
}
