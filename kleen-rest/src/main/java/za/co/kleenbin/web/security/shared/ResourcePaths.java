package za.co.kleenbin.web.security.shared;

public class ResourcePaths {

    public class User {
        public static final String ROOT = "/user";
        public static final String LOGIN = "/login";
        public static final String SYSTEM_INFO = "/unsecured/sysinfo";
    }
}
