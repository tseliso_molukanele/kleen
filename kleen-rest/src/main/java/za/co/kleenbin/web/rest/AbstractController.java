package za.co.kleenbin.web.rest;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ExceptionHandler;

import za.co.kleenbin.web.exception.BadRequestException;

public abstract class AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractController.class);

    private class ErrorPayload {

        private String message;

        public ErrorPayload(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorPayload> anyException(Exception e) {

        String bugRef = "KLEEN-" + new DateTime().toString("yyyyMMddHHmmssSSS");

        e.printStackTrace();
        LOG.debug(String.format("Reference[%s], error [%s], user [%s]", bugRef, e.getMessage(), getCurrentUser()), e);

        String message = e.getMessage();

        return new ResponseEntity<ErrorPayload>(new ErrorPayload(message), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorPayload> validationException(Exception e) {
        
        String bugRef = "KLEEN-" + new DateTime().toString("yyyyMMddHHmmssSSS");
        
        e.printStackTrace();
        LOG.trace(String.format("Reference[%s], error [%s], user [%s]", bugRef, e.getMessage(), getCurrentUser()), e);
        
        String message = e.getMessage();
        
        return new ResponseEntity<ErrorPayload>(new ErrorPayload(message), HttpStatus.BAD_REQUEST);
    }

    protected String getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth == null ? "anonymous" : auth.getName();
    }
    
    protected List<String> getCurrentRoles() {
        List<String> roles = new ArrayList<>();
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            return roles;
        }

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        for (GrantedAuthority grantedAuth : userDetails.getAuthorities()) {
            roles.add(grantedAuth.getAuthority());
        }

        return roles;
    }
}
