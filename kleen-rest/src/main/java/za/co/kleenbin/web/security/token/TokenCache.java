package za.co.kleenbin.web.security.token;

import org.springframework.security.core.Authentication;

public interface TokenCache {

	public void put(String token, Authentication authentication);
	public Authentication get(String token);	
}
