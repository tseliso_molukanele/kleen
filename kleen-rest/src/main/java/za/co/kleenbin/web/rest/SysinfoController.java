package za.co.kleenbin.web.rest;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.web.security.shared.ResourcePaths;

@RestController
@RequestMapping(ResourcePaths.User.SYSTEM_INFO)
public class SysinfoController extends AbstractController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Map<String, String>> get() throws IOException {

    	BufferedReader stream = new BufferedReader(new InputStreamReader(new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream("revision.rev"))));

        Map<String, String> returnables = new HashMap<>();
        returnables.put("LatestCommit", stream.readLine());
    	
        return new ResponseEntity<Map<String, String>>(returnables, HttpStatus.OK);
    }
}
