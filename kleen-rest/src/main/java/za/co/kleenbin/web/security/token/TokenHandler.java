package za.co.kleenbin.web.security.token;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public interface TokenHandler {

	public abstract String createTokenForUser(UsernamePasswordAuthenticationToken user);

}