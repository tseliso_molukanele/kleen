package za.co.kleenbin.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.Action;
import za.co.kleenbin.dto.Task;
import za.co.kleenbin.service.TaskService;

@RestController
@RequestMapping("/task")
public class TaskController extends AbstractController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Task> getNext() {

        Task tasks = taskService.getTask(getCurrentRoles());

        return new ResponseEntity<Task>(tasks, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/action", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Task> action(@RequestBody Action action) {

        Task task = taskService.action(action);

        return new ResponseEntity<Task>(task, HttpStatus.OK);
    }

}
