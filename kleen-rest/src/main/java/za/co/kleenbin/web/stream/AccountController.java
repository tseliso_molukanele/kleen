package za.co.kleenbin.web.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import za.co.kleenbin.web.rest.AbstractController;

@Controller("accountController_stream")
@RequestMapping("/account")
public class AccountController extends AbstractController {

    @Value("${reportLocation}")
    private String reportLocation;
    
    @RequestMapping(value = "/{accountNumber}/statement.pdf")
    public void retrieveDocument(@PathVariable String accountNumber, 
            HttpServletResponse response) throws IOException{

        stream(new URL(reportLocation + "/run?__report=statement.rptdesign&__format=pdf&accountNumber=" + accountNumber).openStream(), response.getOutputStream());
    }
    
    private long stream(InputStream input, OutputStream output) throws IOException {
        try (
            ReadableByteChannel inputChannel = Channels.newChannel(input);
            WritableByteChannel outputChannel = Channels.newChannel(output);
        ) {
            ByteBuffer buffer = ByteBuffer.allocateDirect(10240);
            long size = 0;

            while (inputChannel.read(buffer) != -1) {
                buffer.flip();
                size += outputChannel.write(buffer);
                buffer.clear();
            }

            return size;
        }
    }

}
