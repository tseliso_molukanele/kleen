package za.co.kleenbin.web.rest;

import static za.co.kleenbin.web.security.CustomUserDetailsService.ROLE_OFFICE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.kleenbin.dto.Expense;
import za.co.kleenbin.service.ExpenseService;

@RestController
@RequestMapping("/expense")
public class ExpenseController extends AbstractController {

    @Autowired
    private ExpenseService expenseService;

    @Secured({ROLE_OFFICE})
    @RequestMapping(value = "", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Expense> processExpense(@RequestBody Expense expense) {

        expense = expenseService.saveExpense(expense);

        return new ResponseEntity<Expense>(expense, HttpStatus.CREATED);
    }

    @Secured({ROLE_OFFICE})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Expense> processExpense(@PathVariable Long id, @RequestBody Expense expense) {

        expense = expenseService.updateExpense(id, expense);

        return new ResponseEntity<Expense>(expense, HttpStatus.CREATED);
    }
    
    @Secured({ROLE_OFFICE})
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Expense>> getExpenses() {
        
        List<Expense> expenses = expenseService.getExpenses();
        
        return new ResponseEntity<List<Expense>>(expenses, HttpStatus.OK);
    }

    @Secured({ROLE_OFFICE})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Expense> getExpense(@PathVariable Long id) {

        Expense expense = expenseService.getExpense(id);

        return new ResponseEntity<Expense>(expense, HttpStatus.OK);
    }

}
