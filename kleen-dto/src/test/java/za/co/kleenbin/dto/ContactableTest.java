package za.co.kleenbin.dto;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ContactableTest {

	static class ContactableBuilder {

		private List<PhoneNumber> phoneNumbers = new ArrayList<>();
		private List<EmailAddress> emailAddresses = new ArrayList<>();

		static ContactableBuilder aContable() {

			return new ContactableBuilder();
		}

		ContactableBuilder withNumber(String number) {

			PhoneNumber phoneNumber = new PhoneNumber();
			phoneNumber.setNumber(number);
			this.phoneNumbers.add(phoneNumber);

			return this;
		}

		public ContactableBuilder withEmail(String email) {

			EmailAddress address = new EmailAddress();
			address.setEmail(email);
			this.emailAddresses.add(address);

			return this;
		}

		Contactable build() {

			Contactable contactable = new Contactable();

			contactable.setEmailAddresses(emailAddresses);
			contactable.setPhoneNumbers(phoneNumbers);

			return contactable;
		}
	}

	@Test
	public void should_remove_cell_with_invalid_numbers() {

		//
		String validNumber = "0751231234";
		String inValidNumber1 = "1721175596"; // doesnt start with '0'
		String inValidNumber2 = "072117559"; // too few digits
		String inValidNumber3 = "07211755d9"; // invalid characters

		Contactable contactable = ContactableBuilder.aContable().withNumber(validNumber).withNumber(inValidNumber1)
				.withNumber(inValidNumber2).withNumber(inValidNumber3).build();

		//
		List<PhoneNumber> numbers = contactable.getPhoneNumbers();

		//
		assertEquals(1, numbers.size());
		assertEquals(validNumber, numbers.get(0).getNumber());
	}
	
	@Test
	public void should_remove_cell_with_null_empty_numbers() {

		//
		String validNumber = "0751231234";
		String inValidNumber1 = null; // is null
		String inValidNumber2 = ""; // is empty

		Contactable contactable = ContactableBuilder.aContable().withNumber(validNumber).withNumber(inValidNumber1)
				.withNumber(inValidNumber2).build();

		//
		List<PhoneNumber> numbers = contactable.getPhoneNumbers();

		//
		assertEquals(1, numbers.size());
		assertEquals(validNumber, numbers.get(0).getNumber());
	}

	@Test
	public void should_remove_email_with_invalid_address() {

		//
		String validEmail = "valid@email.com";
		String invalidEmail = "invalidemail.com";

		Contactable contactable = ContactableBuilder.aContable().withEmail(validEmail).withEmail(invalidEmail).build();

		//
		List<EmailAddress> emailAddresses = contactable.getEmailAddresses();

		//
		assertEquals(1, emailAddresses.size());
		assertEquals(validEmail, emailAddresses.get(0).getEmail());
	}

	@Test
	public void should_remove_email_with_null__empty_address() {

		//
		String validEmail = "valid@email.com";
		String invalidEmail = null;
		String invalidEmail1 = "";

		Contactable contactable = ContactableBuilder.aContable()
				.withEmail(validEmail)
				.withEmail(invalidEmail)
				.withEmail(invalidEmail1)
				.build();

		//
		List<EmailAddress> emailAddresses = contactable.getEmailAddresses();

		//
		assertEquals(1, emailAddresses.size());
		assertEquals(validEmail, emailAddresses.get(0).getEmail());
	}

}
