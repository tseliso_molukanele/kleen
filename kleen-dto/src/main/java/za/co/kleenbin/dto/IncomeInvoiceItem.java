package za.co.kleenbin.dto;

import java.util.Date;

public class IncomeInvoiceItem {

    private Date date;
    private Integer invoiceAmount;
    private Integer incomeAmount;
    private Integer expenseAmount;

    public Date getDate() {
        return date;
    }
    
    public Integer getIncomeAmount() {
        return incomeAmount;
    }
    public Integer getInvoiceAmount() {
        return invoiceAmount;
    }
    public Integer getExpenseAmount() {
        return expenseAmount;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public void setIncomeAmount(Integer incomeAmount) {
        this.incomeAmount = incomeAmount;
    }
    
    public void setInvoiceAmount(Integer invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public void setExpenseAmount(Integer expenseAmount) { this.expenseAmount = expenseAmount; }
}
