package za.co.kleenbin.dto.type;

public enum PaymentType {

    CASH, DEPOSIT, EFT, CHEQUE, ADJUSTMENT, OTHER
}
