package za.co.kleenbin.dto.type;

public enum AccountStatus {
    ACTIVE, INACTIVE, DISABLED
}
