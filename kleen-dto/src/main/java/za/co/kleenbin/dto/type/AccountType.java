package za.co.kleenbin.dto.type;

public enum AccountType {
	PRIVATE, CRECHE, RESTAURANT, COMPLEX
}
