package za.co.kleenbin.dto;

public class Person extends Contactable {

	private String firstName;
	private String lastName;
	private String initials;
	private String title;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDisplayName() {

		String returnValue = "";

		returnValue += (title != null && !title.trim().equals("") ? title + " "
				: "");
		returnValue += (firstName != null && !firstName.trim().equals("") ? firstName
				+ " "
				: "");
		returnValue += (lastName != null && !lastName.trim().equals("") ? lastName
				: "");

		return returnValue;
	}

}
