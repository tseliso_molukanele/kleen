package za.co.kleenbin.dto;

public class AccountTask extends Task {

    private Account account;
    
    public Account getAccount() {
        return account;
    }
    
    public void setAccount(Account account) {
        this.account = account;
    }
}
