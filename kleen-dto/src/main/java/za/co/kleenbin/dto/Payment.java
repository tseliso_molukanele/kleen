package za.co.kleenbin.dto;

import java.util.Date;

import za.co.kleenbin.dto.type.PaymentType;

public class Payment {

	private Account account;
	private Date date;
	private Integer amount;
	private Long id;
	private PaymentType type;
	private String createdBy;
	private String reference;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public PaymentType getType() {
		return type;
	}

	public void setType(PaymentType type) {
		this.type = type;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "Payment [accountNumber=" + account.getAccountNumber()
				+ ", date=" + date + ", amount=" + amount + ", reference="
				+ reference + "]";
	}
}
