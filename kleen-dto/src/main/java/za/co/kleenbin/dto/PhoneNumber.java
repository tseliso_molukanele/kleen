package za.co.kleenbin.dto;


import za.co.kleenbin.dto.type.NumberType;

public class PhoneNumber {

	private NumberType numberType;

	private String number;

	public String getNumber() {
		return number;
	}

	public NumberType getNumberType() {
		return numberType;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setNumberType(NumberType numberType) {
		this.numberType = numberType;
	}

	@Override
	public String toString() {

		return super.toString().charAt(0)
				+ super.toString().substring(1).toLowerCase();
	}
}
