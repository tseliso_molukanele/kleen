package za.co.kleenbin.dto.type;


public enum FREQUENCY {

	ONCE_PER_MONTH(1, "once per month"), BI_WEEKLY(2, "fortnighly"), EVERY_WEEK(4, "weekly"), DAILY(20, "daily");

	private int asNumber;
	private String displayName;

	private FREQUENCY(int asNumber, String displayName) {

		this.asNumber = asNumber;
		this.displayName = displayName;
	}

	public int asNumber() {
		return asNumber;
	}

	public String getDisplayName() {
		
		return displayName;
	}
	
	public static FREQUENCY fromNumber(int number) {

		for (FREQUENCY frequency : FREQUENCY.values()) {

			if (frequency.asNumber == number) {

				return frequency;
			}
		}

		throw new RuntimeException(number + " is not a valid FREQUENCY number");
	}
}
