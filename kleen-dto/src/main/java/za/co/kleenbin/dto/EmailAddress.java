package za.co.kleenbin.dto;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class EmailAddress {

	private String email;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getEncodedEmail() {
		
		try {
			return URLEncoder.encode(email, "UTF-8").replace("%", "=");
		} catch (UnsupportedEncodingException e) {
			
			throw new RuntimeException(e);
		}
	}

}
