package za.co.kleenbin.dto;

public class Address {

	private String addressLine1;
	private String street;
	private String surburb;
	private String code;
	private Double latitude;
	private Double longitude;

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getSurburb() {
		return surburb;
	}

	public void setSurburb(String surburb) {
		this.surburb = surburb;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDisplayAddress() {

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append(addressLine1).append(" ");
		stringBuilder.append(street).append(" ");
		stringBuilder.append(surburb).append(" ");
		stringBuilder.append(code);

		return stringBuilder.toString();
	}
	
	public String getShortDisplayAddress() {
		
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append(addressLine1).append(" ");
		stringBuilder.append(street).append(" ");
		stringBuilder.append(surburb);
		
		return stringBuilder.toString();
	}
}
