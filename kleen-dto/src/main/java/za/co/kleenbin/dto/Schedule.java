package za.co.kleenbin.dto;

import java.util.List;

public class Schedule {

    private List<AggregatedService> services;
    private int totalBins;
    private int totalServicedThisWeek;
    private int totalRequiringServiceThisWeek;
    private int totalClients;

    public List<AggregatedService> getServices() {
        return services;
    }

    public void setServices(List<AggregatedService> services) {
        this.services = services;
    }

    public int getTotalBins() {
        return totalBins;
    }

    public void setTotalBins(int totalBins) {
        this.totalBins = totalBins;
    }

    public int getTotalClients() {
        return totalClients;
    }

    public void setTotalClients(int totalClients) {
        this.totalClients = totalClients;
    }

    public int getTotalServicedThisWeek() {
        return totalServicedThisWeek;
    }

    public void setTotalServicedThisWeek(int totalServicedThisWeek) {
        this.totalServicedThisWeek = totalServicedThisWeek;
    }

    public int getTotalRequiringServiceThisWeek() {
        return totalRequiringServiceThisWeek;
    }

    public void setTotalRequiringServiceThisWeek(int totalRequiringServiceThisWeek) {
        this.totalRequiringServiceThisWeek = totalRequiringServiceThisWeek;
    }
}
