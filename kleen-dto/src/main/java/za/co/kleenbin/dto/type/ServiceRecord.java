package za.co.kleenbin.dto.type;

public enum ServiceRecord {

    BIN_UNREACHABLE, SERVICED, NONE
}
