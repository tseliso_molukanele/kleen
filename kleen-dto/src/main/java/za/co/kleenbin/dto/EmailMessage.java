package za.co.kleenbin.dto;

import java.util.List;

public class EmailMessage {

    private List<EmailAttachment> attachments;
    private String emailText;
    private String[] emails;
    private String subject;

    public void setEmailText(String emailText) {

        this.emailText = emailText;
    }

    public void setRecipients(String[] emails) {

        this.emails = emails;
    }

    public void setSubject(String subject) {

        this.subject = subject;
    }

    public void setAttachments(List<EmailAttachment> attachments) {

        this.attachments = attachments;
    }

    public List<EmailAttachment> getAttachments() {
        return attachments;
    }

    public String getEmailText() {
        return emailText;
    }

    public String[] getEmails() {
        return emails;
    }

    public String getSubject() {
        return subject;
    }

}
