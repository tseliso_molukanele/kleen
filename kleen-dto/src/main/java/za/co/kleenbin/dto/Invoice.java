package za.co.kleenbin.dto;

import java.util.Date;

public class Invoice {

	private Account account;
	private Date date;
	private Integer amount;
    private Float invoiceAmount;
    private Integer numberOfBins;
    private Integer serviceCount;
	private String reference;
	private Long id;

    public Float getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(Float invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public Integer getNumberOfBins() {
        return numberOfBins;
    }

    public void setNumberOfBins(Integer numberOfBins) {
        this.numberOfBins = numberOfBins;
    }

    public Integer getServiceCount() {
        return serviceCount;
    }

    public void setServiceCount(Integer serviceCount) {
        this.serviceCount = serviceCount;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getReference() {
		return reference;
	}

	public void setNumber(String reference) {
		this.reference = reference;
	}
	
	public String getNumber() {
	    return reference;
	}
	
	public void setReference(String reference) {
	    this.reference = reference;
	}

	@Override
	public String toString() {
		return "Payment [accountNumber=" + account.getAccountNumber()
				+ ", date=" + date + ", amount=" + amount + ", reference="
				+ reference + "]";
	}
}
