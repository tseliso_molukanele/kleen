package za.co.kleenbin.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import za.co.kleenbin.dto.type.AccountStatus;
import za.co.kleenbin.dto.type.AccountType;

public class Account {

	private Address address;
	private Person accountHolder;

	private String accountNumber;
	private Plan plan;
	private int dayOfCleaning;
	private Float balance;
	private String salesConsultant;
	private Date commencementDate;
	private AccountStatus status;
	private AccountType type;
	private Plan futurePlan;

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getDayOfCleaning() {
		return dayOfCleaning;
	}

	public void setDayOfCleaning(int dayOfCleaning) {
		this.dayOfCleaning = dayOfCleaning;
	}

	public Float getBalance() {
		return balance;
	}

	public void setBalance(Float balance) {
		this.balance = balance;
	}

	public String getSalesConsultant() {
		return salesConsultant;
	}

	public void setSalesConsultant(String salesConsultant) {
		this.salesConsultant = salesConsultant;
	}

	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getCommencementDate() {
		return commencementDate;
	}

	public void setCommencementDate(Date commencementDate) {
		this.commencementDate = commencementDate;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Person getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(Person accountHolder) {
		this.accountHolder = accountHolder;
	}

	public String getDisplayName() {

		return accountNumber + " - " + (accountHolder != null ? accountHolder.getDisplayName() : "") + " - " + (address != null ? address.getShortDisplayAddress() : "");
	}

	@Override
	public String toString() {
		return getDisplayName();
	}

	public String getPaymentReference() {
		return "JX" + String.format("%1$" + 3 + "s", accountNumber).replace(" ", "0");
	}

	public void setFuturePlan(Plan futurePlan) {
		this.futurePlan = futurePlan;
	}
	
	public Plan getFuturePlan() {
		return futurePlan;
	}
}
