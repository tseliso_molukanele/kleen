package za.co.kleenbin.dto.type;

public enum AccountActionType {
    
    HIGH_BALANCE("Balance is too high"), NO_EMAIL ("No email");        
    
    private String message;
    
    AccountActionType(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
}
