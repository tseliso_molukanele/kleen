package za.co.kleenbin.dto;

import java.util.Date;

import za.co.kleenbin.dto.type.ExpenseType;

public class Expense {

	private Date date;
	private Integer amount;
	private ExpenseType expenseType;
	private Long id;
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public ExpenseType getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(ExpenseType expenseType) {
		this.expenseType = expenseType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "Payment [accountNumber="
				+ ", date=" + date + ", amount=" + amount + ", expenseType="
				+ expenseType + "]";
	}
}
