package za.co.kleenbin.dto;

import java.util.Date;

public class ExpenseItem {

    private Date date;
    private Integer petrolAmount = 0;
    private Integer cleaningEquipmentAmount = 0;
    private Integer wagesAmount = 0;
    private Integer motorEquipmentAmount = 0;
    private Integer airtimeAmount = 0;
    private Integer otherAmount = 0;


    public Integer getPetrolAmount() {
        return petrolAmount;
    }

    public void setPetrolAmount(Integer petrolAmount) {
        this.petrolAmount = petrolAmount;
    }

    public Integer getOtherAmount() {
        return otherAmount;
    }

    public void setOtherAmount(Integer otherAmount) {
        this.otherAmount = otherAmount;
    }

    public Integer getAirtimeAmount() {
        return airtimeAmount;
    }

    public void setAirtimeAmount(Integer airtimeAmount) {
        this.airtimeAmount = airtimeAmount;
    }

    public Integer getMotorEquipmentAmount() {
        return motorEquipmentAmount;
    }

    public void setMotorEquipmentAmount(Integer motorEquipmentAmount) {
        this.motorEquipmentAmount = motorEquipmentAmount;
    }

    public Integer getWagesAmount() {
        return wagesAmount;
    }

    public void setWagesAmount(Integer wagesAmount) {
        this.wagesAmount = wagesAmount;
    }

    public Integer getCleaningEquipmentAmount() {
        return cleaningEquipmentAmount;
    }

    public void setCleaningEquipmentAmount(Integer cleaningEquipmentAmount) {
        this.cleaningEquipmentAmount = cleaningEquipmentAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
