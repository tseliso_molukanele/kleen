package za.co.kleenbin.dto;

public class EmailAttachment {

    private String sourceURL;
    private String mimeType;
    private String fileName;

    public void setSourceURL(String sourceURL) {

        this.sourceURL = sourceURL;
    }

    public void setMimeType(String mimeType) {

        this.mimeType = mimeType;
    }

    public void setFileName(String fileName) {

        this.fileName = fileName;
    }

    public String getSourceURL() {
        return sourceURL;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getFileName() {
        return fileName;
    }

}
