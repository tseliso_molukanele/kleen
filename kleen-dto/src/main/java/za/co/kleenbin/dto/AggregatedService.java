package za.co.kleenbin.dto;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import za.co.kleenbin.dto.type.FREQUENCY;
import za.co.kleenbin.dto.type.ServiceRecord;

public class AggregatedService {

	private Account account;
	private List<Service> services;

	public AggregatedService(Account account, List<Service> services) {

		this.account = account;
		this.services = services;
		Collections.sort(this.services, new Comparator<Service>() {

			@Override
			public int compare(Service o1, Service o2) {
				
				return (o1.getWeekStart().before(o2.getWeekStart()) ? -1 : 1);
			}
		});
	}
	
	public Boolean getWashDue() {

		if(services.get(services.size() - 1).getServiced()) {
			return false;
		}

		if(FREQUENCY.EVERY_WEEK.equals(account.getPlan().getFrequency())) {
			return true;
		}

		if(FREQUENCY.BI_WEEKLY.equals(account.getPlan().getFrequency()) &&
				!ServiceRecord.SERVICED.equals(services.get(services.size() - 2).getServiceRecord())) {
			return true; 
		}

		if(FREQUENCY.ONCE_PER_MONTH.equals(account.getPlan().getFrequency()) 
				&& !ServiceRecord.SERVICED.equals(services.get(services.size() - 2).getServiceRecord())
				&& !ServiceRecord.SERVICED.equals(services.get(services.size() - 3).getServiceRecord())
				&& !ServiceRecord.SERVICED.equals(services.get(services.size() - 4).getServiceRecord())) {
			return true; 
		}
		
		return false;
	}
	
	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

}
