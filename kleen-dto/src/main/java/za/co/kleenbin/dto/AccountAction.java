package za.co.kleenbin.dto;

import org.joda.time.DateTime;

import za.co.kleenbin.dto.type.AccountActionType;

public class AccountAction {
    
    private Account account;
    private AccountActionType accountActionType;
    private DateTime createdDate;
    
    public void setAccount(Account account) {
        this.account = account;
    }
    
    public Account getAccount() {
        return account;
    }

    public void setType(AccountActionType type) {
        this.accountActionType = type;
    }
    
    public AccountActionType getType() {
        return accountActionType;
    }
    
    public String getMessage() {
        return accountActionType.getMessage();
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }
    
    public DateTime getCreatedDate() {
        return createdDate;
    }
}
