package za.co.kleenbin.dto;

import java.util.Date;

public class IncomeItem {

    private Date date;
    private Integer incomeAmount;
    private Integer cashIncomeAmount;
    private Integer bankIncomeAmount;
    private Integer preInvoiceAmount;
    private Integer postInvoiceAmount;
	private Integer preDateAmount;
	private Integer postDateAmount;

    public Integer getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(Integer incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCashIncomeAmount() {
        return cashIncomeAmount;
    }

    public void setCashIncomeAmount(Integer cashIncomeAmount) {
        this.cashIncomeAmount = cashIncomeAmount;
    }

    public Integer getBankIncomeAmount() {
        return bankIncomeAmount;
    }

    public void setBankIncomeAmount(Integer bankIncomeAmount) {
        this.bankIncomeAmount = bankIncomeAmount;
    }

    public Integer getPreInvoiceAmount() {
        return preInvoiceAmount;
    }

    public void setPreInvoiceAmount(Integer preInvoiceAmount) {
        this.preInvoiceAmount = preInvoiceAmount;
    }

    public Integer getPostInvoiceAmount() {
        return postInvoiceAmount;
    }

    public void setPostInvoiceAmount(Integer postInvoiceAmount) {
        this.postInvoiceAmount = postInvoiceAmount;
    }

	public void setPostDateAmount(Integer postDateAmount) {
		this.postDateAmount = postDateAmount;
	}
	
	public Integer getPostDateAmount() {
		return postDateAmount;
	}

	public void setPreDateAmount(Integer preDateAmount) {
		this.preDateAmount = preDateAmount;
	}
	
	public Integer getPreDateAmount() {
		return preDateAmount;
	}
}
