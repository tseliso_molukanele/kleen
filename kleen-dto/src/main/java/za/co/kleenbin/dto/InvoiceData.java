package za.co.kleenbin.dto;

public class InvoiceData {

	private long binCount;
	private double income;
	private double royalty;
		
	public long getBinCount() {
		return binCount;
	}
	public void setBinCount(long binCount) {
		this.binCount = binCount;
	}
	public double getIncome() {
		return income;
	}
	public void setIncome(double income) {
		this.income = income;
	}
	
	public double getRoyalty() {
		return royalty;
	}
	
	public void setRoyalty(double royalty) {
		this.royalty = royalty;
	}
	
	
}
