package za.co.kleenbin.dto;

import java.util.Date;

import za.co.kleenbin.dto.type.ServiceRecord;

public class Service {

    private Account account;
    private Date date;
    private Date weekStart;
    private Date weekEnd;
    private Boolean serviced;
    private ServiceRecord serviceRecord;

    public Boolean getServiced() {
        return serviced;
    }

    public void setServiced(Boolean serviced) {
        this.serviced = serviced;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getWeekEnd() {
        return weekEnd;
    }

    public Date getWeekStart() {
        return weekStart;
    }

    public void setWeekEnd(Date weekEnd) {
        this.weekEnd = weekEnd;
    }

    public void setWeekStart(Date weekStart) {
        this.weekStart = weekStart;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ServiceRecord getServiceRecord() { return serviceRecord; }

    public void setServiceRecord(ServiceRecord serviceRecord) { this.serviceRecord = serviceRecord; }

    @Override
    public String toString() {
        return "Service [account=" + account + ", date=" + date + ", weekStart=" + weekStart + ", weekEnd=" + weekEnd + ", serviced=" + serviced + "]";
    }
}
