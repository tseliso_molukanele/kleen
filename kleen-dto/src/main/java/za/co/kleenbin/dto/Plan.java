package za.co.kleenbin.dto;

import za.co.kleenbin.dto.type.FREQUENCY;

public class Plan {

    private Integer numberOfBins;
    private FREQUENCY frequency;
    private Integer price;
    private Integer year;
    private Long id;
    
    public Long getId() {
		return id;
	}
    
    public void setId(Long id) {
		this.id = id;
	}

    public Integer getNumberOfBins() {
        return numberOfBins;
    }

    public void setNumberOfBins(Integer numberOfBins) {
        this.numberOfBins = numberOfBins;
    }

    public FREQUENCY getFrequency() {
        return frequency;
    }

    public void setFrequency(FREQUENCY frequency) {
        this.frequency = frequency;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
    
    public void setYear(Integer year) {
		this.year = year;
	}
    
    public Integer getYear() {
		return year;
	}

    public String getDisplayName() {
    	return numberOfBins + " bins " + frequency + " @ " + price + " per wash in " + year;
    }
    
    public Integer monthly() {
    	return numberOfBins * frequency.asNumber() * price;
    }
    
    public Integer getMonthly() {
    	
    	return monthly();
    }
    
    @Override
    public String toString() {
    	return super.toString();
    }
}
