package za.co.kleenbin.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Contactable {

	private static final Pattern EMAIL_PATTERN = Pattern
			.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

	private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("^0[0-9]{9}$");

	private List<PhoneNumber> phoneNumbers = new ArrayList<>();
	private List<EmailAddress> emailAddresses = new ArrayList<>();

	public void setEmailAddresses(List<EmailAddress> emailAddresses) {
		this.emailAddresses = emailAddresses;
	}

	public List<EmailAddress> getEmailAddresses() {

		if(emailAddresses == null) {
			return null;
		}
		
		emailAddresses = emailAddresses.stream()
				.filter(m -> m.getEmail() != null && EMAIL_PATTERN.matcher(m.getEmail()).matches())
				.collect(Collectors.toList());

		return emailAddresses;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		
		if(phoneNumbers == null) {
			return null;
		}
		
		phoneNumbers = phoneNumbers.stream()
				.filter(n -> n.getNumber() != null && PHONE_NUMBER_PATTERN.matcher(n.getNumber()).matches())
				.collect(Collectors.toList());

		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
}
