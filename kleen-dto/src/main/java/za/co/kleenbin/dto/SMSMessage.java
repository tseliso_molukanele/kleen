package za.co.kleenbin.dto;

import java.util.Date;

public class SMSMessage {

        private Date dateToSend;
	private String number;
	private String message;
	private String customerId;

	public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }
	
	public void setNumber(String number) {
            this.number = number;
        }
	
	public String getNumber() {
		return number;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setMessage(String message) {
            this.message = message;
        }
	
	public String getMessage() {
		return message;
	}

	public Date getDateToSend() {
            return dateToSend;
        }
	
	public void setDateToSend(Date dateToSend) {
            this.dateToSend = dateToSend;
        }
	
	@Override
	public String toString() {
		
		return number + "~" + message + "~" + customerId;
	}
}
