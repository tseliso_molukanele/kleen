package za.co.kleenbin.service;

import java.util.ArrayList;

import org.dozer.DozerBeanMapper;

import za.co.kleen.dao.entity.EmailAddress;
import za.co.kleen.dao.entity.Person;
import za.co.kleen.dao.entity.PhoneNumber;
import za.co.kleen.dao.entity.type.NumberType;

public class PersonBuilder {
	
	private String cellNumber;
	private String email;
	private String title;
	private String surname;

	static PersonBuilder aPerson() {
		
		return new PersonBuilder();
	}
	
	PersonBuilder withCell(String cellNumber) {
		
		this.cellNumber = cellNumber;
		return this;
	}
	
	PersonBuilder withEmail(String email) {
		
		this.email = email;
		return this;
	}
	
	Person build() {
		
		Person person = new Person();
		person.setFirstName("Philemon");
        person.setLastName("Mvubu");
        person.setTitle("Dr");
        person.setPhoneNumbers(new ArrayList<PhoneNumber>());
        person.setEmailAddresses(new ArrayList<EmailAddress>());
		
		if(cellNumber != null) {
			PhoneNumber phone = new PhoneNumber();
            phone.setNumber(this.cellNumber);
            phone.setNumberType(NumberType.CELL);
            person.getPhoneNumbers().add(phone);
		}
		
		if(email != null) {
			EmailAddress address = new EmailAddress();
            address.setEmail(this.email);
            person.getEmailAddresses().add(address);
		}
		
		person.setTitle(title);
		person.setLastName(surname);
		
		return person;
	}

	public PersonBuilder withTitle(String title) {
		
		this.title = title;
		
		return this;
	}

	public PersonBuilder withSurname(String surname) {
		
		this.surname = surname;
		
		return this;
	}

	public za.co.kleenbin.dto.Person buildConverted() {
		return convert(this.build());
	}
	
	public za.co.kleenbin.dto.Person convert(Person person) {

		return new DozerBeanMapper().map(person, za.co.kleenbin.dto.Person.class);
	}
}