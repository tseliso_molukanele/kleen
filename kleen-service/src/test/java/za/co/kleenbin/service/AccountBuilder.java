package za.co.kleenbin.service;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.Address;
import za.co.kleen.dao.entity.Invoice;
import za.co.kleen.dao.entity.Person;
import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.entity.type.AccountStatus;

public class AccountBuilder {

	private String accountNumber;
	private Person accountHolder;
	private Plan plan;
	private Plan futurePlan;
	private AccountStatus accountStatus;
	private List<Invoice> invoices = new ArrayList<>();
	private float balance;
	private Address address;

	public static AccountBuilder anAccount() {

		return new AccountBuilder();
	}

	public AccountBuilder withFuturePlan(Plan futurePlan) {

		this.futurePlan = futurePlan;
		return this;
	}

	AccountBuilder withPlan(Plan plan) {

		this.plan = plan;
		return this;
	}

	AccountBuilder withAccountNumber(String accountNumber) {

		this.accountNumber = accountNumber;
		return this;
	}

	AccountBuilder withAccountHolder(Person accountHolder) {

		this.accountHolder = accountHolder;
		return this;
	}

	Account build() {

		Account account = spy(new Account());
		account.setAccountNumber(this.accountNumber);
		account.setPlan(this.plan);
		account.setFuturePlan(this.futurePlan);
		account.setAccountHolder(this.accountHolder);
		when(account.getBalance()).thenReturn(this.balance);
		account.setInvoices(invoices);
		account.setStatus(accountStatus);
		account.setAddress(address);

		return account;
	}

	public AccountBuilder withStatus(AccountStatus accountStatus) {

		this.accountStatus = accountStatus;
		return this;
	}

	public za.co.kleenbin.dto.Account convert(Account account) {

		return new DozerBeanMapper().map(account, za.co.kleenbin.dto.Account.class);
	}

	public za.co.kleenbin.dto.Account buildConverted() {
		return convert(this.build());
	}

	public AccountBuilder withInvoices(Integer numberOfInvoices, Integer amount) {

		for (int i = 0; i < numberOfInvoices; i++) {

			Invoice invoice = new Invoice();
			invoice.setAmount(amount);

			invoices.add(invoice);
		}

		return this;
	}

	public AccountBuilder withBalance(Float balance) {
		
		this.balance = balance;
		
		return this;
	}

	public AccountBuilder withAddress(za.co.kleen.dao.entity.Address address) {
		
		this.address = address;
		
		return this;
	}
}