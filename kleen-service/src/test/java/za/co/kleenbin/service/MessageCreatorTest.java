package za.co.kleenbin.service;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.kleenbin.service.exception.IncompleteAccountException;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@RunWith(MockitoJUnitRunner.class)
public class MessageCreatorTest {

	@Spy
	private VelocityEngine velocityEngine = new VelocityEngine();
	
	@Before
	public void setup() {		
		
		velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		velocityEngine.init();
	}

	@Test
	public void should_produce_invoice_sms_with_balance_of_2010() throws IOException {

		//
		DateTime thisMonth = new DateTime();
		DateTime nextMonth = thisMonth.plusMonths(1);
		
		//
		String message = new MessageCreator(MessageCreatorType.INVOICE_SMS, velocityEngine)
				.createMessage(AccountBuilder.anAccount().withBalance(2010f).withAccountNumber("324").buildConverted());
		
		//
		assertEquals(
				"KleenBinFG@gmail.com :) Balance for " + thisMonth.toString("MMM") + " is R2010.00 payable by 7th " + nextMonth.toString("MMM yyyy") + " to: Acc name:Be For Sunset, STD Bank Branch:011545, Acc num:012652555, Ref:JX324. Payment may take 7 days to reflect",
				message );
	}
	
	@Test
	public void should_produce_invoice_sms_with_balance_of_200() throws IOException {

		//
		DateTime thisMonth = new DateTime();
		DateTime nextMonth = thisMonth.plusMonths(1);
		
		//
		String message = new MessageCreator(MessageCreatorType.INVOICE_SMS, velocityEngine).createMessage(
				AccountBuilder.anAccount()
					.withBalance(200f)
					.withAccountNumber("324")
				.buildConverted());
		
		//
		assertEquals(
				"KleenBinFG@gmail.com :) Balance for " + thisMonth.toString("MMM") + " is R200.00 payable by 7th " + nextMonth.toString("MMM yyyy") + " to: Acc name:Be For Sunset, STD Bank Branch:011545, Acc num:012652555, Ref:JX324. Payment may take 7 days to reflect",
				message );
	}
	
	@Test
	public void should_produce_missed_service_sms_for_default_client() throws IOException {
		
		//
		
		//
		String message = new MessageCreator(MessageCreatorType.MISSED_SERVICE_SMS, velocityEngine)
				.createMessage(AccountBuilder.anAccount().withAccountHolder(PersonBuilder.aPerson().withTitle("Dr").withSurname("Mvubu").build()).buildConverted());
		
		//
		assertEquals(" KleenBinFG@gmail.com :) Dear  Dr Mvubu  Please be notified that our service men could not service your bin as it was un-reachable",
				message);
	}

	@Test
	public void should_produce_missed_service_sms_for_mr_owen() throws IOException {
		
		//
		
		//
		String message = new MessageCreator(MessageCreatorType.MISSED_SERVICE_SMS, velocityEngine)
				.createMessage(AccountBuilder.anAccount().withAccountHolder(PersonBuilder.aPerson().withTitle("Mr").withSurname("Owen").build()).buildConverted());
		
		//
		assertEquals(" KleenBinFG@gmail.com :) Dear  Mr Owen  Please be notified that our service men could not service your bin as it was un-reachable",
				message);
	}
	
	@Test(expected=IncompleteAccountException.class)
	public void expects_incompleteAccountException_when_account_has_no_plan() throws IOException {
		
		//
		
		//
		new MessageCreator(MessageCreatorType.PENDING_INCREASE_SMS, velocityEngine)
				.createMessage(AccountBuilder.anAccount().buildConverted());
		
	}
	
	@Test(expected=IncompleteAccountException.class)
	public void expects_incompleteAccountException_when_account_has_plan_but_no_futureplan() throws IOException {
		
		//
		
		//
		new MessageCreator(MessageCreatorType.PENDING_INCREASE_SMS, velocityEngine)
				.createMessage(AccountBuilder.anAccount().withPlan(PlanBuilder.aPlan().build()).buildConverted());
		
	}

	
}
