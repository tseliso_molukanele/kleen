package za.co.kleenbin.service.callaction;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.type.AccountActionType;
import za.co.kleen.dao.repository.AccountActionRepository;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleenbin.conf.TestConfig;
import za.co.kleenbin.dto.AccountAction;

@RunWith(MockitoJUnitRunner.class)
@ContextHierarchy({ @ContextConfiguration(classes = { TestConfig.class }) })
public class DBBasedProviderTest {

    @Test
    public void test_notEnoughInRepo() {

        //
        int inRepo = 2;
        int fetchSize = 5;

        testForSize(inRepo, fetchSize);
    }

    @Test
    public void test_enoughInRepo() {

        //
        int inRepo = 5;
        int fetchSize = 5;

        testForSize(inRepo, fetchSize);
    }

    @Test
    public void test_moreThenEnoughInRepo() {

        //
        int inRepo = 6;
        int fetchSize = 5;

        testForSize(inRepo, fetchSize);
    }

    private void testForSize(int inRepo, int fetchSize) {

        //
        DBBasedProvider provider = prepareMocks(inRepo, fetchSize, AccountActionType.HIGH_BALANCE);

        //
        List<AccountAction> actions = provider.getNextActions(fetchSize);

        Mockito.verify(accountActionRepository, Mockito.atMost(Math.max(0, fetchSize - inRepo))).save(Matchers.any(za.co.kleen.dao.entity.AccountAction.class));

        //
        assertEquals(fetchSize, actions.size());
    }
    @Mock
    private AccountRepository accountRepository;
    
    @Mock
    private AccountActionRepository accountActionRepository;
    
    @Mock
    private DozerBeanMapper dozerBeanMapper;

    @InjectMocks
    private DBBasedProvider provider = new DBBasedProvider() {
        
        @Override
        public int getSpotsRequired() {
            return 10;
        }
        
        @Override
        protected AccountActionType getType() {
            return AccountActionType.HIGH_BALANCE;
        }
        
        @Override
        protected List<Account> backlog(int fetchSize) {
            
            List<Account> backlog = new ArrayList<Account>();
            
            for (int i = 0; i < fetchSize; i++) {
                
                backlog.add(new Account());
            }
            
            return backlog;
        }
    };
    
    private DBBasedProvider prepareMocks(final int inRepo, final int fetchSize, final AccountActionType type) {

        za.co.kleen.dao.entity.AccountAction savedAccountAction = new za.co.kleen.dao.entity.AccountAction();
        savedAccountAction.setAccount(new Account());
        savedAccountAction.setType(type);
        Mockito.when(accountActionRepository.save(Matchers.any(za.co.kleen.dao.entity.AccountAction.class))).thenReturn(savedAccountAction);

        List<za.co.kleen.dao.entity.AccountAction> repoActions = new ArrayList<za.co.kleen.dao.entity.AccountAction>();

        for (int i = 0; i < Math.min(inRepo, fetchSize); i++) {

            za.co.kleen.dao.entity.AccountAction accountAction = new za.co.kleen.dao.entity.AccountAction();
            accountAction.setAccount(new Account());
            accountAction.setType(type);

            repoActions.add(accountAction);
        }

        System.out.println("adding repoActions.size(): " + repoActions.size());

        Mockito.when(accountActionRepository.findByType(Matchers.eq(type), Matchers.argThat(new HasSameLimit(fetchSize)))).thenReturn(repoActions);

        return provider;
    }

    class HasSameLimit extends ArgumentMatcher<PageRequest> {

        private int limit;

        public HasSameLimit(int limit) {

            this.limit = limit;
        }

        public boolean matches(Object other) {

            if (!(other instanceof PageRequest)) {
                return false;
            }

            return ((PageRequest) other).getPageSize() == limit;
        }
    }

}
