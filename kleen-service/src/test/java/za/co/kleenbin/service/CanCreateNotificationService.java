package za.co.kleenbin.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

public class CanCreateNotificationService {

	public CanCreateNotificationService() {
		super();
	}

	public NotificationService getErrorReportingService(Client messagingClient, ObjectMapper mapper, String errorEmailQueue, String errorEmail) {
		NotificationService notificationService = new NotificationService();
		notificationService.setErrorEmail(errorEmail);
		notificationService.setMessagingClient(messagingClient);
		notificationService.setEmailQueue(errorEmailQueue);
		notificationService.setMapper(mapper);
		return notificationService;
	}

}