package za.co.kleenbin.service;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.joda.time.DateTime;
import org.junit.Test;

public class SMSSenderTest {

	private static final DateTime MON29_10 = new DateTime(2016, 8, 29, 10, 0);
	private static final DateTime TUE30_6 =  new DateTime(2016, 8, 30,  6, 0);
	private static final DateTime WED31_21 = new DateTime(2016, 8, 31, 21, 0);
	private static final DateTime FRI2_6 =   new DateTime(2016, 9,  2,  6, 0);
	private static final DateTime FRI2_10 =  new DateTime(2016, 9,  2, 10, 0);
	private static final DateTime FRI2_21 =  new DateTime(2016, 9,  2, 21, 0);
	
	private static final DateTime TUE30_8 = new DateTime(2016, 8, 30, 8, 0);
	private static final DateTime THU1_8 =  new DateTime(2016, 9, 1,  8, 0);
	private static final DateTime FRI2_8 =  new DateTime(2016, 9, 2,  8, 0);
	private static final DateTime MON5_8 =  new DateTime(2016, 9, 5,  8, 0);
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd HH mm");
	
	@Test
	public void test_mon29_10() {
		
		test(MON29_10, MON29_10);
	}
	
	@Test
	public void test_tue30_6() {
		
		test(TUE30_6, TUE30_8);
	}
	
	@Test
	public void test_wed31_21() {
		
		test(WED31_21, THU1_8);
	}
	
	@Test
	public void test_fri2_6() {
		
		test(FRI2_6, FRI2_8);
	}
	
	@Test
	public void test_fri2_10() {
		
		test(FRI2_10, FRI2_10);
	}
	
	@Test
	public void test_fri2_21() {
		
		test(FRI2_21, MON5_8);
	}

	private void test(DateTime input, DateTime output) {
		//
		SMSSender sender = new SMSSender();
		
		//
		Calendar time = sender.getSuitableTimeToSend(input.toDate());
		
		//
		assertEquals(dateFormat.format(output.toDate()), dateFormat.format(time.getTime()));
	}

}
