package za.co.kleenbin.service;

import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Field;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.service.exception.ClientCommunicationNotPossibleException;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorFactory;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

	@InjectMocks
	private NotificationService notificationService;

	@Mock
	private Client messagingClient;
	
	@Mock 
	private MessageCreatorFactory messageCreatorFactory;
	
	@Mock
	private SMSSender smsSender;
	
	@Spy
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private String emailQueue = "emailQueue";
	
	@Before
	public void setup() throws NoSuchFieldException, SecurityException {
		
		Field emailQField = notificationService.getClass().getDeclaredField("emailQueue");
		
		emailQField.setAccessible(true);
		
		ReflectionUtils.setField(emailQField, notificationService, emailQueue);
	}
	
	@Test
	public void should_send_SMS_given_no_email() throws IOException {
		
		//
		String message = "message send via SMS";
		Account account = 
				AccountBuilder.anAccount()
					.withAccountHolder(
							PersonBuilder.aPerson()
								.withCell("0710010001")
							.build())
				.buildConverted();
		
		MessageCreator messageCreator = mock(MessageCreator.class);
		when(messageCreator.createMessage(account)).thenReturn(message);
		
		when(messageCreatorFactory.get(MessageCreatorType.NEW_ACCOUNT_SMS))
		.thenReturn(messageCreator);
		
		//
		notificationService.sendMessage(account, MessageCreatorType.NEW_ACCOUNT_SMS, 
				MessageCreatorType.NEW_ACCOUNT_EMAIL);
		
		//
		verify(smsSender, times(1)).sendSMS(account, MessageCreatorType.NEW_ACCOUNT_SMS);
	}

	@Test
	public void should_send_email_given_email() throws IOException {

		//
		String message = "message send via EMAIL";
		Account account = 
				AccountBuilder.anAccount()
					.withAccountHolder(
							PersonBuilder.aPerson()
							.withEmail("blah@email.com")
							.build())
				.buildConverted();
		
		MessageCreator messageCreator = mock(MessageCreator.class);
		when(messageCreator.createMessage(account)).thenReturn(message);
		
		when(messageCreatorFactory.get(MessageCreatorType.NEW_ACCOUNT_EMAIL))
		.thenReturn(messageCreator);
		
		//
		notificationService.sendMessage(account, MessageCreatorType.NEW_ACCOUNT_SMS, 
				MessageCreatorType.NEW_ACCOUNT_EMAIL);
		
		//
		verify(messagingClient, times(1)).send(eq(emailQueue), contains(message));
	}

	@Test(expected = ClientCommunicationNotPossibleException.class)
	public void expecting_an_exception_given_no_mail_no_sms() throws IOException {

		//
		Account account = AccountBuilder.anAccount().withAccountHolder(PersonBuilder.aPerson().build()).buildConverted();
		
		//
		notificationService.sendMessage(account, 
				MessageCreatorType.NEW_ACCOUNT_SMS, 
				MessageCreatorType.NEW_ACCOUNT_EMAIL);
		
		//
	}

}
