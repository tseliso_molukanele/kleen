package za.co.kleenbin.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.kleenbin.dto.Person;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

	@InjectMocks
	private PersonService service;
	
	@Test
	public void should_return_false_if_not_contactable() {

		//
		Person person = PersonBuilder.aPerson().buildConverted();
		
		//
		boolean isContactable = service.isContactable(person);
		
		//
		assertFalse(isContactable);
	}
	
	@Test
	public void should_return_true_if_theres_a_cell_number() {
	
		
		Person person = PersonBuilder.aPerson().withCell("0721155454").buildConverted();
		//
		boolean isContactable = service.isContactable(person);
		
		//
		assertTrue(isContactable);
	}
	
	@Test
	public void should_return_true_if_theres_a_email() {
		
		Person person = PersonBuilder.aPerson().withEmail("mail@email.com").buildConverted();
		
		//
		boolean isContactable = service.isContactable(person);
		
		//
		assertTrue(isContactable);
		
	}
}
