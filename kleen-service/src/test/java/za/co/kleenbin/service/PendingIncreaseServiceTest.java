package za.co.kleenbin.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static za.co.kleenbin.service.PendingActionService.PROPRIOTER_ACCOUNT_NUMBER;
import static za.co.kleenbin.service.PendingActionService.PROPRIOTER_ACTION_ACTIVATION_DATE;
import static za.co.kleenbin.service.PendingActionService.PROPRIOTER_PRIMARY_TYPE;
import static za.co.kleenbin.service.PendingActionService.PROPRIOTER_STATUS;
import static za.co.kleenbin.service.PendingActionService.VALUE_ACTIVE;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleen.dao.entity.Proprietor;
import za.co.kleen.dao.repository.ProprietorRepository;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.service.exception.NoFuturePlanException;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@RunWith(MockitoJUnitRunner.class)
public class PendingIncreaseServiceTest extends CanCreateNotificationService {

	@Mock
	NotificationService notificationService;

	@Mock
	ProprietorRepository proprietorRepository;

	@Mock
	PendingIncreaseService pendingIncreaseService;

	@Mock
	AccountService accountService;

	@InjectMocks
	PendingIncreaseService service;

	@Mock
	private SMSSender smsSender;

	@Mock
	private Client messagingClient;

	@Mock
	private VelocityEngine velocityEngine;

	@Spy
	private ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {

		when(velocityEngine.getTemplate(any(String.class))).thenReturn(mock(Template.class));
	}

	@Test(expected = NoFuturePlanException.class)
	public void expect_runtime_exception_and_error_email_when_an_account_has_no_future_plan() {

		String accountNumber = "001";
		List<Proprietor> propriotors = new ArrayList<>();
		Proprietor proprietor = new Proprietor();
		proprietor.getProperties().put(PROPRIOTER_ACCOUNT_NUMBER, accountNumber);
		proprietor.getProperties().put(PROPRIOTER_PRIMARY_TYPE, "PendingIncrease");
		proprietor.getProperties().put(PROPRIOTER_ACTION_ACTIVATION_DATE,
				new DateTime().plusDays(-1).toString("dd MMM yyyy"));
		propriotors.add(proprietor);

		//
		when(proprietorRepository.findByKeyValue(PROPRIOTER_STATUS, VALUE_ACTIVE)).thenReturn(propriotors);

		doReturn(AccountBuilder.anAccount().withAccountNumber(accountNumber)
				.withStatus(za.co.kleen.dao.entity.type.AccountStatus.ACTIVE).buildConverted()).when(accountService)
						.getAccount(accountNumber);

		when(accountService.upgradePricePlan(accountNumber)).thenThrow(NoFuturePlanException.class);

		//
		service.increaseRun();

		//
	}

	@Test
	public void testReminder_NoAccountsAtAll() {

		ProprietorRepository proprietorRepository = mock(ProprietorRepository.class);
		Client messagingClient = mock(Client.class);
		AccountService accountService = mock(AccountService.class);

		when(accountService.getEligableForPriceIncrease()).thenReturn(new ArrayList<Account>());

		PendingIncreaseService service = new PendingIncreaseService();
		service.setProprietorRepository(proprietorRepository);
		service.setMessagingClient(messagingClient);
		service.setAccountService(accountService);

		//
		service.reminderRun();

		//

		verify(proprietorRepository, never()).save(any(Proprietor.class));
		verify(messagingClient, never()).send(any(String.class), any(String.class));
		verifyNoMoreInteractions(proprietorRepository, messagingClient);

	}

	@Test
	public void testReminder_1SMS1Email() throws IOException {

		ArrayList<Account> accounts = new ArrayList<Account>();
		accounts.add(
				AccountBuilder.anAccount()
				.withAccountNumber("101")
				.withPlan(PlanBuilder.aPlan().build())
				.withFuturePlan(PlanBuilder.aPlan().build())
				.withAccountHolder(
						PersonBuilder.aPerson().withCell("0711140" + "101").build())
				.withAddress(mock(za.co.kleen.dao.entity.Address.class))
				.buildConverted());
		
		accounts.add(AccountBuilder.anAccount()
				.withAccountNumber("102")
				.withPlan(PlanBuilder.aPlan().build())
				.withFuturePlan(PlanBuilder.aPlan().build())
				.withAccountHolder(
						PersonBuilder.aPerson().withEmail("102" + "@email.com").build())
				.withAddress(mock(za.co.kleen.dao.entity.Address.class))
				.buildConverted());
		
		when(accountService.getEligableForPriceIncrease()).thenReturn(accounts);

		//
		service.reminderRun();

		//
		verify(proprietorRepository, times(1)).save(argThat(new IsProprioterWithProperties(getProperties("101"))));
		verify(smsSender, times(1)).sendSMS(any(Account.class), eq(MessageCreatorType.PENDING_INCREASE_SMS));

		verify(proprietorRepository, times(1)).save(argThat(new IsProprioterWithProperties(getProperties("102"))));
		verify(messagingClient, times(1)).send(any(String.class), contains("Pending Account Increase for 102"));

		verify(notificationService, times(1)).sendEmail("Accounts Marked For Increase Today", "2 accounts processed");

	}

	@Test
	public void should_notify_1_account_given_one_is_ACTIVE_another_is_OTHER_and_another_is_has_no_properties()
			throws IOException {

		//
		String accountNotNotified = "101";
		String accountAlreadyNotified = "102";
		String accountAlreadyNotifiedWithOtherStatus = "103";

		List<Proprietor> proprietorsForActive = new ArrayList<>();
		proprietorsForActive.add(getPropriotor(PendingIncreaseService.PROPRIOTER_ACCOUNT_NUMBER, accountAlreadyNotified,
				PendingIncreaseService.PROPRIOTER_STATUS, "Active", PendingIncreaseService.PROPRIOTER_PRIMARY_TYPE,
				"PendingIncrease"));

		List<Proprietor> proprietorsForOtherStatus = new ArrayList<>();
		proprietorsForOtherStatus.add(getPropriotor(PendingIncreaseService.PROPRIOTER_ACCOUNT_NUMBER,
				accountAlreadyNotifiedWithOtherStatus, PendingIncreaseService.PROPRIOTER_STATUS, "Other",
				PendingIncreaseService.PROPRIOTER_PRIMARY_TYPE, "PendingIncrease"));

		when(proprietorRepository.findByKeyValue(PendingIncreaseService.PROPRIOTER_ACCOUNT_NUMBER,
				accountAlreadyNotifiedWithOtherStatus)).thenReturn(proprietorsForOtherStatus);
		when(proprietorRepository.findByKeyValue(PendingIncreaseService.PROPRIOTER_ACCOUNT_NUMBER,
				accountAlreadyNotified)).thenReturn(proprietorsForActive);

		ArrayList<Account> accounts = new ArrayList<Account>();
		accounts.add(AccountBuilder.anAccount().withAccountNumber(accountNotNotified)
				.withAccountHolder(PersonBuilder.aPerson().withCell("0711140" + accountNotNotified).build())
				.buildConverted());
		accounts.add(AccountBuilder.anAccount().withAccountNumber(accountAlreadyNotified)
				.withAccountHolder(PersonBuilder.aPerson().withCell("0711140" + accountAlreadyNotified).build())
				.buildConverted());
		accounts.add(AccountBuilder.anAccount().withAccountNumber(accountAlreadyNotifiedWithOtherStatus)
				.withAccountHolder(
						PersonBuilder.aPerson().withCell("0711140" + accountAlreadyNotifiedWithOtherStatus).build())
				.buildConverted());
		when(accountService.getEligableForPriceIncrease()).thenReturn(accounts);

		//
		service.reminderRun();

		//
		verify(proprietorRepository, times(1))
				.save(argThat(new IsProprioterWithProperties(getProperties(accountNotNotified))));
		verify(smsSender, times(1)).sendSMS(any(Account.class), eq(MessageCreatorType.PENDING_INCREASE_SMS));

		verify(notificationService, times(1)).sendEmail("Accounts Marked For Increase Today", "1 accounts processed");

	}

	@Test
	public void testActioning_NoAccountsAtAll() {

		//
		service.increaseRun();

		//

		verify(proprietorRepository, never()).save(any(Proprietor.class));
		verify(messagingClient, never()).send(any(String.class), any(String.class));
		verify(proprietorRepository, times(1)).findByKeyValue(PendingIncreaseService.PROPRIOTER_STATUS,
				PendingIncreaseService.VALUE_ACTIVE);
		verifyNoMoreInteractions(proprietorRepository, messagingClient);

	}

	@Test
	public void should_ignore_account_with_OTHER_status_given_1_account_with_OTHER_status() {

		//
		String accountNumber = "101";
		Account account = AccountBuilder.anAccount().withAccountNumber(accountNumber)
		.withAccountHolder(PersonBuilder.aPerson().withEmail(accountNumber + "@email.com").build())
		.buildConverted();

		AccountService accountService = mock(AccountService.class);
		when(accountService.getAccount(accountNumber)).thenReturn(account);

		List<Proprietor> propriotors = new ArrayList<>();
		propriotors.add(getPropriotor(PendingIncreaseService.PROPRIOTER_ACCOUNT_NUMBER, accountNumber,
				PendingIncreaseService.PROPRIOTER_PRIMARY_TYPE, PendingIncreaseService.VALUE_PENDING_INCREASE,
				PendingIncreaseService.PROPRIOTER_STATUS, "OTHER",
				PendingIncreaseService.PROPRIOTER_ACTION_ACTIVATION_DATE,
				new SimpleDateFormat("dd MMM yyy").format(new DateTime().minusDays(1).toDate())));

		ProprietorRepository proprietorRepository = mock(ProprietorRepository.class);

		PendingIncreaseService service = spy(new PendingIncreaseService());
		service.setAccountService(accountService);
		service.setProprietorRepository(proprietorRepository);
		service.setNotificationService(mock(NotificationService.class));
		service.setMessagingClient(mock(Client.class));
		service.setMapper(new ObjectMapper());

		//
		service.increaseRun();

		//
		verify(service, never()).doAction(account);
	}

	@Test
	public void should_action_1_active_account_when_theres_one_active_account() throws IOException {

		String accountNumber = "101";

		Account account = AccountBuilder.anAccount()
				.withStatus(za.co.kleen.dao.entity.type.AccountStatus.ACTIVE)
				.withAddress(mock(za.co.kleen.dao.entity.Address.class))
				.withAccountNumber(accountNumber)
				.withAccountHolder(PersonBuilder.aPerson().withEmail(accountNumber + "@email.com").build())
		.buildConverted();
		
		when(accountService.getAccount(accountNumber)).thenReturn(account);

		List<Proprietor> propriotors = new ArrayList<>();
		propriotors.add(getPropriotor(PendingIncreaseService.PROPRIOTER_ACCOUNT_NUMBER, accountNumber,
				PendingIncreaseService.PROPRIOTER_PRIMARY_TYPE, PendingIncreaseService.VALUE_PENDING_INCREASE,
				PendingIncreaseService.PROPRIOTER_STATUS, PendingIncreaseService.VALUE_ACTIVE,
				PendingIncreaseService.PROPRIOTER_ACTION_ACTIVATION_DATE,
				new SimpleDateFormat("dd MMM yyy").format(new DateTime().minusDays(1).toDate())));

		when(proprietorRepository.findByKeyValue(PendingIncreaseService.PROPRIOTER_STATUS,
				PendingIncreaseService.VALUE_ACTIVE)).thenReturn(propriotors);

		//
		service.increaseRun();

		//
		verify(accountService, times(1)).upgradePricePlan(accountNumber);
	}

	private Proprietor getPropriotor(String... keyValuePairs) {

		Proprietor proprietor = new Proprietor();

		Map<String, String> ret = new HashMap<String, String>();

		for (int i = 0; i < keyValuePairs.length; i += 2) {
			ret.put(keyValuePairs[i], keyValuePairs[i + 1]);
		}

		proprietor.setProperties(ret);

		return proprietor;
	}

	private Map<String, String> getProperties(String accountNumber) {
		Map<String, String> ret = new HashMap<String, String>();

		ret.put(PendingIncreaseService.PROPRIOTER_PRIMARY_TYPE, "PendingIncrease");
		ret.put(PendingIncreaseService.PROPRIOTER_STATUS, "Active");
		ret.put(PendingIncreaseService.PROPRIOTER_ACCOUNT_NUMBER, accountNumber);
		DateTime now = new DateTime();
		DateTime monthFromNow = now.plusMonths(1);
		ret.put(PendingIncreaseService.PROPRIOTER_ACTION_ACTIVATION_DATE,
				new SimpleDateFormat("dd MMM yyy").format(monthFromNow.toDate()));

		return ret;
	}

	private class IsProprioterWithProperties extends ArgumentMatcher<Proprietor> {

		private Map<String, String> properties;

		public IsProprioterWithProperties(Map<String, String> properties) {
			this.properties = properties;
		}

		@Override
		public boolean matches(Object argument) {

			Proprietor other = (Proprietor) argument;

			for (String key : this.properties.keySet()) {

				if (!other.getProperties().get(key).equals(this.properties.get(key))) {

					System.out.println(argument + " value of " + key + " is not equal to " + this.properties.get(key)
							+ " but " + other.getProperties().get(key) + " instead");
					return false;
				}
			}

			return true;
		}
	}
}
