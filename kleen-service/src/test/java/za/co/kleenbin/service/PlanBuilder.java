package za.co.kleenbin.service;

import org.dozer.DozerBeanMapper;

import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.entity.type.FREQUENCY;

public class PlanBuilder {

	static PlanBuilder aPlan() {
		
		return new PlanBuilder();
	}

	private FREQUENCY frequency = FREQUENCY.EVERY_WEEK;
	private Integer numberOfBins = 1;
	private Integer year;;
	private Integer price = 25;
	private Long id;
	
	PlanBuilder withFrequency(FREQUENCY frequency) {
		
		this.frequency= frequency;
		return this;
	}
	
	public Plan build() {
		
		Plan plan = new Plan();
		
		plan.setFrequency(this.frequency);
		plan.setNumberOfBins(this.numberOfBins);
		plan.setPrice(this.price);
		plan.setId(id);
		plan.setYear(year);
		
		return plan;
	}

	public PlanBuilder withPrice(int price) {
		this.price = price;
		return this;
	}
	
	public PlanBuilder withYear(int year) {
		this.year = year;
		return this;
	}

	public PlanBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public za.co.kleenbin.dto.Plan buildConverted() {
		return convert(this.build());
	}
	
	public za.co.kleenbin.dto.Plan convert(Plan plan) {

		return new DozerBeanMapper().map(plan, za.co.kleenbin.dto.Plan.class);
	}
}
