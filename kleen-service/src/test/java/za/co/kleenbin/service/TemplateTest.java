package za.co.kleenbin.service;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.joda.time.DateTime;
import org.junit.Test;

import za.co.kleen.dao.entity.type.FREQUENCY;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

public class TemplateTest {

	public VelocityEngine velocityEngine() {

		VelocityEngine velocityEngine = new VelocityEngine();

		velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		velocityEngine.init();

		return velocityEngine;
	}

	DateTime thisMonth = new DateTime();
	DateTime nextMonth = thisMonth.plusMonths(1);
	
	@Test
	public void testInvoiceSMS() throws IOException {

		//
		
		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
			.withBalance(2010f)
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.INVOICE_SMS, velocityEngine()).createMessage(account);
		assertEquals(
				"KleenBinFG@gmail.com :) Balance for " + thisMonth.toString("MMM") + " is R2010.00 payable by 7th " + nextMonth.toString("MMM yyyy") + " to: Acc name:Be For Sunset, STD Bank Branch:011545, Acc num:012652555, Ref:JX324. Payment may take 7 days to reflect",
				text);
	}

	@Test
	public void testMissedServiceSMS() throws IOException {

		za.co.kleenbin.dto.Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.MISSED_SERVICE_SMS, velocityEngine()).createMessage(account);
		assertEquals(
				" KleenBinFG@gmail.com :) Dear  Dr Mvubu  Please be notified that our service men could not service your bin as it was un-reachable",
				text);
	}

	@Test
	public void testStatementSMS() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
			.withBalance(2010f)
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.STATEMENT_SMS, velocityEngine()).createMessage(account);
		assertEquals(
				" KleenBinFG@gmail.com :) Dear  Dr Mvubu  your current balance is R2010.00. Please reply with your email address to receive the full statement.",
				text);
	}

	@Test
	public void testPendingSuspensionSMS() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
			.withBalance(2010f)
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.PENDING_SUSPENSION_SMS, velocityEngine()).createMessage(account);
		String twoWeeksFromNow = new SimpleDateFormat("dd MMM yyyy").format(new DateTime().plusWeeks(2).toDate());
		assertEquals(
				"Dear Dr Mvubu, To avoid suspension you must make payment towards your balance of R2010.00 by " + twoWeeksFromNow  + ". Ignore if already paid. customer@kbfg.co.za",
				text);
	}

	@Test
	public void testInvoiceEmail() throws IOException {

		//	
		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
		.buildConverted();

		//
		String emailText = new MessageCreator(MessageCreatorType.INVOICE_MAIL, velocityEngine()).createMessage(account);
		
		//
		assertEquals("Dear  Dr Mvubu " + System.lineSeparator() + System.lineSeparator()
				+ "* Payment for the month of " + thisMonth.toString("MMM") + " is due on or before the 7th of " + nextMonth.toString("MMM yyyy") + System.lineSeparator()
				+ "* EFT & Bank Deposits are preferred methods of payment, your unique reference is JX324"
				+ System.lineSeparator() + "* Always request a receipt for cash payment" + System.lineSeparator()
				+ "* Payments may take up to 7 days to reflect" + System.lineSeparator() + System.lineSeparator()
				+ "Kind Regards" + System.lineSeparator() + "Kleen Bin Faery Glen Customer Service"
				+ System.lineSeparator() + "0726467948" + System.lineSeparator(), emailText);
	}

	@Test
	public void testMissedEmail() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
		.buildConverted();

		String emailText = new MessageCreator(MessageCreatorType.MISSED_SERVICE_MAIL, velocityEngine()).createMessage(account);
		assertEquals("Dear  Dr Mvubu " + System.lineSeparator() + System.lineSeparator()
				+ "Please be notified that our service men could not service your bin as it was un-reachable."
				+ System.lineSeparator() + System.lineSeparator() + "Kind Regards" + System.lineSeparator()
				+ "Kleen Bin Faery Glen Customer Service" + System.lineSeparator() + "0726467948"
				+ System.lineSeparator(), emailText);
	}

	@Test
	public void testStatementEmail() throws IOException {

		za.co.kleenbin.dto.Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
		.buildConverted();

		String emailText = new MessageCreator(MessageCreatorType.STATEMENT_MAIL, velocityEngine()).createMessage(account);
		assertEquals("Dear  Dr Mvubu " + System.lineSeparator() + System.lineSeparator()
				+ "* Find attached the latest statement with your account history" + System.lineSeparator()
				+ "* EFT & Bank Deposits are preferred methods of payment, your unique reference is JX324"
				+ System.lineSeparator() + "* Always request a receipt for cash payment" + System.lineSeparator()
				+ "* Payments may take up to 7 days to reflect" + System.lineSeparator() + System.lineSeparator()
				+ "Kind Regards" + System.lineSeparator() + "Kleen Bin Faery Glen Customer Service"
				+ System.lineSeparator() + "0726467948" + System.lineSeparator(), emailText);
	}

	@Test
	public void testPendingSuspensionEmail() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
			.withBalance(2010f)
		.buildConverted();

		String emailText = new MessageCreator(MessageCreatorType.PENDING_SUSPENSION_MAIL, velocityEngine()).createMessage(account);
		String twoWeeksFromNow = new SimpleDateFormat("dd MMM yyyy").format(new DateTime().plusWeeks(2).toDate());
		assertEquals(
				"Dear  Dr Mvubu " + System.lineSeparator() + System.lineSeparator()
						+ "To avoid having services suspended you need to make a payment by " + twoWeeksFromNow + "."
						+ System.lineSeparator() + System.lineSeparator()
						+ "Our records show that you have not made any payments in the last 60 days and that you have an outstanding balance of R2010.00."
						+ System.lineSeparator() + System.lineSeparator()
						+ "For further information or to make payment arrangements please email customer@kbfg.co.za or call/SMS 0726467948."
						+ System.lineSeparator() + System.lineSeparator()
						+ "Please ignore if payment has already been made." + System.lineSeparator()
						+ System.lineSeparator() + "Kind Regards" + System.lineSeparator() + "Kleenbin Faerie Glen",
				emailText);
	}

	@Test
	public void testInvoicemessage() throws IOException {

		//
		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
		.buildConverted();

		//
		String emailText = new MessageCreator(MessageCreatorType.INVOICE_MESSAGE, velocityEngine()).createMessage(account);
		
		//
		assertEquals(System.lineSeparator() + "* Payment for the month of " + thisMonth.toString("MMM") + " is due on or before the 7th of " + nextMonth.toString("MMM yyyy")
				+ System.lineSeparator()
				+ "* EFT & Bank Deposits are preferred methods of payment, your unique reference is JX324"
				+ System.lineSeparator() + "* Always request a receipt for cash payment" + System.lineSeparator()
				+ "* Payments may take up to 7 days to reflect", emailText);
	}

	@Test
	public void testSuspensionSMS() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
			.withBalance(2010f)
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.SUSPENSION_SMS, velocityEngine()).createMessage(account);
		assertEquals(
				"Dear Dr Mvubu, your account has been auto-suspended due to lack of payments towards balance of R2010.00. Email customer@kbfg.co.za to query or re-instate",
				text);
	}

	@Test
	public void testSuspensionEmail() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
					.build()
					)
			.withAccountNumber("324")
			.withBalance(2010f)
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.SUSPENSION_MAIL, velocityEngine()).createMessage(account);
		assertEquals("Dear Dr Mvubu" + System.lineSeparator() + System.lineSeparator()
				+ "Your account has been auto-suspended due to lack of payment towards your balance of R2010.00."
				+ System.lineSeparator() + System.lineSeparator()
				+ "For further information or to make payment arrangements for a re-instatement please email customer@kbfg.co.za or call/SMS 0726467948."
				+ System.lineSeparator() + System.lineSeparator() + "Kind Regards" + System.lineSeparator()
				+ "Kleenbin Faerie Glen", text);
	}

	@Test
	public void testPendingIncreaseSMS() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withPlan(PlanBuilder.aPlan().withFrequency(FREQUENCY.EVERY_WEEK).withPrice(26).build())
			.withFuturePlan(PlanBuilder.aPlan().withFrequency(FREQUENCY.EVERY_WEEK).withPrice(40).build())
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.PENDING_INCREASE_SMS, velocityEngine()).createMessage(account);
		assertEquals(
				"Dear Dr Mvubu, Your account is due for an increase from R104 pm to R160 pm.  The increase will come in affect next month. customer@kbfg.co.za",
				text);
	}

	@Test
	public void testPendingIncreaseEmail() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withEmail("noone@nowhere.com")
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.EVERY_WEEK)
						.withPrice(26)
					.build()
					)
			.withFuturePlan(
					PlanBuilder.aPlan()
						.withFrequency(FREQUENCY.EVERY_WEEK)
						.withPrice(40)
					.build())
			.withAccountNumber("324")
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.PENDING_INCREASE_MAIL, velocityEngine()).createMessage(account);
		assertEquals("Dear  Dr Mvubu " + System.lineSeparator() + System.lineSeparator()
				+ "Your account is due for an increase from R104 pm to R160 pm."
				+ " This increase will come in affect next month." + System.lineSeparator() + System.lineSeparator()
				+ "For further information please reply or email customer@kbfg.co.za ." + System.lineSeparator()
				+ System.lineSeparator() + "Kind Regards" + System.lineSeparator() + "Kleenbin Faerie Glen", text);
	}

	@Test
	public void testIncreaseSMS() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withPrice(40)
						.withFrequency(FREQUENCY.EVERY_WEEK)
					.build())
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.INCREASE_SMS, velocityEngine()).createMessage(account);
		assertEquals(
				"Dear Dr Mvubu, This is to notify you that an increase to price plan of R160 pm has come into effect. customer@kbfg.co.za",
				text);
	}

	@Test
	public void testIncreaseEmail() throws IOException {

		Account account = AccountBuilder
			.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(PlanBuilder.aPlan().withFrequency(FREQUENCY.EVERY_WEEK).withPrice(40).build())
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.INCREASE_MAIL, velocityEngine()).createMessage(account);
		assertEquals("Dear Dr Mvubu" 
				+ System.lineSeparator() + System.lineSeparator()
				+ "This serves to notify you of the price increase which is henceforth in effect."
				+ System.lineSeparator()
				+ "You account is now on a price plan of R160 pm."
				+ System.lineSeparator() + System.lineSeparator()
				+ "For more information please reply or email customer@kbfg.co.za."
				+ System.lineSeparator() + System.lineSeparator() 
				+ "Kind Regards" 
				+ System.lineSeparator()
				+ "Kleenbin Faerie Glen", text);
	}
	
	@Test
	public void testNewAccountEmail() throws IOException {

		Account account = AccountBuilder
			.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(PlanBuilder.aPlan().withFrequency(FREQUENCY.EVERY_WEEK).withPrice(60).build())
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.NEW_ACCOUNT_EMAIL, velocityEngine()).createMessage(account);
		assertEquals("Dear Dr Mvubu" 
				+ System.lineSeparator() + System.lineSeparator()
				+ "We are pleased to welcome you as a client of Kleenbin Faery Glen (www.kbfg.co.za)."
				+ System.lineSeparator()
				+ System.lineSeparator()
				+ "You account is on the following price plan:"
				+ System.lineSeparator()
				+ " # 1 bin/s"
				+ System.lineSeparator()
				+ " # weekly"
				+ System.lineSeparator()
				+ " # @ R60 per bin per service"
				+ System.lineSeparator() + System.lineSeparator()
				+ "Which amounts to R240 pm."
				+ System.lineSeparator() + System.lineSeparator()
				+ "Expect an invoice from this email towards the end of the month with information relating to the balance and monthly invoice."
				+ System.lineSeparator() + System.lineSeparator() 
				+ "For more information or assistence please email customer@kbfg.co.za and we will get back to you."
				+ System.lineSeparator() + System.lineSeparator() 
				+ "Kind Regards" 
				+ System.lineSeparator()
				+ "Kleenbin Faerie Glen", text);
	}
	
	@Test
	public void testNewAccountSMS() throws IOException {

		Account account = AccountBuilder.anAccount()
			.withAccountHolder(
					PersonBuilder.aPerson()
						.withTitle("Dr")
						.withSurname("Mvubu")
					.build())
			.withPlan(
					PlanBuilder.aPlan()
						.withPrice(60)
						.withFrequency(FREQUENCY.EVERY_WEEK)
					.build())
		.buildConverted();

		String text = new MessageCreator(MessageCreatorType.NEW_ACCOUNT_SMS, velocityEngine()).createMessage(account);
		assertEquals(
				"kleenbinfg@gmail.com :) Dear Dr Mvubu, We welcome you as our newest Kleenbin FG client. You are currenlty on a R240 pm price plan. Please email customer@kbfg.co.za for more information and monthly invoice.",
				text);
	}

}
