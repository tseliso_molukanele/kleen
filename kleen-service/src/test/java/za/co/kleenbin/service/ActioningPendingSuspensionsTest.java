package za.co.kleenbin.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleen.dao.entity.Proprietor;
import za.co.kleen.dao.repository.ProprietorRepository;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.EmailAddress;
import za.co.kleenbin.dto.Payment;
import za.co.kleenbin.dto.Person;
import za.co.kleenbin.dto.PhoneNumber;
import za.co.kleenbin.dto.type.AccountStatus;
import za.co.kleenbin.dto.type.NumberType;
import za.co.kleenbin.service.exception.IrrecoverableException;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@RunWith(MockitoJUnitRunner.class)
public class ActioningPendingSuspensionsTest {

	/*
	 * having account with the following joining dates
	 * 
	 * 101- 102- 103- 104- 105- 106- 107 joining date jan feb mar apr may jun
	 * jul
	 * 
	 * 
	 * the following cycles should give the follfowing results
	 * 
	 * new- late- pending- pending for overdue- overdue 
	 * Cycle 1 jan 1 0 0 0 0
	 * Cycle 2 feb 2 0 0 0 0 
	 * Cycle 3 mar 2 1 0 0 0 
	 * Cyle 4 apr 2 1 1 0 0 
	 * Cycle 5 may 2 1 1 1 0 
	 * Cycle 6 jun 2 1 1 1 1 
	 * Cycle 7 jul 2 1 1 1 2
	 * 
	 */

	private static final String ACC3 = "103";
	private static final String ACC2 = "102";
	private static final String ACC1 = "101";

	@Test
	public void test_cycle6_hasPaid() throws IOException {

		//
		DateTime now = new DateTime();
		DateTime lastWeek = now.plusDays(-7);
		DateTime aLittleAfterLastWeek = now.plusDays(-4);
		DateTime nextWeek = now.plusDays(7);

		Account account = mock(Account.class);
		when(account.getStatus()).thenReturn(AccountStatus.ACTIVE);
		when(account.getAccountNumber()).thenReturn(ACC2);
		Person p = getMailablePerson(ACC2);
		when(account.getAccountHolder()).thenReturn(p);

		Payment payment = new Payment();
		payment.setDate(aLittleAfterLastWeek.toDate());
		List<Payment> payments = new ArrayList<>();
		payments.add(payment);

		when(accountService.getAccount(ACC2)).thenReturn(account);
		when(accountService.retrievePaymentsByAccount(account)).thenReturn(payments);

		List<Proprietor> activePropriotors = new ArrayList<>();

		Proprietor paidPropriotor = getPropriotor(ACC2, PendingSuspensionService.VALUE_ACTIVE, lastWeek);
		Proprietor owingPropriotor = getPropriotor(ACC3, PendingSuspensionService.VALUE_ACTIVE, nextWeek);

		activePropriotors.add(paidPropriotor);
		activePropriotors.add(owingPropriotor);

		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE)).thenReturn(activePropriotors);

		//
		service.suspensionRun();

		//

		verify(proprietorRepository, times(1)).delete(argThat(new IsProprioterLike(paidPropriotor)));
	}

	@Test(expected = IrrecoverableException.class)
	public void expects_Irrecoverable_exception_when_an_exception_occurs() throws IOException {

		//
		String actionDate = "BROKEN";

		Account account = mock(Account.class);
		when(account.toString()).thenCallRealMethod();
		when(account.getStatus()).thenReturn(AccountStatus.ACTIVE);
		when(account.getAccountNumber()).thenReturn(ACC2);
		Person p = getMailablePerson(ACC2);
		when(account.getAccountHolder()).thenReturn(p);

		when(accountService.getAccount(ACC2)).thenReturn(account);

		List<Proprietor> activePropriotors = new ArrayList<>();

		DateTime now = new DateTime();
		DateTime lastWeek = now.plusDays(-7);
		DateTime nextWeek = now.plusDays(7);

		Proprietor brokenPropriotor = getPropriotor(ACC2, PendingSuspensionService.VALUE_ACTIVE, lastWeek);
		brokenPropriotor.getProperties().put(PendingSuspensionService.PROPRIOTER_ACTION_ACTIVATION_DATE, actionDate);
		activePropriotors.add(brokenPropriotor);
		activePropriotors.add(getPropriotor(ACC3, PendingSuspensionService.VALUE_ACTIVE, nextWeek));

		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE)).thenReturn(activePropriotors);

		//
		service.suspensionRun();
	}

	@Test
	public void test_cycle6_NonActive() throws IOException {

		//

		Account account = mock(Account.class);
		when(account.toString()).thenCallRealMethod();
		when(account.getStatus()).thenReturn(AccountStatus.DISABLED);
		when(account.getAccountNumber()).thenReturn(ACC2);
		Person p = getMailablePerson(ACC2);
		when(account.getAccountHolder()).thenReturn(p);

		when(accountService.getAccount(ACC2)).thenReturn(account);

		List<Proprietor> activePropriotors = new ArrayList<>();

		DateTime now = new DateTime();
		DateTime lastWeek = now.plusDays(-7);
		DateTime nextWeek = now.plusDays(7);

		activePropriotors.add(getPropriotor(ACC2, PendingSuspensionService.VALUE_ACTIVE, lastWeek));
		activePropriotors.add(getPropriotor(ACC3, PendingSuspensionService.VALUE_ACTIVE, nextWeek));

		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE)).thenReturn(activePropriotors);

		//
		service.suspensionRun();

		//
		verify(accountService, times(1)).getAccount(eq(ACC2));
		verify(proprietorRepository, times(1)).findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE);

		verifyNoMoreInteractions(accountService, proprietorRepository, messagingClient);
	}
	
	@InjectMocks
	private PendingSuspensionService service = new PendingSuspensionService();
	
	@Mock
	private AccountService accountService;
	
	@Mock
	private VelocityEngine engine;
	
	@Mock
	ProprietorRepository proprietorRepository;
	
	@Mock
	private SMSSender smsSender;
	
	@Mock
	Client messagingClient;
	
	@Spy
	ObjectMapper objectMapper = new ObjectMapper();
	
	@Mock
	private NotificationService notificationService;
	
	@Before
	public void setup() {
		
		when(engine.getTemplate(any(String.class))).thenReturn(mock(Template.class));
	}

	@Test
	public void test_cycle6_EmailMessage() throws IOException {

		//

		Account account = mock(Account.class);
		when(account.toString()).thenCallRealMethod();
		when(account.getStatus()).thenReturn(AccountStatus.ACTIVE);
		when(account.getAccountNumber()).thenReturn(ACC2);
		Person p = getMailablePerson(ACC2);
		when(account.getAccountHolder()).thenReturn(p);

		when(accountService.getAccount(ACC2)).thenReturn(account);

		List<Proprietor> activePropriotors = new ArrayList<>();

		DateTime now = new DateTime();
		DateTime lastWeek = now.plusDays(-7);
		DateTime nextWeek = now.plusDays(7);

		activePropriotors.add(getPropriotor(ACC2, PendingSuspensionService.VALUE_ACTIVE, lastWeek));
		activePropriotors.add(getPropriotor(ACC3, PendingSuspensionService.VALUE_ACTIVE, nextWeek));

		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE)).thenReturn(activePropriotors);

		//
		service.suspensionRun();

		//
		verify(accountService, times(1)).update(eq(ACC2), eq(Boolean.FALSE));
		verify(proprietorRepository, times(1)).save(
				argThat(new IsProprioterLike(getPropriotor(ACC2, PendingSuspensionService.VALUE_COMPLETED, lastWeek))));
		verify(messagingClient, times(1)).send(any(String.class), contains(
				"Your account has been suspended: 102"));
		
	}

	@Test
	public void test_cycle5_SMS() throws IOException {

		//

		Account account = mock(Account.class);
		when(account.toString()).thenCallRealMethod();
		when(account.getStatus()).thenReturn(AccountStatus.ACTIVE);
		when(account.getAccountNumber()).thenReturn(ACC1);
		Person p = getSMSablePerson(ACC1);
		when(account.getAccountHolder()).thenReturn(p);

		when(accountService.getAccount(ACC1)).thenReturn(account);

		List<Proprietor> activePropriotors = new ArrayList<>();

		DateTime now = new DateTime();
		DateTime lastWeek = now.plusDays(-7);
		DateTime nextWeek = now.plusDays(7);

		activePropriotors.add(getPropriotor(ACC2, PendingSuspensionService.VALUE_ACTIVE, nextWeek));
		activePropriotors.add(getPropriotor(ACC1, PendingSuspensionService.VALUE_ACTIVE, lastWeek));

		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE)).thenReturn(activePropriotors);

		//
		service.suspensionRun();

		//
		verify(smsSender, times(1)).sendSMS(account, MessageCreatorType.SUSPENSION_SMS);
		verify(accountService, times(1)).update(eq(ACC1), eq(Boolean.FALSE));
		verify(proprietorRepository, times(1)).save(
				argThat(new IsProprioterLike(getPropriotor(ACC1, PendingSuspensionService.VALUE_COMPLETED, lastWeek))));

	}

	@Test
	public void test_cycle4() {

		//

		List<Proprietor> activePropriotors = new ArrayList<>();

		DateTime now = new DateTime();
		DateTime nextWeek = now.plusDays(7);

		activePropriotors.add(getPropriotor(ACC1, PendingSuspensionService.VALUE_ACTIVE, nextWeek));

		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE)).thenReturn(activePropriotors);

		//
		service.suspensionRun();

		//
		verify(proprietorRepository, times(1)).findByKeyValue(PendingSuspensionService.PROPRIOTER_STATUS,
				PendingSuspensionService.VALUE_ACTIVE);

		verifyNoMoreInteractions(accountService, proprietorRepository, messagingClient);
	}

	private NotificationService getErrorReportingService(Client messagingClient, ObjectMapper mapper,
			String errorEmailQueue, String errorEmail) {
		NotificationService notificationService = new NotificationService();
		notificationService.setErrorEmail(errorEmail);
		notificationService.setMessagingClient(messagingClient);
		notificationService.setEmailQueue(errorEmailQueue);
		notificationService.setMapper(mapper);
		return notificationService;
	}

	private Person getSMSablePerson(String accountNumber) {
		PhoneNumber pNumber = new PhoneNumber();
		pNumber.setNumber("0711140" + accountNumber);
		pNumber.setNumberType(NumberType.CELL);
		List<PhoneNumber> numbers = new ArrayList<>();
		numbers.add(pNumber);
		Person accountHolder = mock(Person.class);
		when(accountHolder.getPhoneNumbers()).thenReturn(numbers);
		return accountHolder;
	}

	private Person getMailablePerson(String accountNumber) {
		EmailAddress emailAdd = new EmailAddress();
		emailAdd.setEmail(accountNumber + "@email.com");
		List<EmailAddress> emails = new ArrayList<>();
		emails.add(emailAdd);
		Person accountHolder = mock(Person.class);
		when(accountHolder.getEmailAddresses()).thenReturn(emails);
		return accountHolder;
	}

	private Proprietor getPropriotor(String accountNumber, String status, DateTime whenToActivate) {

		Proprietor proprietor = new Proprietor();

		Map<String, String> keyValuePairs = new HashMap<String, String>();

		keyValuePairs.put(PendingSuspensionService.PROPRIOTER_PRIMARY_TYPE,
				PendingSuspensionService.VALUE_PENDING_SUSPENSION);
		keyValuePairs.put(PendingSuspensionService.PROPRIOTER_STATUS, status);
		keyValuePairs.put(PendingSuspensionService.PROPRIOTER_ACCOUNT_NUMBER, accountNumber);
		keyValuePairs.put(PendingSuspensionService.PROPRIOTER_ACTION_ACTIVATION_DATE,
				new SimpleDateFormat("dd MMM yyy").format(whenToActivate.toDate()));

		proprietor.setProperties(keyValuePairs);
		proprietor.setCreatedDate(whenToActivate.plusDays(-14));

		return proprietor;
	}

	private class IsProprioterLike extends ArgumentMatcher<Proprietor> {

		private Proprietor proprietor;

		public IsProprioterLike(Proprietor proprietor) {
			this.proprietor = proprietor;
		}

		@Override
		public boolean matches(Object argument) {

			if (!(argument instanceof Proprietor)) {
				System.out.println(argument + " is not of type " + Proprietor.class);
				return false;
			}

			Proprietor other = (Proprietor) argument;

			for (String key : this.proprietor.getProperties().keySet()) {

				if (!other.getProperties().containsKey(key)) {

					System.out.println(argument + " does not contain " + key);
					return false;
				}

				if (!other.getProperties().get(key).equals(this.proprietor.getProperties().get(key))) {

					System.out.println(argument + " value of " + key + " is not equal to "
							+ this.proprietor.getProperties().get(key) + " but " + other.getProperties().get(key)
							+ " instead");
					return false;
				}
			}

			return true;
		}
	}
}
