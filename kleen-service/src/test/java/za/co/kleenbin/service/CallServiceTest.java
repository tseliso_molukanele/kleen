package za.co.kleenbin.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;

import za.co.kleenbin.dto.AccountAction;
import za.co.kleenbin.dto.type.AccountActionType;
import za.co.kleenbin.service.callaction.AccountActionProvider;

public class CallServiceTest {

    @Test
    public void testList_1Provider() {

        //
        List<AccountActionProvider> callProviders = new ArrayList<AccountActionProvider>();
        AccountActionProvider provider1 = makeProvider(2, null);
        callProviders.add(provider1);

        int spotAvailable = 10;

        CallService service = new CallService();
        service.setSpotAvailable(spotAvailable);
        service.setCallProviders(callProviders);

        //
        List<AccountAction> list = service.list();

        //
        assertEquals(spotAvailable, list.size());
    }
    
    @Test
    public void testList_2Providers() {
        
        //
        List<AccountActionProvider> callProviders = new ArrayList<AccountActionProvider>();
        AccountActionProvider provider1 = makeProvider(2, AccountActionType.HIGH_BALANCE);
        AccountActionProvider provider2 = makeProvider(3, AccountActionType.NO_EMAIL);
        callProviders.add(provider1);
        callProviders.add(provider2);
        
        int spotAvailable = 10;
        
        CallService service = new CallService();
        service.setSpotAvailable(spotAvailable);
        service.setCallProviders(callProviders);
        
        //
        List<AccountAction> list = service.list();
        
        int countHigh = 0;
        int countEmail = 0;
        for (AccountAction accountAction : list) {
            
            if(accountAction.getType().equals(AccountActionType.HIGH_BALANCE)) {
                countHigh ++;
            }
            
            if(accountAction.getType().equals(AccountActionType.NO_EMAIL)) {
                countEmail ++;
            }
        }
        
        //
        assertEquals(spotAvailable, list.size());
        assertEquals(4, countHigh);
        assertEquals(6, countEmail);
    }

    private AccountActionProvider makeProvider(final int spotRequired, final AccountActionType type) {

        return new AccountActionProvider() {

            @Override
            public int getSpotsRequired() {
                return spotRequired;
            }

            @Override
            public List<AccountAction> getNextActions(int count) {
                List<AccountAction> actions = new ArrayList<AccountAction>();

                for (int i = 0; i < count; i++) {
                    AccountAction accountAction = new AccountAction();
                    accountAction.setCreatedDate(new DateTime());
                    accountAction.setType(type);
                    actions.add(accountAction);
                }
                return actions;
            }
        };
    }

}
