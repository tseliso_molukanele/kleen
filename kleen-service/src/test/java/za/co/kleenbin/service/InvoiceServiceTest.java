package za.co.kleenbin.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.Address;
import za.co.kleen.dao.entity.EmailAddress;
import za.co.kleen.dao.entity.Invoice;
import za.co.kleen.dao.entity.Person;
import za.co.kleen.dao.entity.PhoneNumber;
import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.entity.type.FREQUENCY;
import za.co.kleen.dao.entity.type.NumberType;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleen.dao.repository.InvoiceRepository;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceServiceTest {

	@InjectMocks
	private InvoiceService service;
	
	@Mock
	private InvoiceSender invoiceSender;
	
	@Mock
	private AccountRepository accountRepository;
	
	@Mock
	private InvoiceRepository invoiceRepository;
	
	@Mock
	private NotificationService notificationService;
	
	@Mock
	private AccountService accountService;
	
    @Test
    public void testSuccessfullInvoice() throws IOException {

        //
        List<Account> activeAccounts = new ArrayList<Account>();

        int totalAccounts_4 = 10;
        int totalAccounts_2 = 5;
        int totalAccounts_1 = 2;

        for (int i = 0; i < totalAccounts_4; i++) {

            activeAccounts.add(makeAccount(FREQUENCY.EVERY_WEEK, i));
        }

        for (int i = totalAccounts_4; i < totalAccounts_4 + totalAccounts_2; i++) {

            activeAccounts.add(makeAccount(FREQUENCY.BI_WEEKLY, i));
        }

        for (int i = totalAccounts_4 + totalAccounts_2; i < totalAccounts_4 + totalAccounts_2 + totalAccounts_1; i++) {

            activeAccounts.add(makeAccount(FREQUENCY.ONCE_PER_MONTH, i));
        }

        when(accountRepository.findActive()).thenReturn(activeAccounts);

        Plan everyWeekPlan = new Plan();
        everyWeekPlan.setFrequency(FREQUENCY.EVERY_WEEK);
        everyWeekPlan.setNumberOfBins(1);
        everyWeekPlan.setPrice(100);

        Plan biPlan = new Plan();
        biPlan.setFrequency(FREQUENCY.BI_WEEKLY);
        biPlan.setNumberOfBins(1);
        biPlan.setPrice(75);

        Plan oncePlan = new Plan();
        oncePlan.setFrequency(FREQUENCY.ONCE_PER_MONTH);
        oncePlan.setNumberOfBins(1);
        oncePlan.setPrice(65);

        //
        service.invoiceRun();

        //
        verify(invoiceRepository, times(totalAccounts_4)).save(argThat(new IsInvoiceWithServiceCount(4)));
        verify(invoiceRepository, times(totalAccounts_2)).save(argThat(new IsInvoiceWithServiceCount(2)));
        verify(invoiceRepository, times(totalAccounts_1)).save(argThat(new IsInvoiceWithServiceCount(1)));

        verify(invoiceSender, times(9)).smsInvoice(any(za.co.kleenbin.dto.Account.class));
        verify(invoiceSender, times(8)).emailInvoice(any(za.co.kleenbin.dto.Account.class));

    }

    @Test
    public void testFailingInvoice() throws IOException {

        //

        List<Account> activeAccounts = new ArrayList<Account>();

        activeAccounts.add(makeAccount(FREQUENCY.EVERY_WEEK, 0));
        Account brokenAccount1 = makeAccount(FREQUENCY.EVERY_WEEK, 1);

        Plan plan = new Plan();
        plan.setNumberOfBins(2);
        brokenAccount1.setPlan(plan);
        
        activeAccounts.add(brokenAccount1);

        activeAccounts.add(makeAccount(FREQUENCY.BI_WEEKLY, 2));
        Account brokenAccount2 = makeAccount(FREQUENCY.BI_WEEKLY, 2);

        Plan plan2 = new Plan();
        plan2.setNumberOfBins(2);
        brokenAccount2.setPlan(plan2);
        
        activeAccounts.add(brokenAccount2);

        activeAccounts.add(makeAccount(FREQUENCY.ONCE_PER_MONTH, 3));
        Account brokenAccount3 = makeAccount(FREQUENCY.ONCE_PER_MONTH, 4);

        Plan plan3 = new Plan();
        plan3.setNumberOfBins(2);
        brokenAccount3.setPlan(plan3);
        
        activeAccounts.add(brokenAccount3);

        when(accountRepository.findActive()).thenReturn(activeAccounts);

        Plan everyWeekPlan = new Plan();
        everyWeekPlan.setFrequency(FREQUENCY.EVERY_WEEK);
        everyWeekPlan.setNumberOfBins(1);
        everyWeekPlan.setPrice(100);

        Plan biPlan = new Plan();
        biPlan.setFrequency(FREQUENCY.BI_WEEKLY);
        biPlan.setNumberOfBins(1);
        biPlan.setPrice(75);

        Plan oncePlan = new Plan();
        oncePlan.setFrequency(FREQUENCY.ONCE_PER_MONTH);
        oncePlan.setNumberOfBins(1);
        oncePlan.setPrice(65);

        //
        service.invoiceRun();

        //
        verify(invoiceRepository, times(1)).save(argThat(new IsInvoiceWithServiceCount(4)));
        verify(invoiceRepository, times(1)).save(argThat(new IsInvoiceWithServiceCount(2)));
        verify(invoiceRepository, times(1)).save(argThat(new IsInvoiceWithServiceCount(1)));

        verify(invoiceSender, times(2)).smsInvoice(any(za.co.kleenbin.dto.Account.class));
        verify(invoiceSender, times(1)).emailInvoice(any(za.co.kleenbin.dto.Account.class));
        verify(notificationService, times(1)).sendEmail(contains(", Error invoicing account Acc No: 1, acc holder: , address: Mock for Address --> null , Error invoicing account Acc No: 2, acc holder: , address: Mock for Address --> null , Error invoicing account Acc No: 4, acc holder: , address: Mock for Address --> null "));

    }

    @Test
    public void testSkipSending() throws Exception {

        //

        List<Account> activeAccounts = new ArrayList<Account>();

        activeAccounts.add(makeAccount(FREQUENCY.EVERY_WEEK, 0));
        Account brokenAccount1 = makeAccount(FREQUENCY.EVERY_WEEK, 1);
        
        Plan plan = new Plan();
        plan.setNumberOfBins(2);
        brokenAccount1.setPlan(plan);
        
        activeAccounts.add(brokenAccount1);

        activeAccounts.add(makeAccount(FREQUENCY.BI_WEEKLY, 2));
        Account brokenAccount2 = makeAccount(FREQUENCY.BI_WEEKLY, 1);
        
        Plan plan2 = new Plan();
        plan2.setNumberOfBins(2);
        brokenAccount2.setPlan(plan);
        
        activeAccounts.add(brokenAccount2);

        activeAccounts.add(makeAccount(FREQUENCY.ONCE_PER_MONTH, 3));
        Account brokenAccount3 = makeAccount(FREQUENCY.ONCE_PER_MONTH, 4);
        
        Plan plan3 = new Plan();
        plan3.setNumberOfBins(2);
        brokenAccount3.setPlan(plan3);
        
        activeAccounts.add(brokenAccount3);

        when(accountRepository.findActive()).thenReturn(activeAccounts);

        Plan everyWeekPlan = new Plan();
        everyWeekPlan.setFrequency(FREQUENCY.EVERY_WEEK);
        everyWeekPlan.setNumberOfBins(1);
        everyWeekPlan.setPrice(100);

        Plan biPlan = new Plan();
        biPlan.setFrequency(FREQUENCY.BI_WEEKLY);
        biPlan.setNumberOfBins(1);
        biPlan.setPrice(75);

        Plan oncePlan = new Plan();
        oncePlan.setFrequency(FREQUENCY.ONCE_PER_MONTH);
        oncePlan.setNumberOfBins(1);
        oncePlan.setPrice(65);

        //
        service.invoiceRun(false);

        //
        verify(invoiceRepository, times(1)).save(argThat(new IsInvoiceWithServiceCount(4)));
        verify(invoiceRepository, times(1)).save(argThat(new IsInvoiceWithServiceCount(2)));
        verify(invoiceRepository, times(1)).save(argThat(new IsInvoiceWithServiceCount(1)));

        verify(notificationService, times(1)).sendEmail(contains(", Error invoicing account Acc No: 1, acc holder: , address: Mock for Address --> null , Error invoicing account Acc No: 1, acc holder: , address: Mock for Address --> null , Error invoicing account Acc No: 4, acc holder: , address: Mock for Address --> null "));
    }

    @Test
    public void testSendOnDate() throws Exception {

        //

        List<Account> activeAccounts = new ArrayList<Account>();

        activeAccounts.add(makeAccount(FREQUENCY.EVERY_WEEK, 0));
        Account brokenAccount1 = makeAccount(FREQUENCY.EVERY_WEEK, 1);
        
        Plan plan = new Plan();
        plan.setNumberOfBins(2);
        brokenAccount1.setPlan(plan);
        
        activeAccounts.add(brokenAccount1);

        activeAccounts.add(makeAccount(FREQUENCY.BI_WEEKLY, 2));
        Account brokenAccount2 = makeAccount(FREQUENCY.BI_WEEKLY, 1);
        
        Plan plan2 = new Plan();
        plan2.setNumberOfBins(2);
        brokenAccount2.setPlan(plan2);
        
        activeAccounts.add(brokenAccount2);

        activeAccounts.add(makeAccount(FREQUENCY.ONCE_PER_MONTH, 3));
        Account brokenAccount3 = makeAccount(FREQUENCY.ONCE_PER_MONTH, 4);

        Plan plan3 = new Plan();
        plan3.setNumberOfBins(2);
        brokenAccount3.setPlan(plan3);
        
        activeAccounts.add(brokenAccount3);

        when(accountRepository.findActive()).thenReturn(activeAccounts);

        Plan everyWeekPlan = new Plan();
        everyWeekPlan.setFrequency(FREQUENCY.EVERY_WEEK);
        everyWeekPlan.setNumberOfBins(1);
        everyWeekPlan.setPrice(100);

        Plan biPlan = new Plan();
        biPlan.setFrequency(FREQUENCY.BI_WEEKLY);
        biPlan.setNumberOfBins(1);
        biPlan.setPrice(75);

        Plan oncePlan = new Plan();
        oncePlan.setFrequency(FREQUENCY.ONCE_PER_MONTH);
        oncePlan.setNumberOfBins(1);
        oncePlan.setPrice(65);

        DateTime dateTime = new DateTime();
        dateTime = dateTime.minusMonths(4);

        //
        service.invoiceRun(dateTime);

        //
        verify(invoiceRepository, times(3)).save(argThat(new IsInvoiceWithDate(dateTime.toDate())));

        verify(notificationService, times(1)).sendEmail(contains(", Error invoicing account Acc No: 1, acc holder: , address: Mock for Address --> null , Error invoicing account Acc No: 1, acc holder: , address: Mock for Address --> null , Error invoicing account Acc No: 4, acc holder: , address: Mock for Address --> null "));

    }

    private Account makeAccount(FREQUENCY biWeekly, int i) {

        Person accountHolder = new Person();
        accountHolder.setPhoneNumbers(new ArrayList<PhoneNumber>());
        accountHolder.setEmailAddresses(new ArrayList<EmailAddress>());

        if (i % 2 == 0) {

            PhoneNumber phone = new PhoneNumber();
            phone.setNumber("0721153215");
            phone.setNumberType(NumberType.CELL);
            accountHolder.getPhoneNumbers().add(phone);
        } else {

            EmailAddress address = new EmailAddress();
            address.setEmail("noone@nowhere.com");
            accountHolder.getEmailAddresses().add(address);
        }

        Account account = new Account();
        account.setAccountNumber(i + "");
        
        Plan plan = new Plan();
        plan.setFrequency(biWeekly);
        plan.setNumberOfBins(1);
        plan.setPrice(50);
        
        account.setPlan(plan);
        
        account.setAccountHolder(accountHolder);
        Address address = mock(Address.class);
        when(address.toString()).thenReturn("Mock for Address");
        account.setAddress(address);
        return account;
    }

    private class IsInvoiceWithServiceCount extends ArgumentMatcher<Invoice> {

        private int serviceCount;

        public IsInvoiceWithServiceCount(int serviceCount) {
            this.serviceCount = serviceCount;
        }

        @Override
        public boolean matches(Object argument) {

            if (!(argument instanceof Invoice)) {
                return false;
            }

            Invoice other = (Invoice) argument;

            return other.getServiceCount().equals(serviceCount);

        }
    }

    private class IsInvoiceWithDate extends ArgumentMatcher<Invoice> {

        private Date date;

        public IsInvoiceWithDate(Date date) {
            this.date = date;
        }

        @Override
        public boolean matches(Object argument) {

            if (!(argument instanceof Invoice)) {
                return false;
            }

            Invoice other = (Invoice) argument;

            return other.getDate().equals(date);

        }
    }
}
