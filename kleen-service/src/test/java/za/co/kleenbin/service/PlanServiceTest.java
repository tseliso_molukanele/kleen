package za.co.kleenbin.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.dozer.DozerBeanMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import za.co.kleen.dao.entity.type.FREQUENCY;
import za.co.kleen.dao.repository.PlanRepository;
import za.co.kleenbin.dto.Plan;

@RunWith(MockitoJUnitRunner.class)
public class PlanServiceTest {

	@Mock PlanRepository planRepository;
	
	@Spy DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
	
	@InjectMocks PlanService service;
	
	@Test
	public void should_return_saved_plan_given_a_plan_to_save() {
		
		//
		final Long id = 99l;
		
		Plan inputPlan = 
				PlanBuilder
					.aPlan()
					.withFrequency(FREQUENCY.BI_WEEKLY)
					.withPrice(30)
					.buildConverted();
		
		Mockito.when(planRepository.save(Matchers.any(za.co.kleen.dao.entity.Plan.class))).thenAnswer(new Answer<za.co.kleen.dao.entity.Plan>() {

			@Override
			public za.co.kleen.dao.entity.Plan answer(InvocationOnMock invocation) throws Throwable {
				
				za.co.kleen.dao.entity.Plan plan = (za.co.kleen.dao.entity.Plan) invocation.getArguments()[0];
				
				plan.setId(id);
				
				return plan;
			}
			
		});
		
		Plan outputPlan = service.newPlan(inputPlan);
		
		//
		Mockito.verify(planRepository, Mockito.times(1)).save(Matchers.any((za.co.kleen.dao.entity.Plan.class)));
		assertThat(outputPlan.getDisplayName(), is(inputPlan.getDisplayName()));
		assertThat(outputPlan.getId(), is(id));
	
	}
	
	@Test
	public void should_return_no_plan_given_a_plan_to_update_and_a_non_matching_id() {
		
		//
		final Long id = 99l;
		
		Plan inputPlan = 
				PlanBuilder
					.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
						.withPrice(30)
					.buildConverted();
		
		Mockito.when(planRepository.findOne(id)).thenReturn(null);
		
		Plan outputPlan = service.updatePlan(id, inputPlan);
		
		//
		Mockito.verify(planRepository, Mockito.never()).save(Matchers.any((za.co.kleen.dao.entity.Plan.class)));
		assertNull(outputPlan);
	}
	
	@Test
	public void should_return_saved_plan_given_a_plan_to_update() {
		
		//
		final Long id = 99l;
		
		Plan inputPlan = 
				PlanBuilder
					.aPlan()
						.withFrequency(FREQUENCY.BI_WEEKLY)
						.withPrice(30)
					.buildConverted();
		
		Mockito.when(planRepository.findOne(id)).thenAnswer(new Answer<za.co.kleen.dao.entity.Plan>() {

			@Override
			public za.co.kleen.dao.entity.Plan answer(InvocationOnMock invocation) throws Throwable {
				
				za.co.kleen.dao.entity.Plan plan = new za.co.kleen.dao.entity.Plan();
				
				plan.setId(id);
				
				return plan;
			}
			
		});
		
		Mockito.when(planRepository.save(Matchers.any(za.co.kleen.dao.entity.Plan.class))).thenAnswer(new Answer<za.co.kleen.dao.entity.Plan>() {

			@Override
			public za.co.kleen.dao.entity.Plan answer(InvocationOnMock invocation) throws Throwable {
				
				za.co.kleen.dao.entity.Plan plan = (za.co.kleen.dao.entity.Plan) invocation.getArguments()[0];
				
				plan.setId(id);
				
				return plan;
			}
			
		});
		
		Plan outputPlan = service.updatePlan(id, inputPlan);
		
		//
		Mockito.verify(planRepository, Mockito.times(1)).save(Matchers.any((za.co.kleen.dao.entity.Plan.class)));
		assertThat(outputPlan.getDisplayName(), is(inputPlan.getDisplayName()));
		assertThat(outputPlan.getId(), is(id));
	}

}
