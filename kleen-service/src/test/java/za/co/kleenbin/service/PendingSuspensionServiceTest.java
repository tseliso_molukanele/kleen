package za.co.kleenbin.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleen.dao.entity.Proprietor;
import za.co.kleen.dao.repository.ProprietorRepository;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.Address;
import za.co.kleenbin.dto.EmailAddress;
import za.co.kleenbin.dto.Person;
import za.co.kleenbin.dto.PhoneNumber;
import za.co.kleenbin.dto.type.NumberType;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@RunWith(MockitoJUnitRunner.class)
public class PendingSuspensionServiceTest {

	@InjectMocks
	private PendingSuspensionService service = new PendingSuspensionService();
	
	@Mock
	private ProprietorRepository proprietorRepository;
	
	@Mock
	private AccountService accountService;
	
	@Mock
	private SMSSender smsSender;
	
	@Mock
	private Client messagingClient;
	
	@Mock
	private NotificationService notificationService;
	
	@Mock
	private VelocityEngine engine;
	
	@Spy
	private ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void setup() {
		Template template = mock(Template.class);
		when(engine.getTemplate(any(String.class))).thenReturn(template );
	}
	
	@Test
	public void testNoAccountsAtAll() {

		when(accountService.getEligableForPendingSuspension()).thenReturn(new ArrayList<Account>());

		//
		service.reminderRun();

		//

		verify(proprietorRepository, never()).save(any(Proprietor.class));
		verify(messagingClient, never()).send(any(String.class), any(String.class));
		verifyNoMoreInteractions(proprietorRepository, messagingClient);

	}

	@Test
	public void test1SMS1Email1NoContact() throws IOException {

		ArrayList<Account> accounts = new ArrayList<Account>();
		accounts.add(getNormalSMSAccount("101"));
		accounts.add(getNormalEmailAccount("102"));
		accounts.add(getNormalNoContactAccount("103"));
		when(accountService.getEligableForPendingSuspension()).thenReturn(accounts);

		//
		service.reminderRun();

		//
		verify(proprietorRepository, times(1)).save(argThat(new IsProprioterWithProperties(getProperties("101"))));
		verify(proprietorRepository, times(1)).save(argThat(new IsProprioterWithProperties(getProperties("103"))));
		verify(smsSender, times(2)).sendSMS(any(Account.class), eq(MessageCreatorType.PENDING_SUSPENSION_SMS));

		verify(proprietorRepository, times(1)).save(argThat(new IsProprioterWithProperties(getProperties("102"))));
		verify(messagingClient, times(1)).send(any(String.class), contains("Pending Account Suspension for 102"));
		
		verify(notificationService, times(1)).sendEmail("Accounts Marked For Suspension Today", "3 accounts processed");
	}

	@Test
	public void should_notify_1_account_given_one_is_active_another_is_OTHER_and_another_is_has_not_properties() throws IOException {

		//
		String errorEmailQueue = "errorEmailQueue";
		String errorEmail = "error@kbfg.co.za";
		String smsQueue = "smsQueue";
		String accountNotNotified = "101";
		String accountAlreadyNotified = "102";
		String accountAlreadyNotifiedWithOtherStatus = "103";

		List<Proprietor> proprietorsForActive = new ArrayList<>();
		proprietorsForActive.add(getPropriotor(
				PendingSuspensionService.PROPRIOTER_ACCOUNT_NUMBER, accountAlreadyNotified,
				PendingSuspensionService.PROPRIOTER_STATUS, "Active",
				PendingSuspensionService.PROPRIOTER_PRIMARY_TYPE, "PendingSuspension"));

		List<Proprietor> proprietorsForOtherStatus = new ArrayList<>();
		proprietorsForOtherStatus.add(getPropriotor(
				PendingSuspensionService.PROPRIOTER_ACCOUNT_NUMBER, accountAlreadyNotifiedWithOtherStatus,
				PendingSuspensionService.PROPRIOTER_STATUS, "Other",
				PendingSuspensionService.PROPRIOTER_PRIMARY_TYPE, "PendingSuspension"));

		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_ACCOUNT_NUMBER, accountAlreadyNotifiedWithOtherStatus)).thenReturn(proprietorsForOtherStatus);
		when(proprietorRepository.findByKeyValue(PendingSuspensionService.PROPRIOTER_ACCOUNT_NUMBER, accountAlreadyNotified)).thenReturn(proprietorsForActive);

		ArrayList<Account> accounts = new ArrayList<Account>();
		accounts.add(getNormalSMSAccount(accountNotNotified));
		accounts.add(getNormalSMSAccount(accountAlreadyNotified));
		accounts.add(getNormalSMSAccount(accountAlreadyNotifiedWithOtherStatus));
		when(accountService.getEligableForPendingSuspension()).thenReturn(accounts);

		PendingSuspensionService service = new PendingSuspensionService();
		service.setProprietorRepository(proprietorRepository);
		service.setMessagingClient(messagingClient);
		service.setAccountService(accountService);
		service.setMapper(mapper);
		service.setSmsSender(smsSender);
		service.setNotificationService(getErrorReportingService(messagingClient, mapper, errorEmailQueue, errorEmail));

		//
		service.reminderRun();

		//
		verify(proprietorRepository, times(1)).save(argThat(new IsProprioterWithProperties(getProperties(accountNotNotified))));
		verify(smsSender, times(1)).sendSMS(any(Account.class),eq(MessageCreatorType.PENDING_SUSPENSION_SMS));

		verify(messagingClient, times(1)).send(eq(errorEmailQueue), contains(
				"{\"attachments\":null,\"emailText\":\"1 accounts processed\",\"emails\":[\"error@kbfg.co.za\"],\"subject\":\"Accounts Marked For Suspension Today\"}"));
		
	}

	private NotificationService getErrorReportingService(Client messagingClient, ObjectMapper mapper, String errorEmailQueue, String errorEmail) {
		NotificationService notificationService = new NotificationService();
		notificationService.setErrorEmail(errorEmail);
		notificationService.setMessagingClient(messagingClient);
		notificationService.setEmailQueue(errorEmailQueue);
		notificationService.setMapper(mapper);
		return notificationService;
	}

	private Account getNormalSMSAccount(String accountNumber) {

		Person accountHolder = getSMSablePerson(accountNumber);
		return getNormalAccount(accountNumber, accountHolder);
	}

	private Account getNormalEmailAccount(String accountNumber) {

		Person accountHolder = getMailablePerson(accountNumber);
		return getNormalAccount(accountNumber, accountHolder);
	}

	private Account getNormalNoContactAccount(String accountNumber) {

		return getNormalAccount(accountNumber, mock(Person.class));
	}

	private Person getSMSablePerson(String accountNumber) {
		PhoneNumber pNumber = new PhoneNumber();
		pNumber.setNumber("0711140" + accountNumber);
		pNumber.setNumberType(NumberType.CELL);
		List<PhoneNumber> numbers = new ArrayList<>();
		numbers.add(pNumber);
		Person accountHolder = mock(Person.class);
		when(accountHolder.getPhoneNumbers()).thenReturn(numbers);
		return accountHolder;
	}

	private Person getMailablePerson(String accountNumber) {
		EmailAddress emailAdd = new EmailAddress();
		emailAdd.setEmail(accountNumber + "@email.com");
		List<EmailAddress> emails = new ArrayList<>();
		emails.add(emailAdd);
		Person accountHolder = mock(Person.class);
		when(accountHolder.getEmailAddresses()).thenReturn(emails);
		return accountHolder;
	}

	private Account getNormalAccount(String accountNumber, Person accountHolder) {

		Account account = new Account();

		account.setAccountNumber(accountNumber);
		account.setBalance(2010f);
		account.setAccountHolder(accountHolder);
		account.setAddress(mock(Address.class));

		return account;
	}

	private Proprietor getPropriotor(String ... keyValuePairs) {

		Proprietor proprietor = new Proprietor();

		Map<String, String> ret = new HashMap<String, String>();

		if(keyValuePairs.length % 2 > 0) {
			throw new RuntimeException("Creating a propriotor required and even number of arguments");
		}

		for(int i = 0; i<keyValuePairs.length;i += 2) {
			ret.put(keyValuePairs[i], keyValuePairs[i + 1]);
		}

		proprietor.setProperties(ret);

		return proprietor;
	}

	private Map<String, String> getProperties(String accountNumber) {
		Map<String, String> ret = new HashMap<String, String>();

		ret.put(PendingSuspensionService.PROPRIOTER_PRIMARY_TYPE, "PendingSuspension");
		ret.put(PendingSuspensionService.PROPRIOTER_STATUS, "Active");
		ret.put(PendingSuspensionService.PROPRIOTER_ACCOUNT_NUMBER, accountNumber);
		DateTime now = new DateTime();
		DateTime twoWeeksFromNow = now.plusDays(14);
		ret.put(PendingSuspensionService.PROPRIOTER_ACTION_ACTIVATION_DATE, new SimpleDateFormat("dd MMM yyy").format(twoWeeksFromNow.toDate()));

		return ret;
	}

	private class IsProprioterWithProperties extends ArgumentMatcher<Proprietor> {

		private Map<String, String> properties;

		public IsProprioterWithProperties(Map<String, String> properties) {
			this.properties = properties;
		}

		@Override
		public boolean matches(Object argument) {

			if (!(argument instanceof Proprietor)) {
				System.out.println(argument + " is not of type " + Proprietor.class);
				return false;
			}

			Proprietor other = (Proprietor) argument;

			for (String key : this.properties.keySet()) {

				if (!other.getProperties().containsKey(key)) {

					System.out.println(argument + " does not contain " + key);
					return false;
				}

				if (!other.getProperties().get(key).equals(this.properties.get(key))) {

					System.out.println(argument + " value of " + key + " is not equal to " + this.properties.get(key) + " but " + other.getProperties().get(key) + " instead");
					return false;
				}
			}

			return true;
		}
	}
}
