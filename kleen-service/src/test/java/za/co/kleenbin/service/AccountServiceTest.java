
package za.co.kleenbin.service;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.entity.type.AccountStatus;
import za.co.kleen.dao.entity.type.FREQUENCY;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleen.dao.repository.AddressRepository;
import za.co.kleen.dao.repository.PersonRepository;
import za.co.kleen.dao.repository.PlanRepository;
import za.co.kleenbin.service.exception.NoFuturePlanException;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest extends CanCreateNotificationService {

	@InjectMocks
	AccountService service;
	
	@Mock
	AccountRepository accountRepository;
	
	@Mock
	NotificationService notificationService;
	
	@Mock
	private DozerBeanMapper dozerBeanMapper;
	
	@Mock
	private PersonRepository personRepository;
	
	@Mock
	private AddressRepository addressRepository;
	
	@Mock
	private PlanRepository planRepository;
	
	@Before
	public void  setup() {
		
		when(dozerBeanMapper.map(any(Account.class), eq(Account.class))).thenReturn(new Account());
	}
	
	private class IsAccountWithStatus extends ArgumentMatcher<Account> {

		private AccountStatus status;

		public IsAccountWithStatus(AccountStatus status) {
			this.status = status;
		}

		@Override
		public boolean matches(Object argument) {

			if (!(argument instanceof Account)) {
				return false;
			}

			Account other = (Account) argument;

			return status.equals(other.getStatus());
		}
	}

	private class IsAccountWithPlans extends ArgumentMatcher<Account> {

		private Plan plan;
		private Plan futurePlan;

		public IsAccountWithPlans(Plan plan, Plan futurePlan) {
			this.plan = plan;
			this.futurePlan = futurePlan;
		}

		@Override
		public boolean matches(Object argument) {

			Account other = (Account) argument;

			boolean plansEqual = plan.equals(other.getPlan());
			boolean futurePlansEquals = (futurePlan == null ? other.getFuturePlan() == null : futurePlan.equals(other.getFuturePlan()));
			
			return plansEqual && futurePlansEquals ;
		}
	}

	@Test
	public void test_update_disabledToInactive() {

		//
		AccountStatus startStatus = AccountStatus.DISABLED;
		AccountStatus endStatus = AccountStatus.INACTIVE;
		String emailSubject = "Account Update";
		String emailContents = " has been uped from 'DISABLED' to 'INACTIVE'";

		testUpdate(startStatus, endStatus, true, emailSubject, emailContents);
	}

	@Test
	public void test_update_inactiveToDisabled() {

		//
		AccountStatus startStatus = AccountStatus.INACTIVE;
		AccountStatus endStatus = AccountStatus.DISABLED;
		String emailSubject = "Account Update";
		String emailContents = " has been downed from 'INACTIVE' to 'DISABLED'";

		testUpdate(startStatus, endStatus, false, emailSubject, emailContents);
	}

	@Test
	public void test_update_inactiveToActive() {

		//
		AccountStatus startStatus = AccountStatus.INACTIVE;
		AccountStatus endStatus = AccountStatus.ACTIVE;
		String emailSubject = "Account Update";
		String emailContents = " has been uped from 'INACTIVE' to 'ACTIVE'";

		testUpdate(startStatus, endStatus, true, emailSubject, emailContents);
	}

	@Test
	public void test_update_activeToInactive() {

		//
		AccountStatus startStatus = AccountStatus.ACTIVE;
		AccountStatus endStatus = AccountStatus.INACTIVE;
		String emailSubject = "Account Update";
		String emailContents = " has been downed from 'ACTIVE' to 'INACTIVE'";

		testUpdate(startStatus, endStatus, false, emailSubject, emailContents);
	}

	private void testUpdate(AccountStatus startStatus, AccountStatus endStatus, boolean up, String emailSubject,
			String emailContents) {
		String accountNumber = "10";
		Account account = new Account();
		account.setStatus(startStatus);

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.findByAccountNumber(accountNumber)).thenReturn(account);

		NotificationService notificationService = mock(NotificationService.class);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);
		service.setNotificationService(notificationService);
		service.setDozerBeanMapper(new DozerBeanMapper());

		//
		service.update(accountNumber, up);

		//
		verify(accountRepository, times(1)).findByAccountNumber(accountNumber);
		verify(accountRepository, times(1)).save(argThat(new IsAccountWithStatus(endStatus)));
		verify(notificationService, times(1)).sendEmail(contains(emailSubject), contains(emailContents));

		verifyNoMoreInteractions(accountRepository, notificationService);
	}

	@Test
	public void test_updateNoStatus() {

		//
		String accountNumber = "10";
		Account account = new Account();

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.findByAccountNumber(accountNumber)).thenReturn(account);

		NotificationService notificationService = mock(NotificationService.class);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);
		service.setNotificationService(notificationService);
		service.setDozerBeanMapper(new DozerBeanMapper());

		//
		service.update(accountNumber, true);

		//
		verify(accountRepository, times(1)).findByAccountNumber(accountNumber);
		verify(notificationService, times(1)).sendEmail(contains("Error: Account Data"), contains(" has no status"));

		verifyNoMoreInteractions(accountRepository, notificationService);
	}

	@Test
	public void test_updateCeiling() {

		//
		String accountNumber = "10";
		Account account = new Account();
		account.setStatus(AccountStatus.ACTIVE);

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.findByAccountNumber(accountNumber)).thenReturn(account);

		NotificationService notificationService = mock(NotificationService.class);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);
		service.setNotificationService(notificationService);
		service.setDozerBeanMapper(new DozerBeanMapper());

		//
		service.update(accountNumber, true);

		//
		verify(accountRepository, times(1)).findByAccountNumber(accountNumber);

		verifyNoMoreInteractions(accountRepository, notificationService);
	}

	@Test
	public void getEligableForPendingSuspension_2LowOwing_2HighOwing_with2invoices() {

		String accountNumber1 = "101";
		String accountNumber2 = "102";
		String accountNumber3 = "103";
		String accountNumber4 = "104";

		//
		List<Account> latePayers = new ArrayList<>();
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber1)
		.withInvoices(2, 100)
		.withBalance(50f)
		.build());
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber2)
		.withInvoices(2, 100)
		.withBalance(150f)
		.build());
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber3)
		.withInvoices(2, 100)
		.withBalance(250f)
		.build());
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber4)
		.withInvoices(2, 100)
		.withBalance(350f)
		.build());

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.getLatePayers(60)).thenReturn(latePayers);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);
		service.setDozerBeanMapper(new DozerBeanMapper());

		//
		List<za.co.kleenbin.dto.Account> accounts = service.getEligableForPendingSuspension();

		//
		assertEquals(2, accounts.size());

		assertEquals(accountNumber3, accounts.get(0).getAccountNumber());
		assertEquals(accountNumber4, accounts.get(1).getAccountNumber());
	}

	@Test
	public void getEligableForPendingSuspension_1LowOwing_1HighOwing_with1Invoice() {

		String accountNumber1 = "101";
		String accountNumber2 = "102";
		String accountNumber3 = "103";
		String accountNumber4 = "104";

		//
		List<Account> latePayers = new ArrayList<>();
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber2)
		.withInvoices(1, 100)
		.withBalance(150f)
		.build());
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber3)
		.withInvoices(1, 100)
		.withBalance(250f)
		.build());

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.getLatePayers(60)).thenReturn(latePayers);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);

		//
		List<za.co.kleenbin.dto.Account> accounts = service.getEligableForPendingSuspension();

		//
		assertEquals(0, accounts.size());
	}

	@Test
	public void getEligableForPendingSuspension_1LowOwing_1HighOwing_with0Invoice() {

		String accountNumber1 = "101";
		String accountNumber2 = "102";
		String accountNumber3 = "103";
		String accountNumber4 = "104";

		//
		List<Account> latePayers = new ArrayList<>();
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber2)
		.withInvoices(0, 100)
		.withBalance(150f)
		.build());
		latePayers.add(AccountBuilder
		.anAccount()
		.withAccountNumber(accountNumber3)
		.withInvoices(0, 100)
		.withBalance(250f)
		.build());

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.getLatePayers(60)).thenReturn(latePayers);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);

		//
		List<za.co.kleenbin.dto.Account> accounts = service.getEligableForPendingSuspension();

		//
		assertEquals(0, accounts.size());
	}

	@Test
	public void getEligableForPendingIncrease__compareMonthOfPlanWithNow__2PreviousMonth_2CurrentMonth_2NextMonth_2FollowingMonth() {

		String accountNumber1 = "101";
		String accountNumber2 = "102";

		//
		List<Account> accountsWithPlans = new ArrayList<>();
		accountsWithPlans.add(AccountBuilder.anAccount().withAccountNumber(accountNumber1).build());
		accountsWithPlans.add(AccountBuilder.anAccount().withAccountNumber(accountNumber2).build());

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.getActiveStartedInMonth((new DateTime().getMonthOfYear() + 1) % 12))
				.thenReturn(accountsWithPlans);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);
		service.setDozerBeanMapper(new DozerBeanMapper());

		//
		List<za.co.kleenbin.dto.Account> accounts = service.getEligableForPriceIncrease();

		//
		assertEquals(2, accounts.size());

		assertEquals(accountNumber1, accounts.get(0).getAccountNumber());
		assertEquals(accountNumber2, accounts.get(1).getAccountNumber());
	}
	
	@Test
	public void should_return_false_if_no_account_found() {
		
		//
		AccountService service = new AccountService();
		service.setAccountRepository(mock(AccountRepository.class));
		
		//
		boolean result = service.upgradePricePlan("101");
		
		//
		assertFalse(result);
		
	}

	@Test
	public void should_call_account_repo_with_upgraded_account_given_an_account_with_plan_and_futureplan() {

		Plan currentPlan = new Plan();
		currentPlan.setYear(2011);
		currentPlan.setFrequency(FREQUENCY.EVERY_WEEK);
		currentPlan.setNumberOfBins(2);
		
		Plan futurePlan = new Plan();
		futurePlan.setYear(2012);
		futurePlan.setFrequency(FREQUENCY.EVERY_WEEK);
		futurePlan.setNumberOfBins(2);

		String accountNumber = "101";
		Account account = new Account();
		account.setPlan(currentPlan);
		account.setFuturePlan(futurePlan);

		AccountRepository accountRepository = mock(AccountRepository.class);
		when(accountRepository.findByAccountNumber(accountNumber)).thenReturn(account);

		Plan replacementFuturePlan = new Plan();
		replacementFuturePlan.setYear(2013);
		replacementFuturePlan.setFrequency(FREQUENCY.EVERY_WEEK);
		replacementFuturePlan.setNumberOfBins(2);
		replacementFuturePlan.setPrice(30);

		PlanRepository planRepository = mock(PlanRepository.class);
		when(planRepository.findByFrequencyAndNumberOfBinsAndYear(futurePlan.getFrequency(), futurePlan.getNumberOfBins(),
				futurePlan.getYear() + 1)).thenReturn(replacementFuturePlan);

		AccountService service = new AccountService();
		service.setAccountRepository(accountRepository);
		service.setPlanRepository(planRepository);

		service.upgradePricePlan(accountNumber);

		verify(accountRepository, times(1)).save(argThat(new IsAccountWithPlans(futurePlan, null)));
	}

	@Test(expected=NoFuturePlanException.class)
	public void expects_plannotfound_when_upgrading_with_no_future_plan() {
		
		//
		String accountNumber = "101";
		Account account = AccountBuilder.anAccount().build();
		when(accountRepository.findByAccountNumber(accountNumber)).thenReturn(account);
		
		//
		service.upgradePricePlan(accountNumber);
		
		//
	}
	
	@Test
	public void should_notify_new_client_of_new_account() throws IOException {
		
		//
		za.co.kleenbin.dto.Account account = AccountBuilder.anAccount()
					.withPlan(
							PlanBuilder.aPlan()
								.withYear(2017)
							.build())
				.buildConverted();
		
		//
		service.newAccount(account);
		
		//
		verify(accountRepository, times(1)).save(any(Account.class));
		verify(notificationService, times(1)).sendMessage(null, MessageCreatorType.NEW_ACCOUNT_SMS, MessageCreatorType.NEW_ACCOUNT_EMAIL);
	}
}
