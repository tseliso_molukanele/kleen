package za.co.kleenbin.service;

import org.mockito.ArgumentMatcher;

import za.co.kleenbin.dto.Plan;

public class PlanWithSameFieldsAs extends ArgumentMatcher<Plan> {

	private Plan plan;
	
	public PlanWithSameFieldsAs(Plan plan) {
		
		this.plan = plan;
	}
	
	@Override
	public boolean matches(Object item) {
		
		Plan other = (Plan)item;
		
		return other.getId().equals(plan.getId())
				&& other.getFrequency().equals(plan.getFrequency())
				&& other.getNumberOfBins().equals(plan.getNumberOfBins())
				&& other.getPrice().equals(plan.getPrice())
				&& other.getYear().equals(plan.getYear());
	}
}