package za.co.kleenbin.conf;

import org.dozer.DozerBeanMapper;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import za.co.kleen.dao.repository.AccountActionRepository;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleen.dao.repository.AddressRepository;
import za.co.kleen.dao.repository.InvoiceRepository;
import za.co.kleen.dao.repository.PaymentRepository;
import za.co.kleen.dao.repository.PersonRepository;
import za.co.kleen.dao.repository.PlanRepository;
import za.co.kleen.dao.repository.ServiceRepository;

@Configuration
@ComponentScan({ "za.co.kleenbin.service" })
public class TestConfig {

    @Bean
    public PaymentRepository paymentRepository() {

        return Mockito.mock(PaymentRepository.class);
    }

    @Bean
    public AccountRepository accountRepository() {

        return Mockito.mock(AccountRepository.class);
    }
    
    @Bean
    public AccountActionRepository accountActionRepository() {
        
        return Mockito.mock(AccountActionRepository.class);
    }

    @Bean
    public AddressRepository addressRepository() {

        return Mockito.mock(AddressRepository.class);
    }

    @Bean
    public PersonRepository personRepository() {

        return Mockito.mock(PersonRepository.class);
    }

    @Bean
    public ServiceRepository serviceRepository() {

        return Mockito.mock(ServiceRepository.class);
    }

    @Bean
    public InvoiceRepository invoiceRepository() {

        return Mockito.mock(InvoiceRepository.class);
    }

    @Bean
    public PlanRepository planRepository() {

        return Mockito.mock(PlanRepository.class);
    }
    
    @Bean
    public DozerBeanMapper dozerBeanMapper() {
    	return new DozerBeanMapper();
    }
    
}
