package za.co.kleenbin.schedule;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.kleenbin.service.NotificationService;
import za.co.kleenbin.service.PendingSuspensionService;
import za.co.kleenbin.service.exception.IrrecoverableException;

@RunWith(MockitoJUnitRunner.class)
public class PendingSuspensionReminderTest {

	@InjectMocks
	PendingSuspensionReminder reminder;
	
	@Mock
	PendingSuspensionService service;
	
	@Mock
	NotificationService notificationService;
	
	@Test
	public void testRunReminder() {
		PendingSuspensionReminder reminder = new PendingSuspensionReminder();
		PendingSuspensionService service = mock(PendingSuspensionService.class);
		
		reminder.setPendingSuspensionService(service);
		
		//
		reminder.runReminder();
		
		//
        verify(service, times(1)).reminderRun();
        verifyNoMoreInteractions(service);
	}
	
	@Test
	public void testRunSuspender() {
		PendingSuspensionReminder reminder = new PendingSuspensionReminder();
		PendingSuspensionService service = mock(PendingSuspensionService.class);
		
		reminder.setPendingSuspensionService(service);
		
		//
		reminder.runSuspend();
		
		//
        verify(service, times(1)).suspensionRun();
        verifyNoMoreInteractions(service);
	}
	
	@Test
	public void should_send_error_message_if_runReminder_fails() {
		
		//
		String innerMessage = "Something terrible happened";
		String errorMessage = "Error notifying customer of pending supensions: " + "java.lang.RuntimeException: " + innerMessage;
		doThrow(new IrrecoverableException(new RuntimeException(innerMessage))).when(service).reminderRun();
		
		//
		reminder.runReminder();
		
		//
		verify(notificationService, times(1)).sendEmail(errorMessage);
	}
	
	
	@Test
	public void should_send_error_message_if_suspensionRun_fails() {
		
		//
		String innerMessage = "Something terrible happened";
		String errorMessage = "Error running scheduled supensions: " + "java.lang.RuntimeException: " + innerMessage;
		doThrow(new IrrecoverableException(new RuntimeException(innerMessage))).when(service).suspensionRun();
		
		//
		reminder.runSuspend();
		
		//
		verify(notificationService, times(1)).sendEmail(errorMessage);
	}

}
