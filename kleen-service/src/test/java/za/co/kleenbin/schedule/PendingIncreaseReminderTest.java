package za.co.kleenbin.schedule;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.kleenbin.service.NotificationService;
import za.co.kleenbin.service.PendingIncreaseService;
import za.co.kleenbin.service.exception.IrrecoverableException;

@RunWith(MockitoJUnitRunner.class)
public class PendingIncreaseReminderTest {

	@InjectMocks
	PendingIncreaseReminder reminder;
	
	@Mock
	PendingIncreaseService service;  
	
	@Mock
	NotificationService notificationService;
	
	@Test
	public void should_send_error_message_if_runReminder_fails() {
		
		//
		String innerMessage = "Something terrible happened";
		String errorMessage = "Error sending increase reminders: " + "java.lang.RuntimeException: " + innerMessage;
		doThrow(new IrrecoverableException(new RuntimeException(innerMessage))).when(service).reminderRun();
		
		//
		reminder.runReminder();
		
		//
		verify(notificationService, times(1)).sendEmail(errorMessage);
	}
	
	@Test
	public void testRunReminder() {
		
		//
		
		//
		reminder.runReminder();
		
		//
		verify(service, times(1)).reminderRun();
		verifyNoMoreInteractions(service);
	}
	
	@Test
	public void testRunSuspender() {
		
		//
		
		//
		reminder.runIncrease();
		
		//
        verify(service, times(1)).increaseRun();
        verifyNoMoreInteractions(service);
	}
	
	@Test
	public void should_send_error_message_if_runIncrease_fails() {
		
		//
		String innerErrorMessage = "Something terrible happened";
		 
		String errorMessage = "Error running scheduled increase : java.lang.RuntimeException: " + innerErrorMessage;
		doThrow(new IrrecoverableException(new RuntimeException(innerErrorMessage))).when(service).increaseRun();
		
		//
		reminder.runIncrease();
		
		//
		verify(notificationService, times(1)).sendEmail(errorMessage);
	}

}
