package za.co.kleenbin.schedule;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Test;

import za.co.kleenbin.service.InvoiceService;

public class InvoicerTest {

    public static final DateTime _20150613_SAT = new DateTime(2015, 6, 13, 1, 1);
    public static final DateTime _20150620_SAT = new DateTime(2015, 6, 20, 1, 1);
    public static final DateTime _20150621_SUN = new DateTime(2015, 6, 21, 1, 1);
    public static final DateTime _20150627_SAT = new DateTime(2015, 6, 27, 1, 1);

    @After
    public void tearDown() {
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void test_correctWeek_correctDay() {

        DateTimeUtils.setCurrentMillisFixed(_20150620_SAT.getMillis());

        InvoiceService invoiceService = mock(InvoiceService.class);
        
        Invoicer invoicer = new Invoicer();
        invoicer.setInvoiceService(invoiceService);

        //
        invoicer.invoice();
        
        verify(invoiceService, times(1)).invoiceRun();
        verifyNoMoreInteractions(invoiceService);
    }

    @Test
    public void test_incorrectWeek_correctDay() {

        DateTimeUtils.setCurrentMillisFixed(_20150613_SAT.getMillis());

        InvoiceService invoiceService = mock(InvoiceService.class);

        Invoicer invoicer = new Invoicer();
        invoicer.invoice();

        verify(invoiceService, never()).invoiceRun();
        verifyNoMoreInteractions(invoiceService);
    }

    @Test
    public void test_incorrectWeek_incorrectDay() {

        DateTimeUtils.setCurrentMillisFixed(_20150621_SUN.getMillis());

        InvoiceService invoiceService = mock(InvoiceService.class);

        Invoicer invoicer = new Invoicer();
        invoicer.invoice();

        verifyNoMoreInteractions(invoiceService);
    }
    
    @Test
    public void test_afterThe24th() {
        
        DateTimeUtils.setCurrentMillisFixed(_20150627_SAT.getMillis());
        
        InvoiceService invoiceService = mock(InvoiceService.class);
        
        Invoicer invoicer = new Invoicer();
        invoicer.setInvoiceService(invoiceService);

        //
        invoicer.invoice();

        //
        verify(invoiceService, times(1)).invoiceRun();
        verifyNoMoreInteractions(invoiceService);
    }

}
