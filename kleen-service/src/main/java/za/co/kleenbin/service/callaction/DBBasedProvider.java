package za.co.kleenbin.service.callaction;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import za.co.kleen.dao.repository.AccountActionRepository;
import za.co.kleenbin.dto.AccountAction;

public abstract class DBBasedProvider extends SimpleProvider {

    @Autowired
    private AccountActionRepository accountActionRepository;

    @Override
    public List<AccountAction> getNextActions(int count) {

        List<AccountAction> returnableActions = new ArrayList<AccountAction>();

        if (count == 0) {
            return returnableActions;
        }

        List<za.co.kleen.dao.entity.AccountAction> existingActions = accountActionRepository.findByType(getType(), new PageRequest(0, count));

        for (za.co.kleen.dao.entity.AccountAction accountAction : existingActions) {

            AccountAction mapped = map(accountAction);
            returnableActions.add(mapped);
        }

        int currentSize = returnableActions.size();
        
        if (currentSize < count) {
            returnableActions.addAll(super.getNextActions(count - currentSize, true));
        }

        return returnableActions;
    }
}