package za.co.kleenbin.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.EmailAddress;
import za.co.kleen.dao.entity.PhoneNumber;
import za.co.kleen.dao.entity.type.NumberType;
import za.co.kleenbin.dto.EmailMessage;
import za.co.kleenbin.dto.SMSMessage;
import za.co.kleenbin.service.exception.ServiceException;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@Service
public class ScheduleSender {

    private static final DateFormat MMMMM_YYYY = new SimpleDateFormat("MMMMM yyyy");

    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private AccountService accountService;

    @Autowired
    private Client messagingClient;
    @Autowired
    private VelocityEngine velocityEngine;
    @Value("${invoice.queue.sms}")
    private String smsQueue;
    @Value("${invoice.queue.email}")
    private String emailQueue;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private NotificationService notificationService;

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public void communicateMissedService(Account account) {

        try {

            List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();

            if (emailAddresses.size() > 0) {

                logger.info(String.format("Sending missed service email to Account %s", account.toString()));
                emailMissedService(account);
                notificationService.sendEmail("Emailed missed service for " + account.toString());
            } else {

                logger.info(String.format("Sending missed service sms to Account %s", account.toString()));
                smsMissedService(account);
                notificationService.sendEmail("SMSed missed service for " + account.toString());
            }

        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    private void emailMissedService(Account account) throws IOException {

        logger.info(String.format("Sending missed service email to Account %s", account.toString()));

        Date now = new Date();

        String subject = "Missed service on date " + MMMMM_YYYY.format(now) + " for account " + account.getAccountNumber();

        List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();
        String[] emails = new String[emailAddresses.size()];

        for (int i = 0; i < emailAddresses.size(); i++) {

            emails[i] = emailAddresses.get(i).getEmail();
        }

        String emailText = new MessageCreator(MessageCreatorType.MISSED_SERVICE_MAIL, velocityEngine).createMessage(accountService.convert(account));

        EmailMessage message = new EmailMessage();
        message.setEmailText(emailText);
        message.setRecipients(emails);
        message.setSubject(subject);

        messagingClient.send(emailQueue, mapper.writeValueAsString(message));
    }

    private void smsMissedService(Account account) throws JsonProcessingException {

        logger.info(String.format("Sending missed service SMS to Account %s", account.toString()));

        boolean sending = false;
        Iterator<PhoneNumber> iter = account.getAccountHolder().getPhoneNumbers().iterator();
        SMSMessage message = null;
        while (!sending && iter.hasNext()) {
            PhoneNumber number = iter.next();

            if (number.getNumberType().equals(NumberType.CELL)) {

                String text;
                try {
                    text = new MessageCreator(MessageCreatorType.MISSED_SERVICE_SMS, velocityEngine).createMessage(accountService.convert(account));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                message = new SMSMessage();
                message.setNumber(number.getNumber());
                message.setCustomerId("ANM-" + account.getAccountNumber());
                message.setMessage(text);

                sending = true;
            }
        }

        if (!sending) {

            logger.info(String.format("Account %s does not have a cell phone to SMS", account.toString()));
        } else {

            logger.info(String.format("Sending invoice - SMS to Account %s", account.toString()) + message);
            messagingClient.send(smsQueue, mapper.writeValueAsString(message));
        }
    }

}
