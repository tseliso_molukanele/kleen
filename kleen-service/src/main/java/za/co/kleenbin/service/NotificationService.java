package za.co.kleenbin.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.EmailMessage;
import za.co.kleenbin.dto.PhoneNumber;
import za.co.kleenbin.dto.SMSMessage;
import za.co.kleenbin.dto.type.NumberType;
import za.co.kleenbin.service.exception.ClientCommunicationNotPossibleException;
import za.co.kleenbin.service.messagecreator.MessageCreatorFactory;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@Service
public class NotificationService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Client messagingClient;
    @Value("${error.sms.number}")
    private String errorSmsNumber;
    @Value("${error.sms.queue}")
    private String smsQueue;
    @Value("${error.email.address}")
    private String errorEmail;
    @Value("${email.queue}")
    private String emailQueue;
    @Autowired
    private ObjectMapper mapper;
    
    @Autowired
    private SMSSender smsSender;
    
    @Autowired
    private MessageCreatorFactory messageCreatorFactory;

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public void setMessagingClient(Client messagingClient) {

        this.messagingClient = messagingClient;
    }

    public void setErrorSmsNumber(String errorSmsNumber) {
        this.errorSmsNumber = errorSmsNumber;
    }

    public void setSmsQueue(String smsQueue) {
        this.smsQueue = smsQueue;
    }

    public void setErrorEmail(String errorEmail) {
        this.errorEmail = errorEmail;
    }

    public void setEmailQueue(String emailQueue) {
        this.emailQueue = emailQueue;
    }

    public void sendSms(String error)  {

        logger.info(String.format("Sending error to number %s", errorSmsNumber));

        SMSMessage message = new SMSMessage();
        message.setNumber(errorSmsNumber);
        message.setCustomerId("KBFG");
        message.setMessage(error + new SimpleDateFormat(": yyyy-MM-dd HH:mm").format(new Date()));

        logger.info("Sending - " + message);

        try {

            messagingClient.send(smsQueue, mapper.writeValueAsString(message));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void sendEmail(String error)  {

        sendEmail("Error-" + new DateTime().toString("yyyyMMddHHmmssSSS"), error);
    }

    public void sendEmail(String subject, String error)  {

        logger.info(String.format("Sending error to email %s", errorEmail));

        EmailMessage message = new EmailMessage();
        message.setEmailText(error);
        message.setRecipients(new String[] {errorEmail});
        message.setSubject(subject);

        logger.info("Sending - " + message);

        try {

            messagingClient.send(emailQueue, mapper.writeValueAsString(message));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	public void sendMessage(Account account, 
			MessageCreatorType smsType, 
			MessageCreatorType emailType) 
			throws IOException {

		String messageText = "";
		
		if(account.getAccountHolder().getEmailAddresses().size() > 0) {
			
			messageText = messageCreatorFactory.get(emailType).createMessage(account);

			EmailMessage message = new EmailMessage();
			message.setEmailText(messageText);
			
			message.setRecipients(account.getAccountHolder().getEmailAddresses().stream().map(em -> em.getEmail()).collect(Collectors.toList()).toArray(new String[] {}));
			message.setSubject("New account activated: " + account.getAccountNumber());
			
			messagingClient.send(emailQueue, mapper.writeValueAsString(message));
			
			return;
		} else {
			
			for(PhoneNumber number : account.getAccountHolder().getPhoneNumbers()) {
			
				if(NumberType.CELL.equals(number.getNumberType())) {
					
					smsSender.sendSMS(account, smsType);
					return;
				}
			}
		}
		
		throw new ClientCommunicationNotPossibleException(String.format("No email or Cell for account %s", account));
		
	}
}
