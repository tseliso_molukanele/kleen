package za.co.kleenbin.service.messagecreator;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.service.exception.IncompleteAccountException;

public class MessageCreator {

	private VelocityEngine engine;
	private MessageCreatorType type;

	public MessageCreator(MessageCreatorType type, VelocityEngine engine) {

		this.type = type;
		this.engine = engine;
	}

	public String createMessage(Account account) throws IOException {

		JXPathContext context = JXPathContext.newContext(account);

		for (String requiredObject : this.type.getRequiredObjects()) {

			if (context.getValue(requiredObject) == null) {

				throw new IncompleteAccountException("'" + requiredObject + "' is required");
			}
		}

		Map<String, Object> attributes = new HashMap<String, Object>();
		attributes.put("account", account);

		return velocity(attributes);
	}

	private String velocity(Map<String, Object> attributes) throws IOException {
		StringWriter result = new StringWriter();
		
		Template template = engine.getTemplate(this.type.getTemplate());
		
		Context context = new VelocityContext();
		
		context.put("dateTool", new DateTool());
		context.put("numberTool", new NumberTool());
		
		if (attributes != null) {
		    for (String key : attributes.keySet()) {
		
		        context.put(key, attributes.get(key));
		    }
		}
		
		template.merge(context, result);
		
		result.close();
		return result.toString();
	}

}