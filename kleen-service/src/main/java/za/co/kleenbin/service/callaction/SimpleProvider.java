package za.co.kleenbin.service.callaction;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import za.co.kleen.dao.repository.AccountActionRepository;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.AccountAction;
import za.co.kleenbin.dto.type.AccountActionType;

public abstract class SimpleProvider implements AccountActionProvider {

    @Autowired
    private AccountActionRepository accountActionRepository;
    
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    protected List<AccountAction> getNextActions(int fetchSize, boolean persist) {
        List<AccountAction> returnableActions = new ArrayList<AccountAction>();

        List<za.co.kleen.dao.entity.Account> backlog = backlog(fetchSize);

        for (za.co.kleen.dao.entity.Account account : backlog) {

            za.co.kleen.dao.entity.AccountAction newAction = new za.co.kleen.dao.entity.AccountAction();
            newAction.setAccount(account);
            newAction.setType(getType());

            if (persist) {
                
                za.co.kleen.dao.entity.AccountAction saved = accountActionRepository.save(newAction);
                AccountAction accountAction = map(saved);
                returnableActions.add(accountAction);
            } else {
                
                returnableActions.add(map(newAction));
            }

        }
        return returnableActions;
    }
    
    public List<AccountAction> getNextActions(int fetchSize) {
        
        return getNextActions(fetchSize, false);
    }

    protected AccountAction map(za.co.kleen.dao.entity.AccountAction accountAction) {
        AccountAction mapped = new AccountAction();
        mapped.setAccount(dozerBeanMapper.map(accountAction.getAccount(), Account.class));
        mapped.setCreatedDate(accountAction.getCreatedDate());
        mapped.setType(AccountActionType.valueOf(accountAction.getType().name()));
        return mapped;
    }

    protected abstract List<za.co.kleen.dao.entity.Account> backlog(int fetchSize);

    protected abstract za.co.kleen.dao.entity.type.AccountActionType getType();

}