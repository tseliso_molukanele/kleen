package za.co.kleenbin.service.exception;

public class NoFuturePlanException extends RuntimeException {
	
	private static final long serialVersionUID = 5942115502724723494L;

	public NoFuturePlanException(String string) {
		super(string);
	}

}
