package za.co.kleenbin.service.callaction;

import java.util.List;

import za.co.kleenbin.dto.AccountAction;

public interface AccountActionProvider {

    List<AccountAction> getNextActions(int count);

    int getSpotsRequired();

}
