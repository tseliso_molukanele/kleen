package za.co.kleenbin.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.PhoneNumber;
import za.co.kleenbin.dto.SMSMessage;
import za.co.kleenbin.dto.type.NumberType;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@Component
public class SMSSender {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Client messagingClient;
	@Autowired
    private VelocityEngine velocityEngine;
	@Autowired
    private ObjectMapper mapper;

	@Autowired
	private NotificationService notificationService;

	@Value("${invoice.queue.sms}")
	private String smsQueue;

	public void setSmsQueue(String smsQueue) {
		this.smsQueue = smsQueue;
	}

	public void setMessagingClient(Client messagingClient) {
		this.messagingClient = messagingClient;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public void sendSMS(Account account, MessageCreatorType templateName) throws JsonProcessingException {

		boolean sending = false;
		Iterator<PhoneNumber> iter = account.getAccountHolder().getPhoneNumbers().iterator();
		SMSMessage message = null;
		while (!sending && iter.hasNext()) {
			PhoneNumber number = iter.next();

			if (number.getNumberType().equals(NumberType.CELL)) {

				String text;
				try {
					text = new MessageCreator(templateName, velocityEngine).createMessage(account);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}

				message = new SMSMessage();
				message.setNumber(number.getNumber());
				message.setCustomerId("ANM-" + account.getAccountNumber());
				message.setMessage(text);

				Calendar calendar = getSuitableTimeToSend(new Date());

				message.setDateToSend(calendar.getTime());

				sending = true;
			}
		}

		if (!sending) {

			logger.info(String.format("Account %s does not have a cell phone to SMS", account.toString()));
			notificationService.sendEmail(String.format("Account %s does not have a cell phone to SMS", account.toString()));
		} else {

			logger.info(String.format("Sending message - SMS to Account %s", account.toString()) + message);
			messagingClient.send(smsQueue, mapper.writeValueAsString(message));
		}
	}

	public Calendar getSuitableTimeToSend(Date now) {
		Calendar calendar = Calendar.getInstance();
		calendar.setLenient(false);

		calendar.setTime(now);

		if(calendar.get(Calendar.HOUR_OF_DAY) >= 18) {
			
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 8);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
		
		}
		
		if(calendar.get(Calendar.HOUR_OF_DAY) <= 8) {
			
			calendar.set(Calendar.HOUR_OF_DAY, 8);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
		}

		while (!isWeekday(calendar)) {

			calendar.add(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 8);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
		}
		return calendar;
	}
	
	

	private boolean isWeekday(Calendar calendar) {

		int dow = calendar.get(Calendar.DAY_OF_WEEK);
		boolean isWeekday = ((dow >= Calendar.MONDAY) && (dow <= Calendar.FRIDAY));

		return isWeekday;
	}

}
