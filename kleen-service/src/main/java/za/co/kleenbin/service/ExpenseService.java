package za.co.kleenbin.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleen.dao.repository.ExpenseRepository;
import za.co.kleenbin.dto.Expense;
import za.co.kleenbin.service.exception.ServiceException;

@Service
public class ExpenseService {

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private AdvancedAuditorAware advancedAuditorAware;

    @Autowired
    private RolesService rolesService;
    
    @Autowired
	private DozerBeanMapper dozerBeanMapper;

    @Transactional
    public Expense saveExpense(Expense expense) {

        validateExpense(null, expense);

        za.co.kleen.dao.entity.Expense expenseModel = dozerBeanMapper.map(expense, za.co.kleen.dao.entity.Expense.class);

        expenseModel.setRole(advancedAuditorAware.getCurrentAuditorRole());

        za.co.kleen.dao.entity.Expense saved = expenseRepository.save(expenseModel);
        return dozerBeanMapper.map(saved, Expense.class);
    }

    @Transactional
    public Expense updateExpense(Long id, Expense expense) {

        validateExpense(id, expense);

        za.co.kleen.dao.entity.Expense expenseModel = dozerBeanMapper.map(expense, za.co.kleen.dao.entity.Expense.class);

        za.co.kleen.dao.entity.Expense existing = expenseRepository.findOne(id);

        if(!rolesService.isAllowedForRole(existing.getRole())) {

            throw new ServiceException("Do not have permission to make changes to this entry");
        }

        existing.setExpenseType(expenseModel.getExpenseType());
        existing.setAmount(expenseModel.getAmount());
        existing.setDate(expenseModel.getDate());

        za.co.kleen.dao.entity.Expense saved = expenseRepository.save(existing);
        return dozerBeanMapper.map(saved, Expense.class);
    }

    private void validateExpense(Long id, Expense expense) {

        if (expense.getExpenseType() == null) {

            throw new ServiceException("Cannot capture expense without type");
        }

        if (expense.getAmount() < 1) {

            throw new ServiceException("Cannot make expense less then R1");
        }

        if (expense.getDate().after(new Date())) {

            throw new ServiceException("Cannot make expense for the future");
        }

        Calendar from = Calendar.getInstance();
        from.setTime(expense.getDate());
        from.add(Calendar.DATE, -1);

        Calendar to = Calendar.getInstance();
        to.setTime(expense.getDate());
        to.add(Calendar.DATE, 1);

        List<za.co.kleen.dao.entity.Expense> expenses = null;
        if(id != null) {

            expenses = expenseRepository.findByType_amount_dateRange_excl_id(za.co.kleen.dao.entity.type.ExpenseType.valueOf(expense.getExpenseType().toString()), expense.getAmount(), from.getTime(), to.getTime(), id);
        } else {

            expenses = expenseRepository.findByType_amount_dateRange(za.co.kleen.dao.entity.type.ExpenseType.valueOf(expense.getExpenseType().toString()), expense.getAmount(), from.getTime(), to.getTime());
        }

        if (expenses.size() > 0) {

            throw new ServiceException("Expense has already been captured");
        }
    }

    @Transactional
    public List<Expense> getExpenses() {
        
        List<za.co.kleen.dao.entity.Expense> findAllOrderByDateDesc = expenseRepository.findTop50ByOrderByDateDesc();
        
        List<Expense> expenses = new ArrayList<Expense>();

        for (za.co.kleen.dao.entity.Expense expense : findAllOrderByDateDesc) {

            if(rolesService.isAllowedForRole(expense.getRole())) {

                expenses.add(dozerBeanMapper.map(expense, Expense.class));
            }
        }
        
        return expenses ;
    }

    @Transactional
    public List<Expense> getExpenses(DateTime startDate, DateTime endDate) {

        List<za.co.kleen.dao.entity.Expense> found = expenseRepository.findByDateBetween(startDate.toDate(), endDate.toDate());

        List<Expense> expenses = new ArrayList<Expense>();

        for (za.co.kleen.dao.entity.Expense expense : found) {

            if(rolesService.isAllowedForRole(expense.getRole())) {

                expenses.add(dozerBeanMapper.map(expense, Expense.class));
            }
        }

        return expenses ;
    }

    @Transactional
    public Expense getExpense(Long id) {

        za.co.kleen.dao.entity.Expense expense = expenseRepository.findOne(id);

        if(rolesService.isAllowedForRole(expense.getRole())) {

            return dozerBeanMapper.map(expense, Expense.class);
        } else {

            throw new ServiceException("Do not have access to this entry");
        }
    }

    public Integer getExpenseForMonth(DateTime date) {
        return expenseRepository.getExpenseForMonth(date.getYear(), date.getMonthOfYear());
    }
}
