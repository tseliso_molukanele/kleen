package za.co.kleenbin.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.AccountAction;
import za.co.kleen.dao.entity.EmailAddress;
import za.co.kleen.dao.entity.PhoneNumber;
import za.co.kleen.dao.entity.type.AccountActionType;
import za.co.kleen.dao.entity.type.NumberType;
import za.co.kleen.dao.repository.AccountActionRepository;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleenbin.dto.EmailAttachment;
import za.co.kleenbin.dto.EmailMessage;
import za.co.kleenbin.dto.SMSMessage;
import za.co.kleenbin.service.exception.ServiceException;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@Service
public class StatementSender {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private AccountRepository accountRepository;
    
    @Autowired
    private AccountActionRepository aaRepo; 
    
    @Autowired
    private VelocityEngine velocityEngine;
    
    @Autowired
    private Client messagingClient;
    
    @Autowired
    private AccountService accountService;
    
    @Value("${invoice.queue.sms}")
    private String smsQueue;

    @Value("${invoice.queue.email}")
    private String emailQueue;

    @Value("${reportLocation}")
    private String reportLocation;
    
    @Autowired
    private ObjectMapper mapper;
    
    private static final DateFormat YYYYMM = new SimpleDateFormat("yyyyMM");
    private static final DateFormat MMMMM_YYYY = new SimpleDateFormat("MMMMM yyyy");
    
    @Transactional
    public void sendStatements() {
        
        /*
         * Temporary until tasks are sorted out
         */
        List<AccountAction> allActions = aaRepo.findByType(AccountActionType.STATEMENT, new PageRequest(0, 100));

        for (AccountAction accountAction : allActions) {
            
            if(accountAction.getLastModifiedDate().getMonthOfYear() == DateTime.now().getMonthOfYear()) {
                
                logger.info("Statement have already been send this month. Skipping!");
                return;
            }
        }
        
        aaRepo.deleteInBatch(allActions);
        AccountAction entity = new AccountAction();
        entity.setType(AccountActionType.STATEMENT);
        aaRepo.save(entity);
        
        List<Account> accounts = accountRepository.findActive();
        
        for (Account account : accounts) {
            
            List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();

            try {
                if (emailAddresses.size() > 0) {

                    emailStatement(account);
                } else {

                    smsStatement(account);
                }

            } catch (Exception e) {
                logger.error("Error sending statement to account {} --> {} ", account.toString(), e.getMessage());
            }
        }
    }

    public void emailStatement(Account account) {
        logger.info(String.format("Sending statement to Account %s", account.toString()));

        Date now = new Date();

        String subject = "Statement for " + MMMMM_YYYY.format(now) + " for account " + account.getAccountNumber();

        List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();
        String[] emails = new String[emailAddresses.size()];

        for (int i = 0; i < emailAddresses.size(); i++) {

            emails[i] = emailAddresses.get(i).getEmail();
        }

        try {
            String emailText = new MessageCreator(MessageCreatorType.STATEMENT_MAIL, velocityEngine).createMessage(accountService.convert(account));

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(reportLocation + "/run?__report=statement.rptdesign&__format=pdf");
            stringBuilder.append("&accountNumber=");
            stringBuilder.append(account.getAccountNumber());

            String attachmentURL = stringBuilder.toString();
            String attachmentName = "Statement" + YYYYMM.format(now) + "_" + account.getAccountNumber() + ".pdf";
            String attachmentType = "application/pdf";

            EmailMessage message = new EmailMessage();
            message.setEmailText(emailText);
            message.setRecipients(emails);
            message.setSubject(subject);
            EmailAttachment emailAttachment = new EmailAttachment();
            emailAttachment.setSourceURL(attachmentURL);
            emailAttachment.setMimeType(attachmentType);
            emailAttachment.setFileName(attachmentName);

            List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
            attachments.add(emailAttachment);
            message.setAttachments(attachments);

            messagingClient.send(emailQueue, mapper.writeValueAsString(message));
        } catch (Exception e) {

            throw new ServiceException(e);
        }
    }

    public void smsStatement(Account account) {

        logger.info(String.format("Sending statement to Account %s", account.toString()));

        try {

            boolean sending = false;
            Iterator<PhoneNumber> iter = account.getAccountHolder().getPhoneNumbers().iterator();
            SMSMessage message = null;
            while (!sending && iter.hasNext()) {
                PhoneNumber number = iter.next();

                if (number.getNumberType().equals(NumberType.CELL)) {

                    String text;
                    try {
                        text = new MessageCreator(MessageCreatorType.STATEMENT_SMS, velocityEngine).createMessage(accountService.convert(account));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }

                    message = new SMSMessage();
                    message.setNumber(number.getNumber());
                    message.setCustomerId("ANM-" + account.getAccountNumber());
                    message.setMessage(text);

                    Date now = new Date();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setLenient(false);

                    calendar.setTime(now);

                    if (calendar.get(Calendar.HOUR_OF_DAY) > 8) {

                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                    }

                    calendar.set(Calendar.HOUR_OF_DAY, 8);
                    calendar.set(Calendar.MINUTE, 30);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);

                    while (!isWeekday(calendar)) {

                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                    }

                    message.setDateToSend(calendar.getTime());

                    sending = true;
                }
            }

            if (!sending) {

                logger.info(String.format("Account %s does not have a cell phone to SMS", account.toString()));
            } else {

                logger.info("Sending - " + message);
                messagingClient.send(smsQueue, mapper.writeValueAsString(message));
            }
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
    
    @Transactional
    public void emailStatement(String accountNumber) {
        
        Account account = accountRepository.findByAccountNumber(accountNumber);
        
        emailStatement(account);
    }

    @Transactional
    public void smsStatement(String accountNumber) {

        Account account = accountRepository.findByAccountNumber(accountNumber);

        smsStatement(account);
    }

    private boolean isWeekday(Calendar calendar) {

        int dow = calendar.get(Calendar.DAY_OF_WEEK);
        boolean isWeekday = ((dow >= Calendar.MONDAY) && (dow <= Calendar.FRIDAY));

        return isWeekday;
    }

}
