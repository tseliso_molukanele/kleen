package za.co.kleenbin.service.messagecreator;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageCreatorFactory {

	@Autowired
	private VelocityEngine engine;
	
	public MessageCreator get(MessageCreatorType messageType) {
		
		return new MessageCreator(messageType, engine);
	}

}
