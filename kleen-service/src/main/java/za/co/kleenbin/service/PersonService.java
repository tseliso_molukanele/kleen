package za.co.kleenbin.service;

import org.springframework.stereotype.Service;

import za.co.kleenbin.dto.Person;
import za.co.kleenbin.dto.PhoneNumber;
import za.co.kleenbin.dto.type.NumberType;

@Service
public class PersonService {

	public boolean isContactable(Person person) {
		
		for(PhoneNumber number : person.getPhoneNumbers()) {
			
			if(NumberType.CELL.equals(number.getNumberType())) {
				return true;
			}
		}
		
		if(person.getEmailAddresses().size() > 0) {
			
			return true;
		}
		
		
		return false;
	}

}
