package za.co.kleenbin.service;

import java.util.List;

import org.springframework.stereotype.Service;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.AccountTask;
import za.co.kleenbin.dto.Action;
import za.co.kleenbin.dto.Address;
import za.co.kleenbin.dto.Person;
import za.co.kleenbin.dto.Task;

@Service
public class TaskService {

    private static final String COMPLETE = "COMPLETE";
    private static final String SKIP = "SKIP";
    
    public Task getTask(List<String> currentRoles) {
                        
        AccountTask task = createDummyTask();
        
        return task ;
    }

    private AccountTask createDummyTask() {
        AccountTask task = new AccountTask();
        task.setId("next");
        task.setMessage("Add sticker to new bin for Mrs Mashall");
        
        Account account = new Account();
        
        account.setAccountNumber("100");
        Person accountHolder = new Person();
        accountHolder.setFirstName("firstName");
        accountHolder.setLastName("lastName");        
        account.setAccountHolder(accountHolder);
        Address address = new Address();
        address.setAddressLine1("addressLine1");
        address.setCode("code");
        address.setStreet("street");
        address.setSurburb("surburb");
        account.setAddress(address);
        
        task.setAccount(account);
        return task;
    }

    public Task action(Action action) {
        
        Task task = createDummyTask();
        
        if(COMPLETE.equalsIgnoreCase(action.getAction())) {
            
            task.setMessage(task.getMessage() + COMPLETE);            
        } else if(SKIP.equalsIgnoreCase(action.getAction())) {
            
            task.setMessage(task.getMessage() + SKIP);
        }
        
        return task;
    }

}
