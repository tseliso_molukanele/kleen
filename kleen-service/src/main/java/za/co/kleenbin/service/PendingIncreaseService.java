package za.co.kleenbin.service;

import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@Component
public class PendingIncreaseService extends PendingActionService {

	public static final String VALUE_PENDING_INCREASE = "PendingIncrease";

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AccountService accountService;

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
		super.setAccountService(accountService);
	}

	@Transactional
	public void reminderRun() {

		logger.info(String.format("Marking pending increase accounts..."));

		List<Account> accounts = accountService.getEligableForPriceIncrease();
		logger.info(String.format("%d accounts eligible for increse", accounts.size()));

		String actionKey = VALUE_PENDING_INCREASE;
		String action = "Increase";

		DateTime now = new DateTime();
		DateTime actionDate = now.plusMonths(1);

		reminderRun(accounts, actionKey, action, MessageCreatorType.PENDING_INCREASE_SMS, 
				MessageCreatorType.PENDING_INCREASE_MAIL,
				"Pending Account Increase for %s", actionDate);

		logger.info(String.format("...done marking pending increase accounts"));
	}

	@Transactional
	public void increaseRun() {

		logger.info(String.format("Increasing pending accounts..."));

		String actionKey = VALUE_PENDING_INCREASE;

		String emailSubject = "Account price plan increase: Account %s";
		String action = "increase";

		actionRun(actionKey, emailSubject, MessageCreatorType.INCREASE_MAIL, 
				MessageCreatorType.INCREASE_SMS, action);
		
		logger.info(String.format("...done increasing pending accounts"));
	}

	@Override
	protected boolean shouldDelete(Account account) {
		return false;
	}

	@Override
	protected void doAction(Account account) {

		accountService.upgradePricePlan(account.getAccountNumber());
	}
}
