package za.co.kleenbin.service;

import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.Payment;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@Component
public class PendingSuspensionService extends PendingActionService {

	public static final String VALUE_PENDING_SUSPENSION = "PendingSuspension";

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AccountService accountService;

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
		super.setAccountService(accountService);
	}

	@Transactional
	public void reminderRun() {

		logger.info(String.format("Marking pending suspension accounts..."));

		List<Account> accounts = accountService.getEligableForPendingSuspension();
		logger.info(String.format("%d accounts eligible for suspension", accounts.size()));

		String actionKey = VALUE_PENDING_SUSPENSION;
		String action = "Suspension";

		DateTime now = new DateTime();
		DateTime actionDate = now.plusDays(14);

		reminderRun(accounts, actionKey, action, MessageCreatorType.PENDING_SUSPENSION_SMS, 
				MessageCreatorType.PENDING_SUSPENSION_MAIL,
				"Pending Account Suspension for %s", actionDate);

		logger.info(String.format("...done marking pending suspension accounts"));
	}

	@Transactional
	public void suspensionRun() {

		logger.info(String.format("Suspending pending accounts..."));

		String actionKey = VALUE_PENDING_SUSPENSION;

		String emailSubject = "Your account has been suspended: %s";
		String action = "suspension";

		actionRun(actionKey, emailSubject, MessageCreatorType.SUSPENSION_MAIL, MessageCreatorType.SUSPENSION_SMS, action);

		logger.info(String.format("...done suspending pending accounts"));
	}

	@Override
	protected void doAction(Account account) {
		accountService.update(account.getAccountNumber(), false);
	}

	@Override
	protected boolean shouldDelete(Account account) {

		List<Payment> payments = accountService.retrievePaymentsByAccount(account);

		if (payments.size() > 0) {

			DateTime dateTime = new DateTime();
			DateTime sixtyDaysAgo = dateTime.minusDays(60);

			if (payments.get(0).getDate().after(sixtyDaysAgo.toDate())) {

				return true;
			}
		}
		
		return false;
	}
}
