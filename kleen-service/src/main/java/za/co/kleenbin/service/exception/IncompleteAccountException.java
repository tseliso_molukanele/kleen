package za.co.kleenbin.service.exception;

public class IncompleteAccountException extends RuntimeException {

	private static final long serialVersionUID = 5961159129187423491L;

	public IncompleteAccountException(String message) {
		super(message);
	}
}
