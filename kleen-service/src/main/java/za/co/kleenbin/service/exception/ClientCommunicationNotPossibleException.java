package za.co.kleenbin.service.exception;

public class ClientCommunicationNotPossibleException extends RuntimeException {
	
	private static final long serialVersionUID = 5942115502724723494L;

	public ClientCommunicationNotPossibleException(String string) {
		super(string);
	}

}
