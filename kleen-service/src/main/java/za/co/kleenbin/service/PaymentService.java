package za.co.kleenbin.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleen.dao.entity.type.PaymentType;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleen.dao.repository.InvoiceRepository;
import za.co.kleen.dao.repository.PaymentRepository;
import za.co.kleenbin.dto.Payment;
import za.co.kleenbin.service.exception.ServiceException;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private AccountRepository accountRepository;
    
    @Autowired
    private InvoiceRepository invoiceRepository;
    
    @Autowired
	private DozerBeanMapper dozerBeanMapper;

    @Transactional
    public Payment savePayment(Payment payment) {

        za.co.kleen.dao.entity.Account account = validatePayment(null, payment);

        za.co.kleen.dao.entity.Payment paymentModel = dozerBeanMapper.map(payment, za.co.kleen.dao.entity.Payment.class);
        paymentModel.setAccount(account);

        za.co.kleen.dao.entity.Payment saved = paymentRepository.save(paymentModel);
        return dozerBeanMapper.map(saved, Payment.class);
    }

    @Transactional
    public Payment updatePayment(Long id, Payment payment) {

        za.co.kleen.dao.entity.Account account = validatePayment(id, payment);

        za.co.kleen.dao.entity.Payment paymentModel = dozerBeanMapper.map(payment, za.co.kleen.dao.entity.Payment.class);
        paymentModel.setAccount(account);

        za.co.kleen.dao.entity.Payment existing = paymentRepository.findOne(id);

        existing.setAccount(paymentModel.getAccount());
        existing.setAmount(paymentModel.getAmount());
        existing.setDate(paymentModel.getDate());
        existing.setType(PaymentType.valueOf(payment.getType().toString()));
        existing.setReference(payment.getReference());

        za.co.kleen.dao.entity.Payment saved = paymentRepository.save(existing);
        return dozerBeanMapper.map(saved, Payment.class);
    }

    private za.co.kleen.dao.entity.Account validatePayment(Long id, Payment payment) {

        za.co.kleen.dao.entity.Account account = accountRepository.findByAccountNumber(payment.getAccount().getAccountNumber());

        if (account == null) {

            throw new ServiceException("No account found for that payment");
        }

        if (payment.getAmount() < 1) {

            throw new ServiceException("Cannot make payment less then R1");
        }

        if (payment.getDate().after(new Date())) {

            throw new ServiceException("Cannot make payment for the future");
        }

        Calendar from = Calendar.getInstance();
        from.setTime(payment.getDate());
        from.add(Calendar.DATE, -1);

        Calendar to = Calendar.getInstance();
        to.setTime(payment.getDate());
        to.add(Calendar.DATE, 1);

        List<za.co.kleen.dao.entity.Payment> payments = null;
        if(id != null) {

            payments = paymentRepository.findByAccount_amount_dateRange_excl_id(account, payment.getAmount(), from.getTime(), to.getTime(), id);
        } else {

            payments = paymentRepository.findByAccount_amount_dateRange(account, payment.getAmount(), from.getTime(), to.getTime());
        }

        if (payments.size() > 0) {

            throw new ServiceException("Duplicate Payment exists");
        }
        return account;
    }

    public Integer getPaymentsForMonth(DateTime date) {
        return paymentRepository.getPaymentsForMonth(date.getYear(), date.getMonthOfYear());
    }

    public Integer getCashPaymentsForMonth(DateTime date) {
        return paymentRepository.getCashPaymentsForMonth(date.getYear(), date.getMonthOfYear());
    }

    public Integer getNonCashPaymentsForMonth(DateTime date) {
        return paymentRepository.getNonCashPaymentsForMonth(date.getYear(), date.getMonthOfYear());
    }

    public Integer getPostInvoicePaymentsForMonth(DateTime date) {
    	
    	Date dateAt = invoiceRepository.getEarliestDateForMonth(date.getYear(), date.getMonthOfYear());
    	
        return paymentRepository.getPostPaymentsForMonth(date.getYear(), date.getMonthOfYear(), dateAt);
    }

    public Integer getPreInvoicePaymentsForMonth(DateTime date) {
    	
    	Date dateAt = invoiceRepository.getEarliestDateForMonth(date.getYear(), date.getMonthOfYear());
    	
        return paymentRepository.getPrePaymentsForMonth(date.getYear(), date.getMonthOfYear(), dateAt);
    }
    
    public Integer getPostDatePaymentsForMonth(DateTime date) {
    	
        return paymentRepository.getPostPaymentsForMonth(date.getYear(), date.getMonthOfYear(), date.toDate());
    }

    public Integer getPreDatePaymentsForMonth(DateTime date) {
    	
        return paymentRepository.getPrePaymentsForMonth(date.getYear(), date.getMonthOfYear(), date.toDate());
    }

    @Transactional
    public List<Payment> getPayments() {
        
        List<za.co.kleen.dao.entity.Payment> findAllOrderByDateDesc = paymentRepository.findTop50ByOrderByDateDesc();
        
        List<Payment> payments = new ArrayList<Payment>();

        for (za.co.kleen.dao.entity.Payment payment : findAllOrderByDateDesc) {

            payments.add(dozerBeanMapper.map(payment, Payment.class));
        }
        
        return payments ;
    }

    @Transactional
    public Payment getPayment(Long id) {

        za.co.kleen.dao.entity.Payment payment = paymentRepository.findOne(id);

        return dozerBeanMapper.map(payment, Payment.class);
    }
}
