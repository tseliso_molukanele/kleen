package za.co.kleenbin.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleenbin.dto.AccountAction;
import za.co.kleenbin.service.callaction.AccountActionProvider;

@Service
public class CallService {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    private int spotAvailable = 10;
    
    public void setSpotAvailable(int spotAvailable) {
        this.spotAvailable = spotAvailable;
    }
    
    @Autowired
    private List<AccountActionProvider> callProviders;
    
    public void setCallProviders(List<AccountActionProvider> callProviders) {
        this.callProviders = callProviders;
    }
    
    @Transactional
    public List<AccountAction> list() {
        
        int totalSpotsRequired = 0;
        
        for(AccountActionProvider provider : callProviders) {
            
            totalSpotsRequired += provider.getSpotsRequired();
        }
        
        List<AccountAction> callActions = new ArrayList<AccountAction>();        
        for(AccountActionProvider provider : callProviders) {
            
              int piece = provider.getSpotsRequired() * (spotAvailable / totalSpotsRequired);
              callActions.addAll(provider.getNextActions(piece));
        }        

        Collections.sort(callActions, new Comparator<AccountAction>() {

            @Override
            public int compare(AccountAction callAction1, AccountAction callAction2) {
                
                return callAction2.getCreatedDate().compareTo(callAction1.getCreatedDate());
            }
        });
        
        return callActions;
    }

}
