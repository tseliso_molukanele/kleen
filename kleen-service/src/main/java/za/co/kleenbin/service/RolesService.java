package za.co.kleenbin.service;

public interface RolesService {

    boolean isAllowedForRole(String role);
}
