package za.co.kleenbin.service;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.EmailAddress;
import za.co.kleenbin.dto.EmailAttachment;
import za.co.kleenbin.dto.EmailMessage;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@Service
public class InvoiceSender {

    private static final DateFormat YYYYMM = new SimpleDateFormat("yyyyMM");
    private static final DateFormat MMMMM_YYYY = new SimpleDateFormat("MMMMM yyyy");
    private static final DateFormat YYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VelocityEngine velocityEngine;
    @Autowired
    private Client messagingClient;
    @Value("${invoice.queue.sms}")
    private String smsQueue;
    @Value("${invoice.queue.email}")
    private String emailQueue;
    @Value("${reportLocation}")
    private String reportLocation;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private SMSSender smsSender;

    public void setSmsQueue(String smsQueue) {
        this.smsQueue = smsQueue;
    }
    public void setMessagingClient(Client messagingClient) {
        this.messagingClient = messagingClient;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }
    
    public void setSmsSender(SMSSender smsSender) {
		this.smsSender = smsSender;
	}

    public void emailInvoice(Account account) throws IOException {

        logger.info(String.format("Sending invoice to Account %s", account.toString()));

        DateTime now = new DateTime();

        String subject = "Invoice for " + MMMMM_YYYY.format(now.toDate()) + " for account " + account.getAccountNumber();

        List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();
        String[] emails = new String[emailAddresses.size()];

        for (int i = 0; i < emailAddresses.size(); i++) {

            emails[i] = emailAddresses.get(i).getEmail();
        }

        String emailText = new MessageCreator(MessageCreatorType.INVOICE_MAIL, velocityEngine).createMessage(account);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(reportLocation + "/run?__report=invoice.rptdesign&__format=pdf");
        stringBuilder.append("&accountNumber=");
        stringBuilder.append(account.getAccountNumber());
        stringBuilder.append("&date=");
        stringBuilder.append(YYYYMMDD.format(now.plusDays(1).toDate()));
        stringBuilder.append("&message=");
        stringBuilder.append(URLEncoder.encode(new MessageCreator(MessageCreatorType.INVOICE_MESSAGE, velocityEngine).createMessage(account), "UTF-8"));

        String attachmentURL = stringBuilder.toString();
        String attachmentName = "Invoice" + YYYYMM.format(now.toDate()) + "_" + account.getAccountNumber() + ".pdf";
        String attachmentType = "application/pdf";

        EmailMessage message = new EmailMessage();
        message.setEmailText(emailText);
        message.setRecipients(emails);
        message.setSubject(subject);
        EmailAttachment emailAttachment = new EmailAttachment();
        emailAttachment.setSourceURL(attachmentURL);
        emailAttachment.setMimeType(attachmentType);
        emailAttachment.setFileName(attachmentName);

        List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
        attachments.add(emailAttachment);
        message.setAttachments(attachments);

        messagingClient.send(emailQueue, mapper.writeValueAsString(message));
    }

    public void smsInvoice(Account account) throws JsonProcessingException {

        logger.info(String.format("Sending email invoice to Account %s", account.toString()));

        smsSender.sendSMS(account, MessageCreatorType.INVOICE_SMS);
    }
    
	
}
