package za.co.kleenbin.service.callaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.type.AccountActionType;
import za.co.kleen.dao.repository.AccountRepository;

@Component
public class HighBalanceProvider extends SimpleProvider {
    
    @Autowired
    private AccountRepository accountRepository;
    
    @Override
    protected AccountActionType getType() {
        return AccountActionType.HIGH_BALANCE;
    }

    @Override
    public int getSpotsRequired() {
        return 5;
    }

    @Override
    protected List<Account> backlog(int fetchSize) {
        return accountRepository.getActiveWithHighBalance(new PageRequest(0, fetchSize));
    }
}
