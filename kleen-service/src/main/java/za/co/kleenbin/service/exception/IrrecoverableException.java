package za.co.kleenbin.service.exception;

public class IrrecoverableException extends RuntimeException {

	private static final long serialVersionUID = 5273861967151939508L;

	public IrrecoverableException(Throwable cause) {
		super(cause);
	}
	
}
