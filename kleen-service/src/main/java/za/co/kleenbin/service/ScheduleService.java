package za.co.kleenbin.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleen.dao.repository.ServiceRepository;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.AggregatedService;
import za.co.kleenbin.dto.Schedule;
import za.co.kleenbin.dto.Service;
import za.co.kleenbin.dto.type.ServiceRecord;
import za.co.kleenbin.service.exception.ServiceException;

@org.springframework.stereotype.Service
public class ScheduleService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private ScheduleSender scheduleSender;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Transactional
	public Schedule getSchedule(Integer day) {

		List<za.co.kleen.dao.entity.Account> foundAccounts = accountRepository
				.findActiveByDay(day);

		List<AggregatedService> aggregatedServices = new ArrayList<AggregatedService>();
		int totalBins = 0;
		int totalServicedThisWeek = 0;
		int totalReqService = 0;
		int totalClients = foundAccounts.size();

		for (za.co.kleen.dao.entity.Account foundAccount : foundAccounts) {

		        totalBins += foundAccount.getPlan().getNumberOfBins();
			List<za.co.kleen.dao.entity.Service> foundServices = foundAccount
					.getServices();

			Account account = dozerBeanMapper.map(foundAccount, Account.class);

			Calendar first = Calendar.getInstance();
			first.set(Calendar.HOUR, 0);
			first.set(Calendar.MINUTE, 0);
			first.set(Calendar.SECOND, 1);

			first.add(Calendar.DAY_OF_WEEK,
					first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK));

			Calendar last = (Calendar) first.clone();
			last.add(Calendar.DAY_OF_YEAR, 6);
			last.set(Calendar.HOUR, 23);
			last.set(Calendar.MINUTE, 59);
			last.set(Calendar.SECOND, 59);

			List<Service> services = new ArrayList<Service>();

			for (int i = 5; i >= 0; i--) {

				boolean found = false;
				for (za.co.kleen.dao.entity.Service foundService : foundServices) {

					if (foundService.getDate().after(first.getTime())
							&& foundService.getDate().before(last.getTime())) {

						Service service = new Service();
						service.setDate(foundService.getDate());
						service.setWeekEnd(last.getTime());
						service.setWeekStart(first.getTime());
						service.setServiced(true);
						service.setServiceRecord( ServiceRecord.valueOf(foundService.getServiceRecord().toString()) );

						if(i == 5) {
						    totalServicedThisWeek++;
						}
						services.add(service);

						found = true;
					}
				}

				if (!found) {

					Service service = new Service();
					service.setWeekEnd(last.getTime());
					service.setWeekStart(first.getTime());
					service.setServiced(false);
					service.setServiceRecord( ServiceRecord.NONE );

					services.add(service);
				}

				first.add(Calendar.DAY_OF_YEAR, -7);
				last.add(Calendar.DAY_OF_YEAR, -7);
			}

			AggregatedService aggr = new AggregatedService(account, services);
			if(aggr.getWashDue()) {
			    totalReqService ++;
			}
                        aggregatedServices.add(aggr);
		}

		Schedule schedule = new Schedule();
		schedule.setServices(aggregatedServices);
		schedule.setTotalBins(totalBins);
		schedule.setTotalClients(totalClients);
		schedule.setTotalServicedThisWeek(totalServicedThisWeek);
		schedule.setTotalRequiringServiceThisWeek(totalReqService);
		
		return schedule;
	}
	
	@Transactional
	public Service processService(Service service) {

		if(service.getServiceRecord() == null) {

			throw new ServiceException("Service record needs to be set to capture a service");
		}

		za.co.kleen.dao.entity.Account account = accountRepository
				.findByAccountNumber(service.getAccount().getAccountNumber());

		if (account == null) {

			throw new ServiceException("No account found");
		}

		Date now = new Date();
		
		Calendar first = Calendar.getInstance();
		first.set(Calendar.HOUR, 0);
		first.set(Calendar.MINUTE, 0);
		first.set(Calendar.SECOND, 1);
		
		first.add(Calendar.DAY_OF_WEEK,
				first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK));
		
		Calendar last = (Calendar) first.clone();
		last.add(Calendar.DAY_OF_YEAR, 6);
		last.set(Calendar.HOUR, 23);
		last.set(Calendar.MINUTE, 59);
		last.set(Calendar.SECOND, 59);
		
		List<za.co.kleen.dao.entity.Service> services = account.getServices();
		
		if(services.size() > 0) {
			
			if(services.get(0).getDate().after(first.getTime()) && services.get(0).getDate().before(last.getTime())) {
			
				throw new ServiceException("Address has already been serviced this week");
			}
		}
		
		za.co.kleen.dao.entity.Service serviceModel = dozerBeanMapper.map(service,
				za.co.kleen.dao.entity.Service.class);
		serviceModel.setAccount(account);

		serviceModel.setDate(now);

		if(za.co.kleen.dao.entity.type.ServiceRecord.BIN_UNREACHABLE.equals(serviceModel.getServiceRecord())) {

			scheduleSender.communicateMissedService(account);
		}

		za.co.kleen.dao.entity.Service saved = serviceRepository
				.save(serviceModel);

		return dozerBeanMapper.map(saved, Service.class);

	}

}
