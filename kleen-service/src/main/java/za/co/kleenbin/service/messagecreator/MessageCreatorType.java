package za.co.kleenbin.service.messagecreator;

public enum MessageCreatorType {

	MISSED_SERVICE_SMS("vm/sms/missedService.vm"), 
	INVOICE_SMS("vm/sms/invoice.vm"), 
	PENDING_INCREASE_SMS("vm/sms/pendingIncrease.vm", "plan", "futurePlan"),
	STATEMENT_SMS("vm/sms/statement.vm"),                     
	PENDING_SUSPENSION_SMS("vm/sms/pendingSuspension.vm"),    
	SUSPENSION_SMS("vm/sms/suspension.vm"),                   
	INCREASE_SMS("vm/sms/increase.vm"),                       
	NEW_ACCOUNT_SMS("vm/sms/newAccount.vm", "plan"), 
	                                                           
	INVOICE_MAIL("vm/mail/invoice.vm"),                       
	MISSED_SERVICE_MAIL("vm/mail/missedService.vm"),          
	STATEMENT_MAIL("vm/mail/statement.vm"),                   
	PENDING_SUSPENSION_MAIL("vm/mail/pendingSuspension.vm"),  
	SUSPENSION_MAIL("vm/mail/suspension.vm"),                 
	PENDING_INCREASE_MAIL("vm/mail/pendingIncrease.vm", "plan", "futurePlan"),      
	INCREASE_MAIL("vm/mail/increase.vm"),                     
	NEW_ACCOUNT_EMAIL("vm/mail/newAccount.vm", "plan"),
	                                                           
	INVOICE_MESSAGE("vm/template/invoiceMessage.vm");
	
	private String template;
	private String[] requiredObjects;

	MessageCreatorType(String template, String ... requiredObjects) {
		
		this.template = template;
		this.requiredObjects = requiredObjects;
	}
	
	public String getTemplate() {
		return template;
	}

	public String[] getRequiredObjects() {
		return requiredObjects;
	}
}
