package za.co.kleenbin.service;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleen.dao.repository.PlanRepository;
import za.co.kleenbin.dto.Plan;

@Service
public class PlanService {

	@Autowired
	private PlanRepository planRepository;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	public List<Plan> getPlans() {
		
		List<Plan> plans = new ArrayList<>();
		List<za.co.kleen.dao.entity.Plan> findAll = planRepository.findAll();
		
		for (za.co.kleen.dao.entity.Plan plan : findAll) {
			
			plans.add(dozerBeanMapper.map(plan, Plan.class));
		}
		
		return plans;
	}

	@Transactional
	public Plan newPlan(Plan plan) {
		
		return convert(planRepository.save(convert(plan)));
	}
	
	@Transactional
	public Plan updatePlan(Long id, Plan plan) {
				
		za.co.kleen.dao.entity.Plan found = planRepository.findOne(id);
		
		if(found == null) {
			return null;
		}
		
		za.co.kleen.dao.entity.Plan converted = convert(plan);
		
		found.setFrequency(converted.getFrequency());
		found.setNumberOfBins(converted.getNumberOfBins());
		found.setPrice(converted.getPrice());
		
		return convert(planRepository.save(found));
	}

	private Plan convert(za.co.kleen.dao.entity.Plan input) {
		
		return dozerBeanMapper.map(input, Plan.class);
	}

	private za.co.kleen.dao.entity.Plan convert(Plan input) {
		
		return dozerBeanMapper.map(input, za.co.kleen.dao.entity.Plan.class);
	}

}
