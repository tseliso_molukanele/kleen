package za.co.kleenbin.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleen.dao.aggregate.InvoiceCount;
import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.EmailAddress;
import za.co.kleen.dao.entity.Invoice;
import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleen.dao.repository.InvoiceRepository;
import za.co.kleenbin.dto.InvoiceData;
import za.co.kleenbin.service.exception.ServiceException;

@Service
public class InvoiceService {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${royaltyPerc}")
	private int royaltyPerc;
	@Autowired
	private InvoiceRepository invoiceRepository;
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private InvoiceSender invoiceSender;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
	
	public void setInvoiceRepository(InvoiceRepository invoiceRepository) {
		this.invoiceRepository = invoiceRepository;
	}

	public void setAccountRepository(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public void setInvoiceSender(InvoiceSender invoiceSender) {
		this.invoiceSender = invoiceSender;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	@Transactional
	public void invoiceRun() {
		invoiceRun(true, new DateTime());
	}

	@Transactional
	public void invoiceRun(boolean sendMessage) {
		invoiceRun(sendMessage, new DateTime());
	}

	@Transactional
	public void invoiceRun(DateTime dateTime) {
		invoiceRun(false, dateTime);
	}

	@Transactional
	private void invoiceRun(boolean sendMessage, DateTime now) {

		List<Account> accounts = accountRepository.findActive();

		String errors = "";

		for (Account account : accounts) {

			try {

				String number = now.getYear() + "" + now.getMonthOfYear() + "" + account.getAccountNumber();
				Invoice invoice = invoiceRepository.findByAccountAndNumber(account, number);

				if (invoice != null) {

					logger.info("Skipping invoice for AccountNumber: {}, with invoiceNumber {}",
							account.getAccountNumber(), number);
					continue;
				}

				Plan plan = account.getPlan();

				invoice = new Invoice();
				invoice.setAccount(account);
				invoice.setDate(now.toDate());
				invoice.setNumber(number);
				invoice.setInvoiceAmount((float) plan.getPrice());
				invoice.setNumberOfBins(account.getPlan().getNumberOfBins());

				// Change to get actuals
				invoice.setServiceCount(account.getPlan().getFrequency().asCount());
				invoice.setAmount(plan.getPrice() * plan.getFrequency().asCount() * plan.getNumberOfBins());

				logger.info("Creating invoice for AccountNumber: {}, with invoiceNumber {}", account.getAccountNumber(),
						number);
				invoiceRepository.save(invoice);

				if (sendMessage) {

					List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();

					if (emailAddresses.size() > 0) {

						invoiceSender.emailInvoice(accountService.convert(account));
					} else {

						invoiceSender.smsInvoice(accountService.convert(account));
					}
				}

			} catch (Exception e) {
				String message = String.format("Error invoicing account %s --> %s ", account.toString(),
						e.getMessage());
				errors += ", " + message;
				logger.error(message);
			}
		}

		if (!errors.equalsIgnoreCase("")) {
			notificationService.sendEmail(errors);
		}
	}

	@Transactional
	public void invoiceSend() {

		List<Account> accounts = accountRepository.findActive();

		String errors = "";

		for (Account account : accounts) {

			try {

				List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();

				if (emailAddresses.size() > 0) {

					invoiceSender.emailInvoice(accountService.convert(account));
				} else {

					invoiceSender.smsInvoice(accountService.convert(account));
				}

			} catch (Exception e) {
				String message = String.format("Error sendoing invoice for account %s --> %s ", account.toString(),
						e.getMessage());
				errors += ", " + message;
				logger.error(message);
			}
		}

		if (!errors.equalsIgnoreCase("")) {
			notificationService.sendEmail(errors);
		}
	}

	public Integer getInvoiceForMonth(DateTime monthIter) {
		return invoiceRepository.getInvoiceForMonth(monthIter.getYear(), monthIter.getMonthOfYear());
	}

	@Transactional
	public za.co.kleenbin.dto.Invoice updateInvoice(Long id, za.co.kleenbin.dto.Invoice invoice) {

		if (invoice.getServiceCount() < 0) {
			throw new ServiceException("Service count must be 0 or more");
		}

		za.co.kleen.dao.entity.Invoice existing = invoiceRepository.findOne(id);

		existing.setServiceCount(invoice.getServiceCount());
		existing.setNumberOfBins(invoice.getNumberOfBins());
		existing.setAmount((int) (existing.getInvoiceAmount() * invoice.getServiceCount() * invoice.getNumberOfBins()));

		Invoice saved = invoiceRepository.save(existing);

		return dozerBeanMapper.map(saved, za.co.kleenbin.dto.Invoice.class);
	}

	@Transactional
	public za.co.kleenbin.dto.Invoice getInvoice(Long id) {

		za.co.kleen.dao.entity.Invoice payment = invoiceRepository.findOne(id);

		return dozerBeanMapper.map(payment, za.co.kleenbin.dto.Invoice.class);
	}

	public InvoiceData getData(String yearmonth) {
		
		DateTime date;
		try {
			date = new DateTime(new SimpleDateFormat("yyyyMM").parse(yearmonth));
		} catch (ParseException e) {
			throw new ServiceException("Date supplied " + yearmonth + " is not of the correct format yyyyMM");
		}
		
		InvoiceCount counts = invoiceRepository.findTotalsByYearMonth(date.getYear(), date.getMonthOfYear());

		InvoiceData invoiceData = new InvoiceData();
		invoiceData.setBinCount(counts.getTotalBins());
		invoiceData.setIncome(counts.getTotalAmount());
		
		invoiceData.setRoyalty(((double)counts.getTotalAmount() * royaltyPerc) / 100);
		
		return invoiceData;
	}
}
