package za.co.kleenbin.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hi3project.vineyard.comm.stomp.gozirraws.Client;

import za.co.kleen.dao.entity.Proprietor;
import za.co.kleen.dao.repository.ProprietorRepository;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.EmailAddress;
import za.co.kleenbin.dto.EmailMessage;
import za.co.kleenbin.dto.type.AccountStatus;
import za.co.kleenbin.service.exception.IrrecoverableException;
import za.co.kleenbin.service.messagecreator.MessageCreator;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

public abstract class PendingActionService {

	protected static final String DD_MMM_YYYY = "dd MMM yyyy";
	protected static final SimpleDateFormat DATE_FORMAT_DD_MMM_YYYY = new SimpleDateFormat(DD_MMM_YYYY);

	/*
	 * Keep name as "PROPRIOTER_SUSPENSION_ACTIVATION_DATE" so it matches with
	 * historical data
	 */
	public static final String PROPRIOTER_ACTION_ACTIVATION_DATE = "PROPRIOTER_SUSPENSION_ACTIVATION_DATE";
	public static final String PROPRIOTER_ACCOUNT_NUMBER = "PROPRIOTER_ACCOUNT_NUMBER";
	public static final String PROPRIOTER_STATUS = "PROPRIOTER_STATUS";
	public static final String PROPRIOTER_PRIMARY_TYPE = "PROPRIOTER_PRIMARY_TYPE";
	public static final String VALUE_ACTIVE = "Active";
	public static final String VALUE_COMPLETED = "Completed";

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${invoice.queue.email}")
	private String emailQueue;

	public void setEmailQueue(String emailQueue) {
		this.emailQueue = emailQueue;
	}

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
    private VelocityEngine velocityEngine;

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	@Autowired
	private Client messagingClient;

	public void setMessagingClient(Client messagingClient) {
		this.messagingClient = messagingClient;
	}

	@Autowired
	private AccountService accountService;

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	private void sendEmail(Account account, MessageCreatorType template, String subjectIn) throws IOException {

		logger.info(String.format("Sending email to Account %s", account.toString()));

		String subject = String.format(subjectIn, account.getAccountNumber());

		List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();
		String[] emails = new String[emailAddresses.size()];

		for (int i = 0; i < emailAddresses.size(); i++) {

			emails[i] = emailAddresses.get(i).getEmail();
		}

		String emailText = new MessageCreator(template, velocityEngine).createMessage(account);

		EmailMessage message = new EmailMessage();
		message.setEmailText(emailText);
		message.setRecipients(emails);
		message.setSubject(subject);

		messagingClient.send(emailQueue, mapper.writeValueAsString(message));
	}

	@Autowired
	private ProprietorRepository proprietorRepository;

	public void setProprietorRepository(ProprietorRepository proprietorRepository) {
		this.proprietorRepository = proprietorRepository;
	}

	@Autowired
	protected SMSSender smsSender;

	@Autowired
	private NotificationService notificationService;

	public void setSmsSender(SMSSender smsSender) {
		this.smsSender = smsSender;
	}

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	protected void reminderRun(List<Account> accounts, String actionKey, String action, 
			MessageCreatorType actionSmsmessageType, MessageCreatorType actionEmailMessageType, 
			String emailSubject, DateTime actionDate) {

		int count = 0;

		accountLoop: for (Account account : accounts) {

			List<Proprietor> byKeyValue = proprietorRepository.findByKeyValue(PROPRIOTER_ACCOUNT_NUMBER,
					account.getAccountNumber());

			for (Proprietor proprietor : byKeyValue) {

				if (actionKey.equals(proprietor.getProperties().get(PROPRIOTER_PRIMARY_TYPE))) {

					continue accountLoop;
				}
			}

			Proprietor proprietor = new Proprietor();

			proprietor.getProperties().put(PROPRIOTER_PRIMARY_TYPE, actionKey);
			proprietor.getProperties().put(PROPRIOTER_STATUS, VALUE_ACTIVE);
			proprietor.getProperties().put(PROPRIOTER_ACCOUNT_NUMBER, account.getAccountNumber());
			proprietor.getProperties().put(PROPRIOTER_ACTION_ACTIVATION_DATE, actionDate.toString(DD_MMM_YYYY));// new
																												// DateTimeFormatterFactory("dd
			List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();

			try {
				if (emailAddresses.size() > 0) {

					sendEmail(account, actionEmailMessageType, emailSubject);
				} else {

					smsSender.sendSMS(account, actionSmsmessageType);
				}
			} catch (IOException e) {
				String message = String.format("Error sending pending " + action + " to account %s --> %s ",
						account.toString(), e.getMessage());
				logger.error(message);
				notificationService.sendEmail(message);
				throw new RuntimeException(e);
			}

			proprietorRepository.save(proprietor);
			count++;
		}

		logger.info(String.format("%d accounts processed", count));

		if (count > 0) {

			notificationService.sendEmail("Accounts Marked For " + action + " Today",
					String.format("%d accounts processed", count));
		}
	}

	protected void actionRun(String actionKey, String emailSubject, MessageCreatorType emailTemplate, MessageCreatorType smsTemplate,
			String action) {

		try {
			Date now = new Date();

			List<Proprietor> propriotors = proprietorRepository.findByKeyValue(PROPRIOTER_STATUS, VALUE_ACTIVE);

			logger.info(String.format("Accounts potentially ready for " + action + " %d", propriotors.size()));

			int count = 0;
			StringBuilder actioned = new StringBuilder();
			int countDeleted = 0;
			StringBuilder deleted = new StringBuilder();
			for (Proprietor proprietor : propriotors) {

				String accountNumber = proprietor.getProperties().get(PROPRIOTER_ACCOUNT_NUMBER);

				if (actionKey.equals(proprietor.getProperties().get(PROPRIOTER_PRIMARY_TYPE))) {

					if (DATE_FORMAT_DD_MMM_YYYY.parse(proprietor.getProperties().get(PROPRIOTER_ACTION_ACTIVATION_DATE))
							.before(now)) {

						Account account = accountService.getAccount(accountNumber);

						if (AccountStatus.ACTIVE.equals(account.getStatus())) {

							if (shouldDelete(account)) {

								countDeleted++;
								deleted.append(account.toString()).append(", ");
								proprietorRepository.delete(proprietor);
								continue;
							}

							doAction(account);

							proprietor.getProperties().put(PROPRIOTER_STATUS, VALUE_COMPLETED);
							proprietorRepository.save(proprietor);

							List<EmailAddress> emailAddresses = account.getAccountHolder().getEmailAddresses();

							if (emailAddresses.size() > 0) {

								sendEmail(account, emailTemplate, emailSubject);
							} else {

								smsSender.sendSMS(account, smsTemplate);
							}

							count++;
							actioned.append(account.toString()).append(", ");
						}
					}
				}
			}

			StringBuilder message = new StringBuilder(String
					.format("Accounts " + action + " %d, accounts '" + action + "' deleted %d", count, countDeleted))
							.append(" ============ ").append(action + ": ").append(actioned.toString())
							.append("Deleted: ").append(deleted.toString());

			logger.info(message.toString());

			if (count > 0) {

				notificationService.sendEmail("Accounts " + action + " Today", message.toString());
			}
		} catch (ParseException | IOException e) {

			throw new IrrecoverableException(e);
		}
	}

	protected abstract boolean shouldDelete(Account account);

	protected abstract void doAction(Account account);

}
