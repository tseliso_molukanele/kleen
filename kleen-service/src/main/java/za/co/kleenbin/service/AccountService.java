package za.co.kleenbin.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import za.co.kleen.dao.entity.type.AccountStatus;
import za.co.kleen.dao.entity.type.FREQUENCY;
import za.co.kleen.dao.repository.AccountRepository;
import za.co.kleen.dao.repository.AddressRepository;
import za.co.kleen.dao.repository.PersonRepository;
import za.co.kleen.dao.repository.PlanRepository;
import za.co.kleenbin.dto.Account;
import za.co.kleenbin.dto.Address;
import za.co.kleenbin.dto.EmailAddress;
import za.co.kleenbin.dto.Invoice;
import za.co.kleenbin.dto.Payment;
import za.co.kleenbin.dto.Person;
import za.co.kleenbin.dto.PhoneNumber;
import za.co.kleenbin.dto.Service;
import za.co.kleenbin.service.exception.NoFuturePlanException;
import za.co.kleenbin.service.exception.ServiceException;
import za.co.kleenbin.service.messagecreator.MessageCreatorType;

@org.springframework.stereotype.Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	public void setAccountRepository(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private PlanRepository planRepository;

	@Transactional
	public List<Account> search(String searchKey) {

		List<za.co.kleen.dao.entity.Account> accountModels = accountRepository.search(searchKey);

		return convert(accountModels);
	}

	@Transactional
	public List<Account> list() {

		List<za.co.kleen.dao.entity.Account> accountModels = accountRepository.findAll();

		return convert(accountModels);
	}

	public Account convert(za.co.kleen.dao.entity.Account account) {

		return dozerBeanMapper.map(account, Account.class);
	}

	private List<Account> convert(List<za.co.kleen.dao.entity.Account> accountModels) {
		ArrayList<Account> accounts = new ArrayList<Account>();
		for (za.co.kleen.dao.entity.Account accountModel : accountModels) {

			accounts.add(dozerBeanMapper.map(accountModel, Account.class));
		}

		return accounts;
	}

	@Transactional
	public List<Payment> retrievePaymentsByAccount(Account account) {

		za.co.kleen.dao.entity.Account foundAccount = accountRepository.findByAccountNumber(account.getAccountNumber());

		if (foundAccount == null) {
			throw new ServiceException("Not account found for account number" + account.getAccountNumber());
		}

		List<Payment> payments = new ArrayList<Payment>();

		for (za.co.kleen.dao.entity.Payment payment : foundAccount.getPayments()) {

			payments.add(dozerBeanMapper.map(payment, Payment.class));
		}

		return payments;
	}

	@Transactional
	public List<Invoice> retrieveInvoicesByAccount(Account account) {

		za.co.kleen.dao.entity.Account foundAccount = accountRepository.findByAccountNumber(account.getAccountNumber());

		if (foundAccount == null) {
			throw new ServiceException("Not account found for account number" + account.getAccountNumber());
		}

		List<Invoice> invoices = new ArrayList<Invoice>();

		for (za.co.kleen.dao.entity.Invoice invoice : foundAccount.getInvoices()) {

			invoices.add(dozerBeanMapper.map(invoice, Invoice.class));
		}

		return invoices;
	}

	@Transactional
	public List<Service> retrieveServicesByAccount(Account account) {

		za.co.kleen.dao.entity.Account foundAccount = accountRepository.findByAccountNumber(account.getAccountNumber());

		if (foundAccount == null) {
			throw new ServiceException("Not account found for account number" + account.getAccountNumber());
		}

		List<Service> services = new ArrayList<Service>();

		for (za.co.kleen.dao.entity.Service service : foundAccount.getServices()) {

			services.add(dozerBeanMapper.map(service, Service.class));
		}

		return services;
	}

	@Transactional
	public Account newAccount(Account account) {
		
		account = save(account);
		
		try {
			notificationService.sendMessage(account, MessageCreatorType.NEW_ACCOUNT_SMS, MessageCreatorType.NEW_ACCOUNT_EMAIL);
		} catch (IOException e) {
			
			throw new RuntimeException(e);
		}
		
		return account;
	}
	
	@Transactional
	public Account existingAccount(Account account) {
		
		return save(account);
	}
	
	private Account save(Account account) {

		if (account.getAccountNumber() != null) {
			return saveExisting(account);
		}

		return saveNew(account);
	}

	private Account saveExisting(Account account) {

		za.co.kleen.dao.entity.Account foundAccount = accountRepository.findByAccountNumber(account.getAccountNumber());

		if (!foundAccount.getPlan().getNumberOfBins().equals(account.getPlan().getNumberOfBins())
				|| !foundAccount.getPlan().getFrequency().name().equals(account.getPlan().getFrequency().name())
				|| !foundAccount.getPlan().getYear().equals(account.getPlan().getYear())) {

			za.co.kleen.dao.entity.Plan plan = planRepository.findByFrequencyAndNumberOfBinsAndYear(
					FREQUENCY.valueOf(account.getPlan().getFrequency().name()), account.getPlan().getNumberOfBins(),
					account.getPlan().getYear());

			foundAccount.setPlan(plan);
		}

		foundAccount.setDayOfCleaning(account.getDayOfCleaning());

		accountRepository.save(foundAccount);

		za.co.kleen.dao.entity.Person accountHolder = foundAccount.getAccountHolder();
		accountHolder.setFirstName(account.getAccountHolder().getFirstName());
		accountHolder.setLastName(account.getAccountHolder().getLastName());
		accountHolder.setInitials(account.getAccountHolder().getInitials());
		accountHolder.setTitle(account.getAccountHolder().getTitle());

		editList(account.getAccountHolder().getEmailAddresses(), accountHolder.getEmailAddresses(), emailComparator,
				za.co.kleen.dao.entity.EmailAddress.class);
		editList(account.getAccountHolder().getPhoneNumbers(), accountHolder.getPhoneNumbers(), phoneNumberComparator,
				za.co.kleen.dao.entity.PhoneNumber.class);

		personRepository.save(accountHolder);

		za.co.kleen.dao.entity.Address address = foundAccount.getAddress();
		address.setAddressLine1(account.getAddress().getAddressLine1());
		address.setStreet(account.getAddress().getStreet());
		address.setSurburb(account.getAddress().getSurburb());
		address.setCode(account.getAddress().getCode());

		addressRepository.save(address);

		foundAccount = accountRepository.findByAccountNumber(account.getAccountNumber());

		return dozerBeanMapper.map(foundAccount, Account.class);
	}

	DisparateComparator<za.co.kleen.dao.entity.EmailAddress, EmailAddress> emailComparator = new DisparateComparator<za.co.kleen.dao.entity.EmailAddress, EmailAddress>() {

		@Override
		public boolean equals(za.co.kleen.dao.entity.EmailAddress object1, EmailAddress object2) {

			return (object1.getEmail().equals(object2.getEmail()));
		}

		@Override
		public boolean equalsType1(za.co.kleen.dao.entity.EmailAddress object1,
				za.co.kleen.dao.entity.EmailAddress object2) {

			return object1.getEmail().equals(object2.getEmail());
		}
	};
	DisparateComparator<za.co.kleen.dao.entity.PhoneNumber, PhoneNumber> phoneNumberComparator = new DisparateComparator<za.co.kleen.dao.entity.PhoneNumber, PhoneNumber>() {

		@Override
		public boolean equals(za.co.kleen.dao.entity.PhoneNumber object1, PhoneNumber object2) {

			return object1.getNumber().equals(object2.getNumber())
					&& object1.getNumberType().name().equals(object2.getNumberType().name());
		}

		@Override
		public boolean equalsType1(za.co.kleen.dao.entity.PhoneNumber object1,
				za.co.kleen.dao.entity.PhoneNumber object2) {

			return object1.getNumber().equals(object2.getNumber())
					&& object1.getNumberType().name().equals(object2.getNumberType().name());
		}

	};

	private interface DisparateComparator<TYPE1, TYPE2> {

		public boolean equals(TYPE1 object1, TYPE2 object2);

		public boolean equalsType1(TYPE1 object1, TYPE1 object2);
	}

	private <DTOT, DAOT> void editList(List<DTOT> emailAddresses, List<DAOT> emailAddresses2,
			DisparateComparator<DAOT, DTOT> comparator, Class<DAOT> klass) {

		List<DAOT> toBeAdded = new ArrayList<DAOT>();
		List<DAOT> toBeRemoved = new ArrayList<DAOT>();

		// mark to be added
		for (DTOT newEmail : emailAddresses) {

			boolean foundNewMail = false;
			for (DAOT existingEmail : emailAddresses2) {

				if (comparator.equals(existingEmail, newEmail)) {

					foundNewMail = true;
				}
			}

			if (!foundNewMail) {
				toBeAdded.add((DAOT) dozerBeanMapper.map(newEmail, klass));
			}
		}

		// mark to be removed
		for (DAOT existingEmail : emailAddresses2) {

			boolean foundOldMail = false;
			for (DTOT oldEmail : emailAddresses) {

				if (comparator.equals(existingEmail, oldEmail)) {

					foundOldMail = true;
				}
			}

			if (!foundOldMail) {
				toBeRemoved.add(existingEmail);
			}
		}

		// apply removals
		for (DAOT emailAddress : toBeRemoved) {

			Iterator<DAOT> iter = emailAddresses2.iterator();

			while (iter.hasNext()) {

				DAOT addressToBeRemoved = iter.next();

				if (comparator.equalsType1(emailAddress, addressToBeRemoved)) {
					iter.remove();
				}
			}
		}

		// apply additions
		for (DAOT emailAddress : toBeAdded) {

			emailAddresses2.add(emailAddress);
		}
	}

	private Account saveNew(Account account) {
		account.setStatus(za.co.kleenbin.dto.type.AccountStatus.ACTIVE);

		account.setAccountNumber((accountRepository.getMaxAccountNumber() + 1) + "");

		Person accountHolder = account.getAccountHolder();

		za.co.kleen.dao.entity.Person personEntity = dozerBeanMapper.map(accountHolder,
				za.co.kleen.dao.entity.Person.class);

		personRepository.save(personEntity);

		Address address = account.getAddress();

		za.co.kleen.dao.entity.Address addressEntity = dozerBeanMapper.map(address,
				za.co.kleen.dao.entity.Address.class);

		addressRepository.save(addressEntity);

		za.co.kleen.dao.entity.Account accountEntity = dozerBeanMapper.map(account,
				za.co.kleen.dao.entity.Account.class);

		accountEntity.setAccountHolder(personEntity);
		accountEntity.setAddress(addressEntity);

		za.co.kleen.dao.entity.Plan plan = planRepository.findByFrequencyAndNumberOfBinsAndYear(
				FREQUENCY.valueOf(account.getPlan().getFrequency().name()), 
				account.getPlan().getNumberOfBins(),
				account.getPlan().getYear());

		accountEntity.setPlan(plan);

		za.co.kleen.dao.entity.Account saved = accountRepository.save(accountEntity);
		return dozerBeanMapper.map(saved, Account.class);
	}

	@Transactional
	public Account getAccount(String accountNumber) {

		za.co.kleen.dao.entity.Account found = accountRepository.findByAccountNumber(accountNumber);
		return found != null ? dozerBeanMapper.map(found, Account.class) : null;
	}

	@Transactional
	public Account update(String accountNumber, Boolean up) {

		za.co.kleen.dao.entity.Account found = accountRepository.findByAccountNumber(accountNumber);

		if (null == found.getStatus()) {
			notificationService.sendEmail("Error: Account Data", "Account: " + found.toString() + " has no status");
		}

		String emailSubject = "Account Update";

		if (up) {
			if (AccountStatus.DISABLED.equals(found.getStatus())) {

				found.setStatus(AccountStatus.INACTIVE);
				accountRepository.save(found);
				notificationService.sendEmail(emailSubject,
						"Account: " + found.toString() + " has been uped from 'DISABLED' to 'INACTIVE'");
			} else if (AccountStatus.INACTIVE.equals(found.getStatus())) {

				found.setStatus(AccountStatus.ACTIVE);
				accountRepository.save(found);
				notificationService.sendEmail(emailSubject,
						"Account: " + found.toString() + " has been uped from 'INACTIVE' to 'ACTIVE'");
			}
		} else {
			if (AccountStatus.INACTIVE.equals(found.getStatus())) {

				found.setStatus(AccountStatus.DISABLED);
				accountRepository.save(found);
				notificationService.sendEmail(emailSubject,
						"Account: " + found.toString() + " has been downed from 'INACTIVE' to 'DISABLED'");
			} else if (AccountStatus.ACTIVE.equals(found.getStatus())) {

				found.setStatus(AccountStatus.INACTIVE);
				accountRepository.save(found);
				notificationService.sendEmail(emailSubject,
						"Account: " + found.toString() + " has been downed from 'ACTIVE' to 'INACTIVE'");
			}
		}

		return found != null ? dozerBeanMapper.map(found, Account.class) : null;
	}

	@Autowired
	private NotificationService notificationService;

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public List<Account> getEligableForPendingSuspension() {

		List<za.co.kleen.dao.entity.Account> accounts = accountRepository.getLatePayers(60);

		List<Account> returnables = new ArrayList<>();

		for (za.co.kleen.dao.entity.Account account : accounts) {

			List<za.co.kleen.dao.entity.Invoice> invoices = account.getInvoices();

			if (invoices.size() > 1) {

				float lastTwoMonthsInvoice = 0;
				lastTwoMonthsInvoice += invoices.get(0).getAmount();
				lastTwoMonthsInvoice += invoices.get(1).getAmount();

				if (account.getBalance() > lastTwoMonthsInvoice) {

					returnables.add(convert(account));
				}
			}

		}

		return returnables;
	}

	public List<Account> getEligableForPriceIncrease() {

		return convert(accountRepository.getActiveStartedInMonth((new DateTime().getMonthOfYear() + 1) % 12));
	}

	@Transactional
	public boolean upgradePricePlan(String accountNumber) {

		za.co.kleen.dao.entity.Account account = accountRepository.findByAccountNumber(accountNumber);

		if (account == null) {
			return false;
		}

		if (account.getFuturePlan() == null) {

			throw new NoFuturePlanException("There is no future plan for account " + accountNumber);
		}

		account.setPlan(account.getFuturePlan());
		account.setFuturePlan(null);
		accountRepository.save(account);
		return true;
	}

	public void setPlanRepository(PlanRepository planRepository) {
		this.planRepository = planRepository;
	}

}
