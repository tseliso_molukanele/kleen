package za.co.kleenbin.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import za.co.kleenbin.dto.Expense;
import za.co.kleenbin.dto.ExpenseItem;
import za.co.kleenbin.dto.IncomeInvoiceItem;
import za.co.kleenbin.dto.IncomeItem;

@Service
public class ReportService {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private ExpenseService expenseService;

    public List<IncomeInvoiceItem> getIncomeInvoiceReport() {

        DateTime now = DateTime.now();
        DateTime monthIter = now.minusMonths(23);

        List<IncomeInvoiceItem> returnList = new ArrayList<IncomeInvoiceItem>();

        for(int i =0;i<24;i++) {

            Integer incomeAmount = paymentService.getPaymentsForMonth(monthIter);
            Integer invoiceAmount = invoiceService.getInvoiceForMonth(monthIter);
            Integer expenseAmount = expenseService.getExpenseForMonth(monthIter);

            IncomeInvoiceItem item = new IncomeInvoiceItem();
            item.setDate(monthIter.toDate());
            item.setIncomeAmount(incomeAmount != null ? incomeAmount : 0);
            item.setInvoiceAmount(invoiceAmount != null ? invoiceAmount : 0);
            item.setExpenseAmount(expenseAmount != null ? expenseAmount : 0);

            returnList.add(item);

            monthIter = monthIter.plusMonths(1);
        }

        return returnList;
    }

    public List<IncomeItem> getIncomeReport() {

        DateTime now = DateTime.now();
        DateTime monthIter = now.minusMonths(23);

        List<IncomeItem> returnList = new ArrayList<IncomeItem>();

        for(int i =0;i<24;i++) {

            Integer incomeAmount = paymentService.getPaymentsForMonth(monthIter);
            Integer cashAmount = paymentService.getCashPaymentsForMonth(monthIter);
            Integer nonCashAmount = paymentService.getNonCashPaymentsForMonth(monthIter);
            Integer postInvAmount = paymentService.getPostInvoicePaymentsForMonth(monthIter);
            Integer preInvAmount = paymentService.getPreInvoicePaymentsForMonth(monthIter);
            Integer postAmount = paymentService.getPostDatePaymentsForMonth(monthIter);
            Integer preAmount = paymentService.getPreDatePaymentsForMonth(monthIter);

            IncomeItem item = new IncomeItem();
            item.setDate(monthIter.toDate());
            item.setIncomeAmount(incomeAmount != null ? incomeAmount : 0);
            item.setCashIncomeAmount(cashAmount != null ? cashAmount : 0);
            item.setBankIncomeAmount(nonCashAmount != null ? nonCashAmount : 0);
            item.setPostInvoiceAmount(postInvAmount != null ? postInvAmount : 0);
            item.setPreInvoiceAmount(preInvAmount != null ? preInvAmount : 0);
            item.setPostDateAmount(postAmount != null ? postAmount : 0);
            item.setPreDateAmount(preAmount != null ? preAmount : 0);

            returnList.add(item);

            monthIter = monthIter.plusMonths(1);
        }

        return returnList;
    }

    public List<ExpenseItem> getExpenseReport() {

        DateTime now = DateTime.now();
        DateTime sixtyDaysEarlier = now.minusDays(60);

        List<ExpenseItem> returnList = new ArrayList<>();

        List<Expense> expenses = expenseService.getExpenses(sixtyDaysEarlier, now);

        Map<Date, ExpenseItem> map = new HashMap<>();

        for(Expense expense : expenses) {

            if(!map.keySet().contains(expense.getDate())) {

                ExpenseItem item = new ExpenseItem();
                item.setDate(expense.getDate());
                map.put(expense.getDate(), item);
                returnList.add(item);
            }

                ExpenseItem found = map.get(expense.getDate());

                switch(expense.getExpenseType()) {
                    case AIR_TIME: found.setAirtimeAmount(found.getAirtimeAmount() + expense.getAmount());break;
                    case CHEMICALS: CLEANING_EQP: found.setCleaningEquipmentAmount(found.getCleaningEquipmentAmount() + expense.getAmount());break;
                    case OTHER: found.setOtherAmount(found.getOtherAmount() + expense.getAmount());break;
                    case PETROL: found.setPetrolAmount(found.getPetrolAmount() + expense.getAmount());break;
                    case MOTOR_EQP: found.setMotorEquipmentAmount(found.getMotorEquipmentAmount() + expense.getAmount());break;
                    case WAGES: found.setWagesAmount(found.getWagesAmount() + expense.getAmount());break;
                }
        }

        return returnList;
    }
}
