package za.co.kleenbin.service;

import org.springframework.data.domain.AuditorAware;

public interface AdvancedAuditorAware extends AuditorAware<String> {

    String getCurrentAuditorRole();
}
