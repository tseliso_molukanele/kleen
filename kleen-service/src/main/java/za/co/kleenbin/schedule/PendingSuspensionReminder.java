package za.co.kleenbin.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import za.co.kleenbin.service.NotificationService;
import za.co.kleenbin.service.PendingSuspensionService;

@Component
public class PendingSuspensionReminder {

	@Autowired
	private PendingSuspensionService service;
	
	@Autowired
	NotificationService notificationService;

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public void setPendingSuspensionService(PendingSuspensionService service) {
		this.service = service;
	}

	@Scheduled(cron = "${pendingsuspension.schedule}")
	public void runReminder() {
		try {
			
			service.reminderRun();
		} catch (RuntimeException e) {

			String message = String.format("Error notifying customer of pending supensions: %s", e.getMessage());

			logger.error(message);
			notificationService.sendEmail(message);
		}
	}

	@Scheduled(cron = "${suspension.schedule}")
	public void runSuspend() {
		try {
			
			service.suspensionRun();
		} catch (RuntimeException e) {

			String message = String.format("Error running scheduled supensions: %s", e.getMessage());

			logger.error(message);
			notificationService.sendEmail(message);
		}
	}

}
