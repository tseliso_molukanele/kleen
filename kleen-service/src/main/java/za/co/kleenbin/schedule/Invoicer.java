package za.co.kleenbin.schedule;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import za.co.kleenbin.service.InvoiceService;

@Component
public class Invoicer {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private InvoiceService invoiceService;
    
    public void setInvoiceService(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @Scheduled(cron = "${invoice.schedule}")
    public void invoice() {

        DateTime now = new DateTime();
        int currentMonth = now.getMonthOfYear();

        boolean done = false;

        logger.info("Checking to run invoicing");

        if (!done && (now.getDayOfWeek() == DateTimeConstants.SATURDAY)) {

            DateTime followingFriday = now.plusDays(6);

            if (followingFriday.getMonthOfYear() == currentMonth) {

                DateTime followingFollowingFriday = followingFriday.plusDays(7);

                if (followingFollowingFriday.getMonthOfYear() != currentMonth) {

                    logger.info("Running invoicing");
                    invoiceService.invoiceRun();
                    done = true;
                }
            }
        }

        if(!done && (now.dayOfMonth().get() >= 24)) {

            logger.info("Running invoicing");
            invoiceService.invoiceRun();
            done = true;
        }
    }
}
