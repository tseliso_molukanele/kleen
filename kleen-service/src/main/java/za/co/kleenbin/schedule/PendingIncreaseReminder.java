package za.co.kleenbin.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import za.co.kleenbin.service.NotificationService;
import za.co.kleenbin.service.PendingIncreaseService;

@Component
public class PendingIncreaseReminder {
	
	@Autowired
	private PendingIncreaseService service;
	
	@Autowired
	private NotificationService notificationService;
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Scheduled(cron = "${pendingincrease.schedule}")
	public void runReminder() {
		
		try {
			
			service.reminderRun();
		} catch (RuntimeException e) {
			
			String message = String.format("Error sending increase reminders: %s", e.getMessage());
			
			logger.error(message);
			notificationService.sendEmail(message);
		}
	}

	@Scheduled(cron = "${increase.schedule}")
	public void runIncrease() {
		try {
			
			service.increaseRun();
		} catch (RuntimeException e) {
			
			String message = String.format("Error running scheduled increase : %s", e.getMessage());
			
			logger.error(message);
			notificationService.sendEmail(message);
		}
	}


}
