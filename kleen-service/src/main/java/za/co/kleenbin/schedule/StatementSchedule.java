package za.co.kleenbin.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import za.co.kleenbin.service.StatementSender;

@Component
public class StatementSchedule {
    
    @Autowired
    private StatementSender statementSender;

    @Value("${statement.run:false}")
    private boolean statementRun;
    
    @Scheduled(cron = "${statement.schedule}")
    public void sendStatements() {
        
        if(statementRun) {
            
            statementSender.sendStatements();
        }
    }
}
