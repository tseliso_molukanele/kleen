package za.co.kleen.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import za.co.kleen.dao.entity.Service;

public interface ServiceRepository extends JpaRepository<Service, Long> {


}
