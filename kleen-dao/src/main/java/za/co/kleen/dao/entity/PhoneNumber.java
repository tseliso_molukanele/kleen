package za.co.kleen.dao.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import za.co.kleen.dao.entity.type.NumberType;

@Entity
@Audited
public class PhoneNumber extends Model {

	private static final long serialVersionUID = 6324130037991474131L;

	@Column
	@NotNull
	private NumberType numberType;

	@Column
	@NotNull
	private String number;

	public String getNumber() {
		return number;
	}

	public NumberType getNumberType() {
		return numberType;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setNumberType(NumberType numberType) {
		this.numberType = numberType;
	}

	@Override
	public String toString() {

		return super.toString().charAt(0)
				+ super.toString().substring(1).toLowerCase();
	}
}
