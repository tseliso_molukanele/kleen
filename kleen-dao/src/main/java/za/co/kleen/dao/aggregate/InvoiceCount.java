package za.co.kleen.dao.aggregate;

public class InvoiceCount {

	private long totalBins;
	private long totalClients;
	private long totalServices;
	private long totalAmount;

	public InvoiceCount(Long totalBins, Long totalClients, Long totalServices, Long totalAmount) {
		
		this.totalBins = totalBins == null ? 0 : totalBins;
		this.totalClients = totalClients == null ? 0 : totalClients;
		this.totalServices = totalServices == null ? 0 : totalServices;
		this.totalAmount = totalAmount == null ? 0 : totalAmount;
	}

	public long getTotalAmount() {
		return totalAmount;
	}
	
	public long getTotalBins() {
		return totalBins;
	}
	
	public long getTotalClients() {
		return totalClients;
	}
	
	public long getTotalServices() {
		return totalServices;
	}
}
