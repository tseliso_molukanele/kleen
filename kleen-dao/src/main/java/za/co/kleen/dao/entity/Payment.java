package za.co.kleen.dao.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.hibernate.envers.Audited;

import za.co.kleen.dao.entity.type.PaymentType;

@Entity
@DiscriminatorValue("Payment")
@Audited
public class Payment extends BalanceUpdate {

	private static final long serialVersionUID = 1L;

	@Column
	@Enumerated(EnumType.STRING)
	private PaymentType type;

	private String reference;

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getReference() {
		return reference;
	}

	public PaymentType getType() {
		return type;
	}

	public void setType(PaymentType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return super.toString() + ", " + type.toString() + ", " + getCreatedBy();
	}

}
