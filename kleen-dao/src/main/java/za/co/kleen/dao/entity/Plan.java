package za.co.kleen.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

import za.co.kleen.dao.entity.type.FREQUENCY;

@Entity
@Audited
public class Plan extends Model {

    private static final long serialVersionUID = -4362261780516872575L;

    @Column
    private Integer numberOfBins;
    @Column
    private FREQUENCY frequency;
    @Column
    private Integer price;
    @Column
	private Integer year;

    public Integer getNumberOfBins() {
        return numberOfBins;
    }

    public void setNumberOfBins(Integer numberOfBins) {
        this.numberOfBins = numberOfBins;
    }

    public FREQUENCY getFrequency() {
        return frequency;
    }

    public void setFrequency(FREQUENCY frequency) {
        this.frequency = frequency;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

	public Integer getYear() {
		return year;
	}
	
	public void setYear(Integer year) {
		this.year = year;
	}
}
