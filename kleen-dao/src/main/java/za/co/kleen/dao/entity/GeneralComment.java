package za.co.kleen.dao.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Entity
@DiscriminatorValue("GeneralComment")
@Audited
public class GeneralComment extends Comment {

	private static final long serialVersionUID = 1L;

}
