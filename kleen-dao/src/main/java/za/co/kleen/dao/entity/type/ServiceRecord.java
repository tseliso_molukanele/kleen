package za.co.kleen.dao.entity.type;

public enum ServiceRecord {

    BIN_UNREACHABLE, SERVICED
}
