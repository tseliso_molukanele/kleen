package za.co.kleen.dao.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import za.co.kleen.dao.aggregate.InvoiceCount;
import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    static final String TOTAL_INVOICE_FOR_MONTH = "select sum(amount) from Invoice i where month(date) = :month and year(date)= :year";
    static final String TOTALS_FOR_YEAR_MONTH = "select new za.co.kleen.dao.aggregate.InvoiceCount("
    		+ " sum(numberOfBins) as totalBins,"
    		+ " sum(id) as totalClients,"
    		+ " sum(serviceCount) as totalServices,"
    		+ " sum(amount) as totalAmount) "
    		+ " from Invoice i "
    		+ " where month(date) = :month and year(date)= :year";
    
    Invoice findByAccountAndNumber(Account account, String number);

    @Query(TOTAL_INVOICE_FOR_MONTH)
    Integer getInvoiceForMonth(@Param("year") int year, @Param("month") int monthOfYear);
    
    static final String EARLIEST_DATE_FOR_MONTH = "select COALESCE(min(i.date), current_date) from Invoice i where month(i.date) = :month and year(i.date) = :year";
    
    @Query(EARLIEST_DATE_FOR_MONTH)
    Date getEarliestDateForMonth(@Param("year") Integer year, @Param("month") Integer monthOfYear);

    @Query(TOTALS_FOR_YEAR_MONTH)
    InvoiceCount findTotalsByYearMonth(@Param("year") int year, @Param("month") int monthOfYear);
    
}
