package za.co.kleen.dao.entity;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class Proprietor extends Model {

    private static final long serialVersionUID = -4362261780516872575L;

    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="proprietor_property", joinColumns=@JoinColumn(name="proprietor_id"))
    @MapKeyColumn(name = "keyname")
    @Column(name = "valuename")
    private Map<String, String> properties = new HashMap<String, String>();

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
