package za.co.kleen.dao.entity;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class Business extends Contactable {

	private static final long serialVersionUID = -5652609709885361456L;

	@OneToOne
	private Address address;
	@Column
	private String tradingAs;
	@Column
	private String companyNumber;
	@Column
	private String franchiseArea;
	@OneToOne
	private BankDetails bankDetails;
	@OneToMany
	private List<Person> members;
	@OneToMany(mappedBy = "business")
	private List<Account> accounts;
	@OneToMany(mappedBy = "business")
	private List<InvoiceCommunication> invoiceCommunications;

	public List<InvoiceCommunication> getInvoiceCommunications() {
		return invoiceCommunications;
	}

	public void setInvoiceCommunications(
			List<InvoiceCommunication> invoiceCommunications) {
		this.invoiceCommunications = invoiceCommunications;
	}

	public String getFranchiseArea() {
		return franchiseArea;
	}

	public void setFranchiseArea(String franchiseArea) {
		this.franchiseArea = franchiseArea;
	}

	public String getTradingAs() {
		return tradingAs;
	}

	public void setTradingAs(String tradingAs) {
		this.tradingAs = tradingAs;
	}

	public String getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	public BankDetails getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}

	public List<Person> getMembers() {
		return members;
	}

	public void setMembers(List<Person> members) {
		this.members = members;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public int countBins() {

		int result = 0;

		if (accounts != null) {

			for (Account account : accounts) {

				result += account.getPlan().getNumberOfBins();
			}

		}
		return result;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
