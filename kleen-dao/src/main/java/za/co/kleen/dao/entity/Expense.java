package za.co.kleen.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.hibernate.envers.Audited;

import za.co.kleen.dao.entity.type.ExpenseType;

@Entity
@Audited
public class Expense extends Model {

	private static final long serialVersionUID = 1L;

	@Column
	private Date date;
	@Column
	private Integer amount;
	@Column
	@Enumerated(EnumType.STRING)
	private ExpenseType expenseType;
	@Column
	private String role;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public ExpenseType getExpenseType() { return expenseType; }

	public void setExpenseType(ExpenseType expenseType) { this.expenseType = expenseType; }

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return (getDate() != null ? YYYY_MM_DD.format(getDate()) : "NO DATE") + ", " + getAmount();
	}

}
