package za.co.kleen.dao.entity.type;

public enum AccountStatus {
    ACTIVE, INACTIVE, DISABLED
}
