package za.co.kleen.dao.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import za.co.kleen.dao.entity.Expense;
import za.co.kleen.dao.entity.type.ExpenseType;

public interface ExpenseRepository extends JpaRepository<Expense, Long> {

    static final String TOTAL_EXPENSES_FOR_MONTH = "select sum(amount) from Expense e where month(date) = :month and year(date)= :year";
    final static String FIND_BY_AMOUNT_DATERANGE =
            "select p "
                    + "from Expense p "
                    + "where p.amount = :amount "
                    + "and p.expenseType = :expenseType "
                    + "and p.date > :dateFrom "
                    + "and p.date < :dateTo";

    final static String FIND_BY_AMOUNT_DATERANGE_EXCL_ID = FIND_BY_AMOUNT_DATERANGE + " and p.id != :id";

    List<Expense> findByDateBetween(Date startDate, Date endDate);

    List<Expense> findTop50ByOrderByDateDesc();

    @Query(TOTAL_EXPENSES_FOR_MONTH)
    Integer getExpenseForMonth(@Param("year") Integer year, @Param("month") Integer month);

    @Query(FIND_BY_AMOUNT_DATERANGE)
    List<Expense> findByType_amount_dateRange(
            @Param("expenseType")ExpenseType expenseType, @Param("amount") Integer amount,
            @Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

    @Query(FIND_BY_AMOUNT_DATERANGE_EXCL_ID)
    List<Expense> findByType_amount_dateRange_excl_id(
            @Param("expenseType")ExpenseType expenseType, @Param("amount") Integer amount,
            @Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo, @Param("id") Long id);
}
