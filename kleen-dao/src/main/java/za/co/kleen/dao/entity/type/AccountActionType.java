package za.co.kleen.dao.entity.type;

public enum AccountActionType {
    
    HIGH_BALANCE("Balance is too high"), STATEMENT("System must send statements");        
    
    private String message;
    
    AccountActionType(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
}
