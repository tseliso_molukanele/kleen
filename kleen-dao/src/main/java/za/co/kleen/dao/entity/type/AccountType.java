package za.co.kleen.dao.entity.type;

public enum AccountType {
	PRIVATE, CRECHE, RESTAURANT, COMPLEX
}
