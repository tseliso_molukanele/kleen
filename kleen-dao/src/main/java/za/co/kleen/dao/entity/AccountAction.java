package za.co.kleen.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;

import za.co.kleen.dao.entity.type.AccountActionType;

@Entity
@Audited
public class AccountAction extends Model {

    private static final long serialVersionUID = -5703258436274445710L;
    
    @OneToOne
    private Account account;
    
    @Enumerated(EnumType.STRING)
    @Column
    private AccountActionType type;
    
    public Account getAccount() {
        return account;
    }
    
    public void setAccount(Account account) {
        this.account = account;
    }
    
    public AccountActionType getType() {
        return type;
    }
    
    public void setType(AccountActionType type) {
        this.type = type;
    }
    
}
