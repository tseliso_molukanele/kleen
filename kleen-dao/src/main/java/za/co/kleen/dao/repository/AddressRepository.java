package za.co.kleen.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import za.co.kleen.dao.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Long>{

}
