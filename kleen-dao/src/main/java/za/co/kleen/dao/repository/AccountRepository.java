package za.co.kleen.dao.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import za.co.kleen.dao.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long>, AccountRepositoryCustom {

	public static final String GET_MAX_ACCOUNT_NUMBER = "SELECT max(cast(a.accountNumber as integer)) FROM Account a";

    static final String SEARCH_QUERY = "select distinct a " + "from Account a "
			+ " left join a.accountHolder.emailAddresses as emailAddress "
			+ " left join a.accountHolder.phoneNumbers as phoneNumber "
			+ "where lower(a.accountNumber) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(a.accountHolder.firstName) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(a.accountHolder.lastName) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(a.address.addressLine1) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(a.address.street) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(a.address.surburb) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(a.address.code) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(emailAddress.email) like CONCAT('%',lower(:searchKey),'%') "
			+ "or lower(phoneNumber.number) like CONCAT('%',lower(:searchKey),'%') ";
	
	static final String FIND_ACTIVE_BY_DAY = "select a from Account a where dayOfCleaning  = :day and a.status = 'ACTIVE'";
	static final String FIND_ACTIVE = "select a from Account a where a.status = 'ACTIVE'";
	
	static final String FIND_BY_ACCOUNT_NUMBER = "select a " + "from Account a "
			+ "where a.accountNumber = :accountNumber ";
	
	static final String FIND_ACTIVE_WITH_HIGH_BALANCE = " select a from Account a where a.status = 'ACTIVE' group by a order by (sum(a.totalInvoiced) - sum(a.totalPayed)) desc ";

	@Query(SEARCH_QUERY)
	List<Account> search(@Param("searchKey") String searchKey);

	@Query(FIND_BY_ACCOUNT_NUMBER)
	Account findByAccountNumber(@Param("accountNumber")String accountNumber);

	@Query(FIND_ACTIVE_BY_DAY)
        List<Account> findActiveByDay(@Param("day")Integer day);
	
	@Query(GET_MAX_ACCOUNT_NUMBER)
	Long getMaxAccountNumber();

	@Query(FIND_ACTIVE)
        List<Account> findActive();

	@Query(FIND_ACTIVE_WITH_HIGH_BALANCE)
        List<Account> getActiveWithHighBalance(Pageable pageable);

	@Query(" select a from Account a where a.id in :idList ")
	List<Account> getByIds(@Param("idList") List<Long> idList);

	@Query(" select a from Account a where a.status = 'ACTIVE' and month(a.commencementDate) = :month ")
	List<Account> getActiveStartedInMonth(@Param("month") Integer month);
}
