package za.co.kleen.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import za.co.kleen.dao.entity.Proprietor;

public interface ProprietorRepository extends JpaRepository<Proprietor, Long> {

    static final String FIND_BY_KEY = "select p from Proprietor p join p.properties ps where ( KEY(ps) = :key ) ";
    static final String FIND_BY_KEY_VALUE = "select p.* from Proprietor p join proprietor_property ps on ps.proprietor_id = p.id where ps.keyname = :key and ps.valuename = :value";

    @Query(FIND_BY_KEY)
    List<Proprietor> findByKey(@Param("key") String key);

    @Query(value = FIND_BY_KEY_VALUE, nativeQuery = true)
    List<Proprietor> findByKeyValue(@Param("key")String key, @Param("value")String value);

}
