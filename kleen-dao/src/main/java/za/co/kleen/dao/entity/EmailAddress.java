package za.co.kleen.dao.entity;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class EmailAddress extends Model {

	private static final long serialVersionUID = 1L;
	
	@NotNull
	private String email;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getEncodedEmail() {
		
		try {
			return URLEncoder.encode(email, "UTF-8").replace("%", "=");
		} catch (UnsupportedEncodingException e) {
			
			throw new RuntimeException(e);
		}
	}

}
