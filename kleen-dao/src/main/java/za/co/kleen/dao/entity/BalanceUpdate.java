package za.co.kleen.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.envers.Audited;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BALANCE_UPDATE_TYPE", discriminatorType = DiscriminatorType.STRING)
@Audited
public abstract class BalanceUpdate extends Model {

	private static final long serialVersionUID = 1712778604730486785L;

	@Column
	private Date date;
	@Column
	private Integer amount;
	@ManyToOne
	private Account account;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return (getDate() != null ? YYYY_MM_DD.format(getDate()) : "NO DATE") + ", " + getAmount() + ", "
				+ account.getId() + ":" + account.getAccountNumber();
	}
}
