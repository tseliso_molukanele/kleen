package za.co.kleen.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.entity.type.FREQUENCY;

public interface PlanRepository extends JpaRepository<Plan, Long> {

	Plan findByFrequencyAndNumberOfBinsAndYear(FREQUENCY frequency, Integer numberOfBins, int year);
}
