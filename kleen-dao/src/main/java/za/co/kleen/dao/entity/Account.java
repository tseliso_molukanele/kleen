package za.co.kleen.dao.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import za.co.kleen.dao.entity.type.AccountStatus;
import za.co.kleen.dao.entity.type.AccountType;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "accountNumber"))
@Audited
public class Account extends Model {

    private static final long serialVersionUID = -4362261780516872575L;

    @OneToOne
    private Address address;

    @ManyToOne
    private Business business;
    
    @ManyToOne
    private Person accountHolder;
    
    @ManyToOne
    @NotNull
    private Plan plan;
    
    @ManyToOne
    private Plan futurePlan;
    
    @Column(unique=true, nullable=false)
    private String accountNumber;
    @OneToMany(mappedBy = "account")
    @OrderBy("date DESC")
    private List<Service> services = new ArrayList<Service>();
    @Column
    private int dayOfCleaning;
    @Column
    private float outstandingBalance;

    @Enumerated(EnumType.STRING)
    @Column
    private AccountStatus status;
    
    @Enumerated(EnumType.STRING)
    @Column
    private AccountType type;

    @Formula(" (select ifnull(sum(pay.amount), 0) from Payment pay where pay.account_id = id)")
    @NotAudited
    private Float totalPayed;
    
    @Formula(" (select ifnull(sum(inv.amount), 0) from Invoice inv where inv.account_id = id)")
    @NotAudited
    private Float totalInvoiced;
    
    @Formula(" (select ifnull(max(inv.date), current_date) from Invoice inv where inv.account_id = id)")
    @NotAudited
    private Date maxDateInvoiced;

    @Column
    private String salesConsultant;
    @Column
    private Date commencementDate;

    @OneToMany(mappedBy = "account")
    @OrderBy("date DESC")
    private List<Invoice> invoices;
        
    @OneToMany(mappedBy = "account")
    @OrderBy("date DESC")
    private List<Payment> payments;

    @OneToMany(mappedBy = "account")
    private List<ScheduleComment> scheduleComments = new ArrayList<ScheduleComment>();

    @OneToMany(mappedBy = "account")
    private List<GeneralComment> generalComments = new ArrayList<GeneralComment>();

    @OneToMany(mappedBy = "account")
    private List<InvoiceComment> invoiceComments = new ArrayList<InvoiceComment>();

    public Plan getPlan() {
		return plan;
	}
    
    public void setPlan(Plan plan) {
		this.plan = plan;
	}
    
    public Date getMaxDateInvoiced() {
        return maxDateInvoiced;
    }

    public float getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(float outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getPaymentReference() {
        return "JX" + String.format("%1$" + 3 + "s", accountNumber).replace(" ", "0");
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public List<GeneralComment> getGeneralComments() {
        return generalComments;
    }

    public void setGeneralComments(List<GeneralComment> generalComments) {
        this.generalComments = generalComments;
    }

    public List<InvoiceComment> getInvoiceComments() {
        return invoiceComments;
    }

    public void setInvoiceComments(List<InvoiceComment> invoiceComments) {
        this.invoiceComments = invoiceComments;
    }

    public List<ScheduleComment> getScheduleComments() {
        return scheduleComments;
    }

    public void setScheduleComments(List<ScheduleComment> scheduleComments) {
        this.scheduleComments = scheduleComments;
    }

    public void setAccountHolder(Person accountHolder) {

        this.accountHolder = accountHolder;
    }

    public Person getAccountHolder() {
        return accountHolder;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Service> getServices() {
        return services;
    }

    public int getDayOfCleaning() {
        return dayOfCleaning;
    }

    public void setDayOfCleaning(int dayOfCleaning) {
        this.dayOfCleaning = dayOfCleaning;
    }

    public String getSalesConsultant() {
        return salesConsultant;
    }

    public void setSalesConsultant(String salesConsultant) {
        this.salesConsultant = salesConsultant;
    }

    public Date getCommencementDate() {
        return commencementDate;
    }

    public void setCommencementDate(Date commencementDate) {
        this.commencementDate = commencementDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public float getBalance() {
                
        return (totalInvoiced == null ? 0 : totalInvoiced) - (totalPayed == null ? 0 : totalPayed);
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }
    
    public AccountType getType() {
		return type;
	}
    
    public void setType(AccountType type) {
		this.type = type;
	}
    
    public Plan getFuturePlan() {
		return futurePlan;
	}
    
    public void setFuturePlan(Plan futurePlan) {
		this.futurePlan = futurePlan;
	}

    @Override
    public String toString() {
        return String.format("Acc No: %s, acc holder: %s, address: %s", accountNumber, accountHolder, address);
    }
}
