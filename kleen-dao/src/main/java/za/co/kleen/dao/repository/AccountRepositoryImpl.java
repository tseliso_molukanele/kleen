package za.co.kleen.dao.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import za.co.kleen.dao.entity.Account;

public class AccountRepositoryImpl implements AccountRepositoryCustom {


    static final String LATE_PAYERS =
            " select a.id " +
                    " from Account a " +
                    " left join Payment p on p.account_id = a.id " +
                    " where a.status = 'ACTIVE' " +
                    " group by a.id " +
                    " having ifnull(((TO_SECONDS(now()) - TO_SECONDS(max(p.date))) / (24 * 60 * 60)), 1000) >= ?1 " +
                    " order by a.id desc ";

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    EntityManager entityManager;

    @Override
    public List<Account> getLatePayers(Integer days) {

        Query query = entityManager.createNativeQuery(LATE_PAYERS).setParameter(1, days);
        List<Number> numbers = query.getResultList();

        List<Long> ids = new ArrayList<>();
        for (Number number : numbers) {
            ids.add(number.longValue());
        }

        return accountRepository.getByIds(ids);
    }
}
