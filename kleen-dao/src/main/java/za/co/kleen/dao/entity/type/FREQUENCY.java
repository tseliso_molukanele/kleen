package za.co.kleen.dao.entity.type;


public enum FREQUENCY {

	ONCE_PER_MONTH(1), BI_WEEKLY(2), EVERY_WEEK(4), DAILY(20);

	private int asNumber;

	private FREQUENCY(int asNumber) {

		this.asNumber = asNumber;
	}

	public int asCount() {
		return asNumber;
	}

	public static FREQUENCY fromNumber(int number) {

		for (FREQUENCY frequency : FREQUENCY.values()) {

			if (frequency.asNumber == number) {

				return frequency;
			}
		}

		throw new RuntimeException(number + " is not a valid FREQUENCY number");
	}
}
