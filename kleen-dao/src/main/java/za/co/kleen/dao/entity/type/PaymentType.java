package za.co.kleen.dao.entity.type;

public enum PaymentType {

    CASH, DEPOSIT, EFT, CHEQUE, ADJUSTMENT, OTHER
}
