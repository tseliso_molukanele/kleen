package za.co.kleen.dao.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import za.co.kleen.dao.entity.AccountAction;
import za.co.kleen.dao.entity.type.AccountActionType;

public interface AccountActionRepository extends JpaRepository<AccountAction, Long> {

    static final String FIND_PENDING_BY_TYPE = 
            "select a " 
            + "from AccountAction a "
            + "where a.type = :type ";
    
    List<za.co.kleen.dao.entity.AccountAction> findByType(@Param("type") AccountActionType type, Pageable pageable);

}
