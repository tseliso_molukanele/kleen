package za.co.kleen.dao.entity;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Entity
@DiscriminatorValue("Invoice")
@Audited
public class Invoice extends BalanceUpdate {

	private static final long serialVersionUID = 1L;

	@Column
	private String number;
	@Column
	private Float invoiceAmount;
	@Column
	private Integer numberOfBins;
	@Column
	private Integer serviceCount;
	@Column
	private Integer month;
	@Column
	private Integer year;

	public Integer getYear() {
		return year;
	}

	public Integer getMonth() {
		return month;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Float getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Float invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Integer getNumberOfBins() {
		return numberOfBins;
	}

	public void setNumberOfBins(Integer numberOfBins) {
		this.numberOfBins = numberOfBins;
	}

	public void setServiceCount(Integer serviceCount) {
		this.serviceCount = serviceCount;
	}

	public Integer getServiceCount() {
		return serviceCount;
	}

	@Override
	public void setDate(Date date) {
		super.setDate(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		this.year = calendar.get(Calendar.YEAR);
		this.month = calendar.get(Calendar.MONTH);
	}
}
