package za.co.kleen.dao.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long> {

	final static String FIND_BY_ACCOUNT_AMOUNT_DATERANGE = 
			"select p "
			+ "from Payment p "
			+ "where p.account = :account "
			+ "and p.amount = :amount "
			+ "and p.date > :dateFrom "
			+ "and p.date < :dateTo";

	final static String FIND_BY_ACCOUNT_AMOUNT_DATERANGE_EXCL_ID = FIND_BY_ACCOUNT_AMOUNT_DATERANGE + " and p.id != :id";
	static final String TOTAL_PAYMENTS_FOR_MONTH = "select sum(amount) from Payment p where month(date) = :month and year(date)= :year";
	static final String TOTAL_CASH_PAYMENTS_FOR_MONTH = "select sum(amount) from Payment p where month(date) = :month and year(date)= :year and type = 'CASH'";
	static final String TOTAL_NON_CASH_PAYMENTS_FOR_MONTH = "select sum(amount) from Payment p where month(date) = :month and year(date)= :year and not (type = 'CASH')";
	static final String TOTAL_PRE_PAYMENTS_FOR_MONTH = "select sum(amount) from Payment p where month(date) = :month and year(date)= :year and date < :dateAt ";
	static final String TOTAL_POST_PAYMENTS_FOR_MONTH = "select sum(amount) from Payment p where month(date) = :month and year(date)= :year and date >= :dateAt";

	@Query(FIND_BY_ACCOUNT_AMOUNT_DATERANGE)
	List<Payment> findByAccount_amount_dateRange(
			@Param("account") Account account, @Param("amount") Integer amount,
			@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

	@Query(FIND_BY_ACCOUNT_AMOUNT_DATERANGE_EXCL_ID)
	List<Payment> findByAccount_amount_dateRange_excl_id(
			@Param("account") Account account, @Param("amount") Integer amount,
			@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo, @Param("id") Long id);

	@Query(TOTAL_PAYMENTS_FOR_MONTH)
        Integer getPaymentsForMonth(@Param("year") Integer year, @Param("month") Integer month);

        List<Payment> findTop50ByOrderByDateDesc();

	@Query(TOTAL_CASH_PAYMENTS_FOR_MONTH)
	Integer getCashPaymentsForMonth(@Param("year") Integer year, @Param("month") Integer monthOfYear);

	@Query(TOTAL_NON_CASH_PAYMENTS_FOR_MONTH)
	Integer getNonCashPaymentsForMonth(@Param("year") Integer year, @Param("month") Integer monthOfYear);

	@Query(TOTAL_POST_PAYMENTS_FOR_MONTH)
	Integer getPostPaymentsForMonth(@Param("year") Integer year, @Param("month") Integer monthOfYear, @Param("dateAt") Date dateAt);

	@Query(TOTAL_PRE_PAYMENTS_FOR_MONTH)
	Integer getPrePaymentsForMonth(@Param("year") Integer year, @Param("month") Integer monthOfYear, @Param("dateAt") Date dateAt);
}
