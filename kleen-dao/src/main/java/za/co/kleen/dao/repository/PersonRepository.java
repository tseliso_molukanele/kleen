package za.co.kleen.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import za.co.kleen.dao.entity.Person;

public interface PersonRepository extends JpaRepository<Person, Long>{

}
