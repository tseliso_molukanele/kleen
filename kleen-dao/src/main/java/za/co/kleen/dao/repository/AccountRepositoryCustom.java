package za.co.kleen.dao.repository;

import java.util.List;

import za.co.kleen.dao.entity.Account;

public interface AccountRepositoryCustom {

    List<Account> getLatePayers(Integer days);
}
