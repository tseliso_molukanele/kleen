package za.co.kleen.dao.entity;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.envers.Audited;

@MappedSuperclass
@Audited
public abstract class Contactable extends Model {

	private static final long serialVersionUID = 4658222874143976282L;

	@OneToMany(cascade=CascadeType.ALL)
	private List<PhoneNumber> phoneNumbers;
	@OneToMany(cascade=CascadeType.ALL)
	private List<EmailAddress> emailAddresses;

	public void setEmailAddresses(List<EmailAddress> emailAddresses) {
		this.emailAddresses = emailAddresses;
	}

	public List<EmailAddress> getEmailAddresses() {
		return emailAddresses;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
}
