package za.co.kleen.dao.entity.type;

public enum ExpenseType {
    PETROL, CLEANING_EQP, WAGES, CHEMICALS, MOTOR_EQP, OTHER, AIR_TIME, RENT, MARKETING, ROYALTY
}
