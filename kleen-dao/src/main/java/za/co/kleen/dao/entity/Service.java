package za.co.kleen.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

import za.co.kleen.dao.entity.type.ServiceRecord;

@Entity
@Audited
public class Service extends Model {

	private static final long serialVersionUID = -189631444055945783L;

	private Date date;

	@Enumerated(EnumType.STRING)
	@Column
	private ServiceRecord serviceRecord;
	
	@ManyToOne
	private Account account;
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}

	public ServiceRecord getServiceRecord() { return serviceRecord; }

	public void setServiceRecord(ServiceRecord serviceRecord) { this.serviceRecord = serviceRecord; }
}
