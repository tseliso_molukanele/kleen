package za.co.kleen.dao.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import za.co.kleen.conf.DAOConfiguration;
import za.co.kleen.dao.entity.Proprietor;

@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DAOConfiguration.class)
@DatabaseSetup("classpath:datasets/proprietor.xml")
@DirtiesContext
@Transactional
public class ProprietorRepositoryTest {

    @Autowired
    private ProprietorRepository proprietorRepository;

    @Autowired
    private PlanRepository paymentRepor;

    @Test
    public void testAnything() {

        paymentRepor.findOne(1l);
    }

    @Test
    public void testFindByKey() {

        String key = "action";

        List<Proprietor> actions = proprietorRepository.findByKey(key);

        assertEquals(4, actions.size());

        for (Proprietor action : actions) {

            assertTrue(action.getProperties().containsKey(key));
        }
    }

    @Test
    public void testFindByKeyValue() {

        String key = "action";
        String value = "office";

        List<Proprietor> actions = proprietorRepository.findByKeyValue(key, value);

        assertEquals(2, actions.size());

        for (Proprietor action : actions) {

            assertEquals(value, action.getProperties().get(key));
        }
    }

    @Test
    public void testAddNewProperty() {

        String key = "action";
        String value = "office";

        Proprietor newP = new Proprietor();
        newP.getProperties().put(key, value);

        Proprietor saved = proprietorRepository.save(newP);

        assertEquals(value, saved.getProperties().get(key));
    }

    @Test
    public void testupdate() {

        String key = "super";
        String value = "admin";

        List<Proprietor> actions = proprietorRepository.findByKeyValue(key, value);

        assertEquals(1, actions.size());

        Proprietor action = actions.get(0);

        assertEquals("admin", action.getProperties().get("super"));
        assertEquals("2016-02-02", action.getProperties().get("date"));

        //
        action.getProperties().put("date", "2016-03-03");

        Proprietor saved = proprietorRepository.save(action);

        //
        assertEquals("admin", saved.getProperties().get("super"));
        assertEquals("2016-03-03", saved.getProperties().get("date"));
    }
}
