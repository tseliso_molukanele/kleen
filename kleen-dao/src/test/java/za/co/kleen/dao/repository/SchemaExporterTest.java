package za.co.kleen.dao.repository;

import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import za.co.kleen.conf.DAOConfiguration;
import za.co.kleen.dao.entity.Account;
import za.co.kleen.dao.entity.Address;
import za.co.kleen.dao.entity.BalanceUpdate;
import za.co.kleen.dao.entity.BankDetails;
import za.co.kleen.dao.entity.Business;
import za.co.kleen.dao.entity.Comment;
import za.co.kleen.dao.entity.Contactable;
import za.co.kleen.dao.entity.EmailAddress;
import za.co.kleen.dao.entity.Expense;
import za.co.kleen.dao.entity.GeneralComment;
import za.co.kleen.dao.entity.Invoice;
import za.co.kleen.dao.entity.InvoiceComment;
import za.co.kleen.dao.entity.InvoiceCommunication;
import za.co.kleen.dao.entity.MetaData;
import za.co.kleen.dao.entity.Model;
import za.co.kleen.dao.entity.Payment;
import za.co.kleen.dao.entity.Person;
import za.co.kleen.dao.entity.PhoneNumber;
import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.entity.Proprietor;
import za.co.kleen.dao.entity.ScheduleComment;
import za.co.kleen.dao.entity.Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DAOConfiguration.class)
public class SchemaExporterTest {

    @Test
    public void exportDatabaseSchema() {
        Configuration configuration = new Configuration();

        configuration
                .addAnnotatedClass(Account.class)
                .addAnnotatedClass(Address.class)
                .addAnnotatedClass(BalanceUpdate.class)
                .addAnnotatedClass(BankDetails.class)
                .addAnnotatedClass(Business.class)
                .addAnnotatedClass(Comment.class)
                .addAnnotatedClass(Contactable.class)
                .addAnnotatedClass(EmailAddress.class)
                .addAnnotatedClass(GeneralComment.class)
                .addAnnotatedClass(Invoice.class)
                .addAnnotatedClass(InvoiceComment.class)
                .addAnnotatedClass(InvoiceCommunication.class)
                .addAnnotatedClass(MetaData.class)
                .addAnnotatedClass(Model.class)
                .addAnnotatedClass(Payment.class)
                .addAnnotatedClass(Person.class)
                .addAnnotatedClass(PhoneNumber.class)
                .addAnnotatedClass(ScheduleComment.class)
                .addAnnotatedClass(Service.class)
                .addAnnotatedClass(Plan.class)
                .addAnnotatedClass(Proprietor.class)
                .addAnnotatedClass(Expense.class)
                .setProperty(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");

        SchemaExport schema = new SchemaExport(configuration).setDelimiter(";");
        schema.setOutputFile("schema.sql");

        schema.create(true, false);
    }

}
