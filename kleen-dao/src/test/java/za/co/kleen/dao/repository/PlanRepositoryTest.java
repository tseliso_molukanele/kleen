package za.co.kleen.dao.repository;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import za.co.kleen.conf.DAOConfiguration;
import za.co.kleen.dao.entity.Plan;
import za.co.kleen.dao.entity.type.FREQUENCY;

@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DAOConfiguration.class)
@DatabaseSetup("classpath:datasets/plan.xml")
@DirtiesContext
@Transactional
public class PlanRepositoryTest {

    @Autowired
    private PlanRepository planRepository;

    @Test
    public void should_find_plan_for_1bin_weekly_in_2016() {

    	Plan plan = planRepository.findByFrequencyAndNumberOfBinsAndYear(FREQUENCY.EVERY_WEEK, 1, 2016);

    	assertEquals(new Integer(2016), plan.getYear());
    	assertEquals(FREQUENCY.EVERY_WEEK, plan.getFrequency());
    	assertEquals(new Integer(1), plan.getNumberOfBins());
    	
    }
   
}