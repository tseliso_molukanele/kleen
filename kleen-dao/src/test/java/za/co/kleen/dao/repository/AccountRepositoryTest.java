package za.co.kleen.dao.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import za.co.kleen.conf.DAOConfiguration;
import za.co.kleen.dao.entity.Account;

@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DAOConfiguration.class)
@DatabaseSetup("classpath:datasets/latepayers.xml")
@DirtiesContext
@Transactional
public class AccountRepositoryTest {
	
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    public void testSearch() {

        //
        String searchKey = "searchKey";

        //
        accountRepository.search(searchKey);
    }

    @Test
    public void testGetLatePayers() {

        Query query = entityManager.createNativeQuery("CREATE ALIAS TO_SECONDS AS $$ "
                + "Long toSeconds(java.sql.Date date) { "
                + "if (date == null) return null;"
                + "return date.getTime() / 1000; "
                + "} $$; ");

        query.executeUpdate();

        DateTime testDate = new DateTime(2016, 8, 26, 10, 0);
        DateTime now = new DateTime();

        int daysBetween = Days.daysBetween(testDate, now).getDays();

        //
        List<Account> latePayers = accountRepository.getLatePayers(60 + daysBetween);

        //
        assertEquals(8, latePayers.size());
    }

}
