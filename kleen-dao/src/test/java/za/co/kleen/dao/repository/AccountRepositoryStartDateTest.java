package za.co.kleen.dao.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import za.co.kleen.conf.DAOConfiguration;
import za.co.kleen.dao.entity.Account;

@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DAOConfiguration.class)
@DatabaseSetup("classpath:datasets/startedInAugust.xml")
@DirtiesContext
@Transactional
public class AccountRepositoryStartDateTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testGetStartedInMonth() {

        //
        List<Account> commencedInJan = accountRepository.getActiveStartedInMonth(1);

        //
        assertEquals(1, commencedInJan.size());
        
        DateTime dateTime = new DateTime(commencedInJan.get(0).getCommencementDate().getTime());
        assertEquals(1, dateTime.getMonthOfYear());
    }

}
