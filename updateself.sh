
cd /root/kleen
git pull > updated.git

if ! cmp updated.git UPTODATE.git >/dev/null 2>&1
then
	export JAVA_HOME=/root/jdk1.8.0_60

	/root/apache-maven-3.2.5/bin/mvn clean install

	cp kleen-rest/target/kleen*.war /root/tomcat/webapps/kleenbin.war
fi

